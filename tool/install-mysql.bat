@setlocal enabledelayedexpansion
@echo off
color f0
rem 编程技术 www.vcvb.cn 出品，转载请保留此信息。
title MYSQL一键安装

set dir=%~dp0mysql-5.7.37-winx64
set basedir=%dir:\=/%
set bin=%basedir%//bin
cd /d %basedir%
if exist !bin!/mysqld.exe (
rem 写入配置
>my.ini echo [mysqld]
>>my.ini echo port=3306
>>my.ini echo basedir=!basedir!
>>my.ini echo datadir=!basedir!/Data
>>my.ini echo log-error=!basedir!/Logs.log
>>my.ini echo max_connections=200
>>my.ini echo max_connect_errors=10
>>my.ini echo character-set-server=utf8mb4
>>my.ini echo default-storage-engine=INNODB
>>my.ini echo default_authentication_plugin=mysql_native_password
>>my.ini echo local_infile=ON
>>my.ini echo.
>>my.ini echo [mysql]
>>my.ini echo default-character-set=utf8mb4
>>my.ini echo local_infile=ON
>>my.ini echo.
>>my.ini echo [client]
>>my.ini echo port=3306
>>my.ini echo default-character-set=utf8mb4

rem 添加环境变量
echo 开始检查环境变量...
echo.
for /f "tokens=2*" %%i ^
in ('reg query "HKLM\SYSTEM\ControlSet001\Control\Session Manager\Environment" /v Path^|findstr /i "path"') ^
do (set "p=%%j")
for /f "tokens=*" %%a in ('echo !p! ^| find /i "!bin!"') do (set flag=1)
if not defined flag (
echo 开始添加环境变量
setx Path "!p!;!bin!" /M | find "成功" && echo 环境变量添加成功
set p=!Path!
) else (echo 环境变量已存在，无需设置。)
echo.

rem 初始化 --console
cd !bin!
md !basedir!\Data
>!basedir!\Logs.log echo --------初始化数据库开始--------

echo 开始初始化数据库

mysqld --initialize-insecure

>>!basedir!\Logs.log echo --------初始化数据库结束--------

rem 安装服务
echo 开始安装服务
mysqld --install MySQL | find "successfully" && echo 服务安装成功
echo.
rem 启动服务
echo 开始启动MYSQL服务
net start MySQL
echo.

mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Changeme_123'";
mysql -uroot -p'Changeme_123' -e "GRANT ALL PRIVILEGES ON . TO 'root'@'%' IDENTIFIED BY 'Changeme_123' WITH GRANT OPTION;"
mysql -uroot -p'Changeme_123' -e "FLUSH PRIVILEGES;"

echo 安装完成
) else (
echo 有错误
)
pause >nul