package com.weijun.util;

import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONArray;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Service
public class ResponseTool {
    private static final Logger logger = LogManager.getLogger(ResponseTool.class.getName());
    public void writeJSONtoClient(HttpServletResponse response, JSONArray jsonArray){
        writeStringtoClient(response,jsonArray.toJSONString());
    }

    public void writeStringtoClient(HttpServletResponse response, String string) {
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();

            out.print(string);
            logger.info("return value of information is " +string);
            out.flush();
        } catch (IOException e) {
            logger.error(e);
        } finally {
            if (null != out) {
                out.close();
            }
        }
    }
}
