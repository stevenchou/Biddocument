package com.weijun.util;

import com.sun.mail.util.MailSSLSocketFactory;
import com.weijun.service.MailService;
import com.weijun.service.impl.AccountService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.*;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.Properties;

@Service
public class MailTool {
    @Autowired
    MailService mailService;
    @Autowired
    SecurityTool securityTool;
    @Autowired
    AccountService accountService;
    @Autowired
    ServletContext context;

    private JavaMailSenderImpl email;

    private SimpleMailMessage message;

    private static final Logger logger = LogManager.getLogger(MailTool.class.getName());
    public  Session generateSession(String currentUserName) throws Exception {
        Properties prop = new Properties();
// 开启debug调试，以便在控制台查看
        prop.setProperty("mail.debug", "true");
// 设置邮件服务器主机名
        prop.setProperty("mail.host", "smtp.exmail.qq.com");
// 发送服务器需要身份验证
        prop.setProperty("mail.smtp.auth", "true");
// 发送邮件协议名称
        prop.setProperty("mail.transport.protocol", "smtp");

// 开启SSL加密，否则会失败
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        prop.put("mail.smtp.ssl.enable", "true");
        prop.put("mail.smtp.ssl.socketFactory", sf);

// 创建session
        Session session = Session.getInstance(prop);
        return session;


// 连接邮件服务器：邮箱类型，帐号，授权码代替密码（更安全）
//        ts.connect("smtp.qq.com","1043521500", "dotyutyurrftbbia");//后面的字符是授权码，用qq密码反正我是失败了（用自己的，别用我的，这个号是我瞎编的，为了。。。。）



    }



    public MimeMessage createSimpleMail(Session session, HttpServletRequest request, String sendEmail)
            throws Exception {
        /**
         * create by: 周江
         * description: 创建一封只包含文本的邮件
         * create time: 5/29/2020 6:07 PM
         *
          * @Param: session 邮件会话
         * @Param: request HttpServletRequest
         * @Param: sendEmail 邮件内容
         * @return javax.mail.internet.MimeMessage
         */

        // 创建邮件对象
        MimeMessage message = new MimeMessage(session);

        String receiver=request.getParameter("receiver");

        InternetAddress[] sentTo=formatEmailAddress(receiver);
        String copyTo=request.getParameter("copyToInput");
        //抄送人不为空时
        if (""!=copyTo){
            InternetAddress[] copyToEmailAddress=formatEmailAddress(copyTo);
            message.setRecipients(MimeMessage.RecipientType.CC,copyToEmailAddress);
        }
        String SecurityToInput =request.getParameter("SecurityToInput");
        //密送人不为空时
        if (""!=SecurityToInput){
            InternetAddress[] securityToEmailAddress=formatEmailAddress(SecurityToInput);
            message.setRecipients(MimeMessage.RecipientType.BCC,securityToEmailAddress);
        }

        String theme=request.getParameter("theme");

        String filepath=request.getParameter("filepath");
        String mailContent=request.getParameter("mailContent");
        String[] fileNames=fixAttachFilePath(filepath);
        File[] attachment=generateFileByName(fileNames);

        // 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
        Multipart multipart = new MimeMultipart();

        // 添加邮件正文
        BodyPart contentPart = new MimeBodyPart();
        contentPart.setContent(mailContent, "text/html;charset=utf-8");
        multipart.addBodyPart(contentPart);

        BodyPart attachmentBodyPart = null;
        // 添加附件的内容
        if (null != attachment && attachment.length != 0) {
            for (File file : attachment) {
                attachmentBodyPart = new MimeBodyPart();

                DataSource source = new FileDataSource(file);
                attachmentBodyPart.setDataHandler(new DataHandler(source));
                //MimeUtility.encodeWord可以避免文件名乱码
                attachmentBodyPart.setFileName(MimeUtility.encodeWord(file.getName()));
                multipart.addBodyPart(attachmentBodyPart);
            }
        }

        // 将multipart对象放到message中
        message.setContent(multipart);

        // 保存邮件
        message.saveChanges();
// 指明邮件的发件人
        message.setFrom(new InternetAddress(sendEmail));
// 指明邮件的收件人，现在发件人和收件人是一样的，那就是自己给自己发
        message.setRecipients(MimeMessage.RecipientType.TO,sentTo);
// 邮件的标题
        message.setSubject(theme);
        // 发送日期
        message.setSentDate(new Date());

// 返回创建好的邮件对象
        return message;
    }

    private File[] generateFileByName(String[] fileNames) {
        File[] retFiles=new File[fileNames.length];
        for(int i=0;i<fileNames.length;i++){
            File tmpFile=new File(fileNames[i]);
            retFiles[i]=tmpFile;
        }
        return retFiles;
    }

    private void paddingAttachAndContent(MimeMessage message, String[] fileNames,String mailContent) throws Exception {

    }

    private String[] fixAttachFilePath(String filepath) {
        //删除filepath中第一个字符---X
        filepath=filepath.substring(1);
        String[] attachFilePathArray=filepath.split("X");

        String[] ret=new String[attachFilePathArray.length];
        String allProjectBase=context.getRealPath("upload")+ File.separator+"projects"+ File.separator;
        for(int i=0;i<attachFilePathArray.length;i++){
            ret[i]=allProjectBase+attachFilePathArray[i];
        }
        return ret;
    }

    private static InternetAddress[] formatEmailAddress(String originalEmailAddress) {
        String[] emailAddressArray=originalEmailAddress.split(";");
        InternetAddress[] format=new InternetAddress[emailAddressArray.length];
        for (int i = 0; i< emailAddressArray.length; i++) {
            String item= emailAddressArray[i];
            int pos=item.indexOf("<");
            item=item.substring(pos+1,item.length());
            int pos2=item.indexOf(">");
            item=item.substring(0,pos2);
            try {
                format[i]= new InternetAddress(item);
            } catch (AddressException e) {
                e.printStackTrace();
            }
        }
        return format;
    }

}
