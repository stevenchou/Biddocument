package com.weijun.util;

import org.springframework.stereotype.Service;

@Service
public class Constant {
    public static final int TOPN = 6;
    public static final String customtype_personalCustom="personalCustom";
    public static final String customtype_companyCustom="companyCustom";
    public static final Integer overtimeDays=7;
    /**
     * 每次查询，只能获取50个记录
     */
    public static final Integer QCCMaxNumberBySingleQuery_50=50;
    public static final double QCCMaxNumberBySingleQuery_50_double=50.0;

}

