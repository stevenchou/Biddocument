package com.weijun.util;

import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class DateTool {
    public Date StrToDate(String strDate){
        Date date=null;
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(strDate);

        }
        catch (ParseException e)
        {
            System.out.println(e.getMessage());
        }
        return date;
    }

    public String getCurrentFormatDateTime(){
        /**
         * create by: 周江
         * description: 获取格式化的时间,输出示例为：2015-08-04 20:55:35
         * create time: 5/29/2020 6:05 PM
         *
          * @Param:
         * @return java.lang.String
         */

        Date date = new Date();
        long times = date.getTime();//时间戳
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(date);
        return dateString;
    }

    public String getCurrentDate(){
        Date date = new Date();
        long times = date.getTime();//时间戳
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    public Date timestampToDate(long times){
        /**
         * create by: 周江
         * description: 将时间戳转化为标准时间，输出示例：Tue Oct 07 12:04:36 CST 2014
         * create time: 5/29/2020 6:06 PM
         *
          * @Param: times
         * @return java.util.Date
         */

        Date date = new Date(times);
        return date;
    }

    public long getCurrentDatestamp() {
        Date date = new Date();
        long times = date.getTime();//时间戳
        return times;
    }


    public boolean isOverTime(long currentStamp, long updateStamp, int i) {
        /**
         * @author: 周江
         * 判断两个stamp之间相差的时间是否超期
         *
          * @Param: currentStamp  通常是现在的stamp
         * @Param: updateStamp   通常是要对比的stamp
         * @Param: i  目标天数，判断两个stamp之间相差的时间是否超过这个天数
         * @return boolean 超期返回true，否则返回false。
         */

        long duration=currentStamp-updateStamp;
        if(duration>7*86400000){
            return true;
        }
        else return false;
    }
}
