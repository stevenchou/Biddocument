package com.weijun.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class SVNTool {
    @Autowired
    FilelTool filelTool;

    @Autowired
    DateTool dateTool;
    private static final Logger logger = LogManager.getLogger(SVNTool.class.getName());

    public boolean isPortUsing(String host,int port) throws UnknownHostException {
        /**
         * create by: 周江
         * description: 检测端口是否被占用
         * create time: 5/29/2020 6:13 PM
         *
          * @Param: host 主机IP
         * @Param: port 端口
         * @return boolean
         */

        boolean flag = false;
        InetAddress Address = InetAddress.getByName(host);
        try {
            Socket socket = new Socket(Address,port);  //建立一个Socket连接
            flag = true;
        } catch (IOException e) {

        }
        return flag;
    }


    public boolean isLocalPortUsing(int port){
        /**
         * create by: 周江
         * description: 检测本地端口是否被占用
         * create time: 5/30/2020 12:58 AM
         *
          * @Param: port 端口号
         * @return boolean
         */

        boolean flag = true;
        try {
            //如果该端口还在使用则返回true,否则返回false,127.0.0.1代表本机
            flag = isPortUsing("127.0.0.1", port);
        } catch (Exception e) {
        }
        return flag;
    }

    public boolean isSVNRunning(){
        boolean flag = false;
        //如果该端口还在使用则返回true,否则返回false,127.0.0.1代表本机
        try {
            flag = isPortUsing("127.0.0.1", 3690);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return flag;
    }

//    java调用服务器后台的bat脚本
    public void runbat(String batPath, String... argStrings) {
        String cmd = "cmd /c start /b " + batPath + " ";
        if (argStrings != null && argStrings.length > 0) {
            for (String string : argStrings) {
                cmd += string + " ";
            }
        }

        runCMD(cmd);
    }

    public String runCMD(String cmd){
        String str = null;
        try {
            Process ps = Runtime.getRuntime().exec(cmd);
            InputStream in = ps.getInputStream();

            //byte[] sb = readStream(in);
//            str = new String(sb, "UTF-8");
//            System.out.println(str);
            in.close();
            ps.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("\nchild thread done");

        return str;
    }

    public byte[] readStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = -1;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        return outSteam.toByteArray();
    }

//    将新加的文件添加到svn，特别注意，免去扫描，因为界面上一次只添加一个文件，可以直接将文件提交到svn服务器
    //projectBase是Biddocument\springmvcdemo\target\springmvcdemo
    public void svnConnitAddedFile(String batFileBase,String uploadBase)
    {
        logger.info("the batfile base is "+batFileBase);
        String svnGetAddedFileBat=batFileBase+File.separator+"getAllAdd.bat";
        //获取增加的文件清单
        String cmd=svnGetAddedFileBat+" "+uploadBase+" "+batFileBase;
        logger.info("get all the added file ,the cmd is "+cmd);
        runCMD(cmd);
        //add.mod的名字在getAllAdd.bat中
        File addmod=new File(batFileBase+File.separator+"add.mod");
        handleSVNStatusOutput(addmod);
    }


    public void svnConnitDeletedFile(String batFileBase,String uploadBase)
    {
        logger.info("the batfile base is "+batFileBase);
        String svnGetDeletedFileBat=batFileBase+File.separator+"getAllDel.bat";
        String cmd=svnGetDeletedFileBat+" "+uploadBase+" "+batFileBase;
        logger.info("get all the deleted file ,the cmd is "+cmd);
        runCMD(cmd);

        File delmod=new File(batFileBase+File.separator+"del.mod");
        handleSVNStatusOutput(delmod);
    }

    public void svnModifyFile(String batFileBase,String uploadBase){
        String svnModDeldFileBat=batFileBase+File.separator+"getModDel.bat";
//        //获取修改的文件清单
        String cmd=svnModDeldFileBat+" "+uploadBase+" "+batFileBase;
        logger.info("get all the modified file ,the cmd is "+cmd);
        runCMD(cmd);

        File moddelfile=new File(batFileBase+File.separator+"moddel.mod");

        handleSVNStatusOutput(moddelfile);
    }


    private void handleSVNStatusOutput(File moddelfile){
        try {
            FileInputStream fileInputStream = new FileInputStream(moddelfile);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "GBK");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line =null;
            logger.info("the content from moddel.mod is as below:");

            String cmd=null;
            while((line=bufferedReader.readLine())!=null)
            {
                logger.info(line);

                Pattern containsA = Pattern.compile("^A");
                Pattern containsD = Pattern.compile("^D");
                Pattern containsAdd = Pattern.compile("^\\?");
                Pattern containsDel = Pattern.compile("^!");

                Matcher mA = containsA.matcher(line);
                Matcher mD = containsD.matcher(line);
                Matcher mAdd = containsAdd.matcher(line);
                Matcher mDel = containsDel.matcher(line);

                if(mA.find()||mD.find()){
                    logger.info("line contains A or D,now commit to svn server");
                    String targetName=line.substring(7);
                    cmd="svn ci -m \""+dateTool.getCurrentFormatDateTime()+",add"+targetName+" to the svn server\" "+targetName;
                    logger.info("the cmd is :"+cmd);
                    runCMD(cmd);
                }
                if(mAdd.find()){
                    logger.info("line contains ？,now add to svn server");
                    String targetName=line.substring(7);
                    cmd="svn add "+targetName;
                    logger.info("the cmd is "+cmd);
                    runCMD(cmd);
                    cmd="svn ci -m \""+dateTool.getCurrentFormatDateTime()+",add"+targetName+" to the svn server\" "+targetName;
                    logger.info("the cmd is :"+cmd);
                    runCMD(cmd);
                }
                if(mDel.find()){
                    logger.info("line contains ！,now delete from svn server");
                    String targetName=line.substring(7);
                    cmd="svn del "+targetName;
                    logger.info("the cmd is "+cmd);
                    runCMD(cmd);
                    cmd="svn ci -m \""+dateTool.getCurrentFormatDateTime()+",delete"+targetName+" from the svn server\" "+targetName;
                    logger.info("the cmd is :"+cmd);
                    runCMD(cmd);
                }
                else{
                    logger.error("do not commit to svn server,it may cause some error");
                }

            };


            bufferedReader.close();
            inputStreamReader.close();
            fileInputStream.close();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
