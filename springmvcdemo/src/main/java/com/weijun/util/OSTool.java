package com.weijun.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class OSTool {
    private static final Logger logger = LogManager.getLogger(OSTool.class.getName());

    public String tomcatRoot(){
        String tomcatRoot=System.getenv("CATALINA_HOME")+"\\webapps\\ROOT";;
        logger.info(tomcatRoot);
        return tomcatRoot;
    }
}
