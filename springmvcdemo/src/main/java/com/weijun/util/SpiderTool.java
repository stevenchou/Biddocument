package com.weijun.util;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Service;

@Service
public class SpiderTool {
    public WebDriver startSpiderDriver(String driverPath){
        //设置chromedriver的地址
        System.setProperty("webdriver.chrome.driver", driverPath);
        //创建一个默认的浏览器
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();//窗口最大化
        return driver;
    }

    public Boolean isElementExist(WebDriver driver, String xpath){
        try {
            driver.findElement(By.xpath(xpath));
            return true;
        }catch (NoSuchElementException e){
            return false;
        }
    }
}
