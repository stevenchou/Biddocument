package com.weijun.util;

import com.weijun.entity.CaseProject;
import com.weijun.entity.OperateLog;
import com.weijun.service.LogService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
public class LogTool {

    @Autowired
    SecurityTool securityTool;

    @Autowired
    LogService logService;
    private static final Logger logger = LogManager.getLogger(LogTool.class.getName());


    public void writeOperateLog(String objectType,String objectName,OperateType operateType){
        String userName=securityTool.getCurrentUser();

        Date nowDate=new Date();

        OperateLog operateLog =new OperateLog();
        operateLog.setUsername(userName);
        operateLog.setOperatetime(nowDate);
        operateLog.setOperateobjecttype(objectType);
        operateLog.setOperateobjectname(objectName);
        operateLog.setOperateType(operateType);

        logService.writeUserLog(operateLog);
    }


    public void writeOperateLog(String objectType, List<String> objectNameList, OperateType operateType){
        for (String objectName:objectNameList
             ) {
            writeOperateLog(objectType,objectName,operateType);
        }
    }

    public void writeProjectsOperateLog(List<CaseProject> projectList,OperateType operateType){
        for (CaseProject caseProject:projectList
             ) {
            writeOperateLog(CaseProject.class.getName(),caseProject.getCasename(),operateType);
        }
    }

    public void writeProjectOperateLog(CaseProject project,OperateType operateType){
        writeOperateLog(CaseProject.class.getName(),project.getCasename(),operateType);
    }
}
