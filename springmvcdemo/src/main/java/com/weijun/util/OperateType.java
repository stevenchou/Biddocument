package com.weijun.util;

public enum OperateType {
    /**
     * 0 新增
     */
    create("新增",0),
    /**
     * 1 删除
     */
    delete("删除",1),
    /**
     * 2 修改,包括删除，添加部分文件等
     */
    modify("修改",2),
    /**
     * 3 查询
     */
    query("查询",3),
    /**
     * 4 更新
     */
    update("更新",4),

    fileCreate("创建文件",5),
    fileDelete("删除文件",6),
    fileUpdate("更新文件",7),
    fileOpen("打开文件",8),
    fileSave("保存文件",9),

    login("登录",10),
    logout("登出",11);
    private String value;

    private int index;

    private OperateType(String value, int index) {
        this.value = value;
        this.index = index;
    }

    public static OperateType get(String value){
        for (OperateType p : OperateType.values()) {
            if (p.getValue().equals(value)) {
                return p;
            }
        }
        return null;
    }

    public static OperateType get(int index){
        for (OperateType p : OperateType.values()) {
            if (p.getIndex() == index) {
                return p;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
