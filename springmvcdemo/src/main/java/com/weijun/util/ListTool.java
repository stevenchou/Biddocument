package com.weijun.util;

import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class ListTool {
    public Long[] listToArray(List<Long> longList){
        Long[] retArray=new Long[longList.size()];
        for (int i=0;i<longList.size();i++) {
            retArray[i]=longList.get(i).longValue();
        }
        return retArray;
    }

    public List<Long> listToLongList(List<BigInteger> bigIntegerList){
        List<Long> longList=new ArrayList<Long>();
        for (int i=0;i<bigIntegerList.size();i++) {
            longList.add(bigIntegerList.get(i).longValue());
        }
        return longList;
    }
}
