package com.weijun.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;

@Service
public class SecurityTool {
    private static final Logger logger = LogManager.getLogger(SVNTool.class.getName());

    public String getCurrentUser(){
        //get the current username
        String currentUserName= (String) SecurityUtils.getSubject().getPrincipal();
        logger.info("the current user is "+currentUserName);
        return currentUserName;
    }

}
