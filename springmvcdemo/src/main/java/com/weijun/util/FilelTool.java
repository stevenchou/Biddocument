package com.weijun.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

@Service
public class FilelTool {
    private static final Logger logger = LogManager.getLogger(FilelTool.class.getName());
    private MultipartFile file;


    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    //保存用户上传的文件至服务器
    public void saveFileToServer(MultipartFile multipartFile, String savePath) {

        //文件的原始名称
        String originalFilename = multipartFile.getOriginalFilename();
        String newFileName = null;
        if (originalFilename.length() > 0) {
            newFileName = savePath + originalFilename;
            //如果文件夹不存在则创建
            File file = new File(savePath);
            if (!file.exists()) {
                logger.info(savePath + " in server is not exists,now create it");
                file.mkdirs();
            }
            /**
             * 获取新文件的File实例,通过spring的org.springframework.web.multipartInterface MultipartFile
             * 下的transferTo方法,这个可以移动文件的文件系统,复制文件系统中的文件或内存内容保存到目标文件.
             * 如果目标文件已经存在,它将被删除。
             */
            //新文件路径实例
            try {
                FileCopyUtils.copy(multipartFile.getBytes(), new File(newFileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            //内存数据读入磁盘

        }
    }


    public String streamToString(InputStream inputStream) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        return result.toString("UTF-8");
    }
}
