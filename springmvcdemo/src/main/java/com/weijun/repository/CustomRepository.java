package com.weijun.repository;

import com.weijun.entity.HSCustom;
import com.weijun.entity.OperateLog;
import com.weijun.entity.RelativeCompany;

import java.util.List;

public interface CustomRepository extends DomainRepository<HSCustom,Long> {
    @Override
    default HSCustom load(Long id) {
        return null;
    }

    @Override
    default HSCustom get(Long id) {
        return null;
    }

    @Override
    default List<HSCustom> findAll() {
        return null;
    }

    @Override
    default void persist(HSCustom entity) {

    }

    @Override
    default void save(HSCustom entity) {

    }

    @Override
    default void saveOrUpdate(HSCustom entity) {

    }

    @Override
    default void delete(Long id) {

    }

    @Override
    default void flush() {

    }

//    @Override
//    default List findAllByHQL(String hql) {
//        return null;
//    }

    @Override
    default List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    default Object findObjectByHQL(String hql) {
        return null;
    }

    @Override
    default Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    default List findPage(final String hql, final int page, final int size, final Object[] args) {
        return null;
    }

    @Override
    default List findPage(final String hql, final int page, final int size) {
        return null;
    }

    @Override
    default void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    default void updateObjectByHQL(String hql) {

    }

    @Override
    default Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    default List findAllBySql(String sql) {
        return null;
    }

    List<OperateLog> customInLog(int topN);

    void updateCustom(HSCustom custom);

    void saveRelativeCompany(RelativeCompany relativeCompany);
}
