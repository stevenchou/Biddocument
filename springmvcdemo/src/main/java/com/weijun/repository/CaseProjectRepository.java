package com.weijun.repository;

import com.weijun.entity.CaseProject;
import com.weijun.entity.HSCustom;
import com.weijun.entity.ProjectField;

import java.util.Date;
import java.util.List;

public interface CaseProjectRepository extends DomainRepository<CaseProject,Long> {
    @Override
    default CaseProject load(Long id) {
        return null;
    }

    @Override
    default CaseProject get(Long id) {
        return null;
    }

    @Override
    default List<CaseProject> findAll() {
        return null;
    }

    @Override
    default void persist(CaseProject entity) {

    }

    @Override
    default void save(CaseProject entity) {

    }

    @Override
    default void saveOrUpdate(CaseProject entity) {

    }

    @Override
    default void delete(Long id) {

    }

    @Override
    default void flush() {

    }

//    @Override
//    default List findAllByHQL(String hql) {
//        return null;
//    }

    @Override
    default List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    default Object findObjectByHQL(String hql) {
        return null;
    }

    @Override
    default Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    default List findPage(final String hql, final int page, final int size, final Object[] args) {
        return null;
    }

    @Override
    default List findPage(final String hql, final int page, final int size) {
        return null;
    }

    @Override
    default void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    default void updateObjectByHQL(String hql) {

    }

    @Override
    default Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    default List findAllBySql(String sql) {
        return null;
    }

    void saveCustom(HSCustom custom);

    List<HSCustom> getCustomList();

    List<String> getProjectTypeList();

    void saveProjectField(ProjectField fields);

    List<Object> getProjectTypeListByNames(String hql, String[] projecttypes);

    void deleteProjectByid(long projectID);


    List<CaseProject> getprojectByYearAndType(List<Date> selectedYear, List<ProjectField> selectedType);

    List<ProjectField> getFieldsByname(List<String> selectedType);

    List<CaseProject> getMyProjects(String hql,List<Long> projectIDs);

    CaseProject getProjectByName(String projectName);

}
