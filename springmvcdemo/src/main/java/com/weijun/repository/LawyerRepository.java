package com.weijun.repository;

import com.weijun.entity.CertificateCard;
import com.weijun.entity.Lawyer;

import java.util.List;

public interface LawyerRepository extends DomainRepository<Lawyer,Long> {
    void saveLawyerCertificate(CertificateCard certificateCard);

    List<Object> getProjectTypeListByNames(String hql, String[] lawyerNames);
}
