package com.weijun.repository;
import java.io.Serializable;
import java.util.List;

public interface DomainRepository<T,PK extends Serializable>{

    T load(PK id);

    T get(PK id);

    List<T> findAll();

    void persist(T entity);

    //save new entity into DB
    void save(T entity);

    void saveOrUpdate(T entity);

    void delete(PK id);

    void flush();

    //HQL interface
    List findAllByHQL(String hql);

    List findAllByHQL(String hql, Object[] args);

    Object findObjectByHQL(String hql);

    Object findObjectByHQL(String hql, Object[] args);

    List findPage(final String hql,final int page,final int size,final Object[] args);

    List findPage(final String hql, final int page, final int size);

    void updateObjectByHQL(String hql, Object[] params);

    void updateObjectByHQL(String hql);
    Object findObjectBySQL(String sql);

    List findAllBySql(String sql);
}
