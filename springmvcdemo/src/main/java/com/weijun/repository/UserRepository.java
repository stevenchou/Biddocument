package com.weijun.repository;

import com.weijun.entity.User;

public interface UserRepository extends DomainRepository<User,Long>  {

}
