package com.weijun.repository.impl;

import com.weijun.entity.CertificateCard;
import com.weijun.entity.Lawyer;
import com.weijun.repository.LawyerRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class LawyerRepositoryImpl implements LawyerRepository{
    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = LogManager.getLogger(LawyerRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public Lawyer load(Long id) {
        return (Lawyer) getCurrentSession().load(Lawyer.class,id);
    }

    @Override
    public Lawyer get(Long id) {
        return (Lawyer) getCurrentSession().load(Lawyer.class,id);
    }

    @Override
    public List<Lawyer> findAll() {
        List<Lawyer> list = null;
        Session session = null;
        try {
            session = getCurrentSession();
            Criteria criteria=session.createCriteria(Lawyer.class);
            list= criteria.list();

            for (Lawyer lawyer : list) {
                System.out.println(lawyer);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void persist(Lawyer entity) {
        getCurrentSession().persist(entity);
    }

    @Override
    public void save(Lawyer entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(Lawyer entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(Long id) {
        Lawyer lawyer= load(id);
        getCurrentSession().delete(lawyer);
    }

    @Override
    public void flush() {
        getCurrentSession().flush();
    }

    @Override
    public List findAllByHQL(String hql) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        return query.list();
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        for (int i = 0; i < args.length; i++) {
            logger.info(args[i]);
            query.setParameter(i, args[i]);
        }
        return query.list();
    }

    @Override
    public Object findObjectByHQL(String hql) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        System.out.println(args[0]);
        for (int i = 0; i < args.length; i++) {
            logger.info(args[i]);
            query.setParameter(i, args[i]);
        }
        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        for (int i = 0; i < args.length; i++) {
            query.setParameter(i, args[i]);
        }
        query.setMaxResults(size);
        query.setFirstResult(page);
        List<Object> list = query.list();
        return list;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setMaxResults(size);
        query.setFirstResult(page);
        List<Object> list = query.list();
        return list;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        query.executeUpdate();
    }

    @Override
    public void updateObjectByHQL(String hql) {
        sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
    }

    @Override
    public Object findObjectBySQL(String sql) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        return query.list();
    }

    @Override
    public void saveLawyerCertificate(CertificateCard certificateCard) {
        getCurrentSession().save(certificateCard);
    }

    @Override
    public List<Object> getProjectTypeListByNames(String hql, String[] lawyerNames) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameterList("strs",lawyerNames);
        List list = query.list();
        return list;
    }
}
