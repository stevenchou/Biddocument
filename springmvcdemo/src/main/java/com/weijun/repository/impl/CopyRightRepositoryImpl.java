package com.weijun.repository.impl;

import com.weijun.entity.IP.CopyRight;
import com.weijun.repository.CopyRightRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <h3>springmvcdemo</h3>
 * <p>操作著作权数据库</p>
 *
 * @author : 周江
 * @date : 2020-06-15 00:35
 **/

@Repository
@Transactional
public class CopyRightRepositoryImpl implements CopyRightRepository {
    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = LogManager.getLogger(CopyRightRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }
    @Override
    public CopyRight load(Long id) {
        return null;
    }

    @Override
    public CopyRight get(Long id) {
        return null;
    }

    @Override
    public List<CopyRight> findAll() {
        return null;
    }

    @Override
    public void persist(CopyRight entity) {

    }

    @Override
    public void save(CopyRight entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(CopyRight entity) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {
        Query query = getCurrentSession().createQuery(hql);
        List<Object> list = query.list();

        return list;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        return null;
    }
}
