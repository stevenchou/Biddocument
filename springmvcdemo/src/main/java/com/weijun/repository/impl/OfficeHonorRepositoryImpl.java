package com.weijun.repository.impl;

import com.weijun.entity.OfficeHonor;
import com.weijun.repository.OfficeHonorRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class OfficeHonorRepositoryImpl implements OfficeHonorRepository{
    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = LogManager.getLogger(OfficeHonorRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public OfficeHonor load(Long id) {
        return (OfficeHonor) getCurrentSession().load(OfficeHonor.class,id);
    }

    @Override
    public OfficeHonor get(Long id) {
        return null;
    }

    @Override
    public List<OfficeHonor> findAll() {
        List<OfficeHonor> list = null;
        Session session = null;
        try {
            session = getCurrentSession();
            Criteria criteria=session.createCriteria(OfficeHonor.class);
            list= criteria.list();

            for (OfficeHonor officeHonor : list) {
                System.out.println(officeHonor);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void persist(OfficeHonor entity) {
        getCurrentSession().persist(entity);
    }

    @Override
    public void save(OfficeHonor entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(OfficeHonor entity) {

    }

    @Override
    public void delete(Long id) {
        OfficeHonor officeHonor= load(id);
        getCurrentSession().delete(officeHonor);
    }

    @Override
    public void flush() {
        getCurrentSession().flush();

    }

    @Override
    public List findAllByHQL(String hql) {
        return null;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        return null;
    }
}
