package com.weijun.repository.impl;

import com.weijun.entity.CaseProject;
import com.weijun.entity.HSCustom;
import com.weijun.entity.ProjectField;
import com.weijun.repository.CaseProjectRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class CaseProjectRepositoryImpl implements CaseProjectRepository {

    @Autowired
    private SessionFactory sessionFactory;
    private static final Logger logger = LogManager.getLogger(LawyerRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public void saveCustom(HSCustom custom) {
        getCurrentSession().save(custom);
    }

    @Override
    public List<HSCustom> getCustomList() {
        List<HSCustom> list = null;
        Session session = null;
        try {
            session = getCurrentSession();
            Criteria criteria=session.createCriteria(HSCustom.class);
            list= criteria.list();

            for (HSCustom custom : list) {
                System.out.println(custom);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<String> getProjectTypeList() {
        return null;
    }

    @Override
    public void saveProjectField(ProjectField fields) {
        getCurrentSession().save(fields);
    }

    @Override
    public List<Object> getProjectTypeListByNames(String hql, String[] projecttypes) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameterList("strs",projecttypes);
        List list = query.list();
        return list;
    }


    @Override
    public List findAllBySql(String sql) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        return query.list();
    }

    @Override
    public Object findObjectBySQL(String sql) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public void save(CaseProject entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public List<CaseProject> findAll() {
        List<CaseProject> list = null;
        Session session = null;
        try {
            session = getCurrentSession();
            Criteria criteria=session.createCriteria(CaseProject.class);
            list= criteria.list();

            for (CaseProject project : list) {
                logger.info(project);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Object findObjectByHQL(String hql) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public void deleteProjectByid(long projectID) {
        CaseProject project= load(projectID);
        getCurrentSession().delete(project);
    }

    @Override
    public CaseProject load(Long id) {
        return (CaseProject) getCurrentSession().load(CaseProject.class,id);
    }

    @Override
    public CaseProject get(Long id) {
        String hql="from CaseProject project where project.caseprojectid ='"+id+"'";
        return (CaseProject) findObjectByHQL(hql);
    }

    @Override
    public void persist(CaseProject entity) {

    }

    @Override
    public void saveOrUpdate(CaseProject entity) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {
        Query query = getCurrentSession().createQuery(hql);
        List<Object> list = query.list();

        return list;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {

        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        for (int i = 0; i < args.length; i++) {
            logger.info(args[i]);
            query.setParameter(i, args[i]);
        }
        return query.list();

    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public List<CaseProject> getprojectByYearAndType(List<Date> selectedYear, List<ProjectField> selectedFields) {
//        首先根据fields选出符合条件的项目，
        String fieldsIDs ="";
        for (ProjectField fields:selectedFields
             ) {
            fieldsIDs+=fields.getId().toString()+",";
        }
        fieldsIDs+="0";
        String sql="select casename from t_caseproject where caseprojectid in (select distinct id from `t_project_fields` where projectfields_id in ("+fieldsIDs+")) ORDER BY caseprojectid";
        List<String> projectsNames =  findAllBySql(sql);
//        Long[] longs =  null;
//                projectsIDs.toArray(longs);

        // 获取Criteria实例对象
        Criteria criteria = getCurrentSession().createCriteria(CaseProject.class);
        List projects = criteria.add(Restrictions.in("casename", projectsNames))
                .add(Restrictions.and(Restrictions.in("signaturedate",selectedYear)))
                .list();
        return projects;
    }

    @Override
    public List<ProjectField> getFieldsByname(List<String> selectedType) {
        // 获取Criteria实例对象
        Criteria criteria = getCurrentSession().createCriteria(ProjectField.class);
        List fields = criteria.add(Restrictions.in("projettypename", selectedType)).list();
        return fields;
    }

    @Override
    public List<CaseProject> getMyProjects(String hql,List<Long> projectIDs) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setParameterList("strs",projectIDs);
        List<Object> list = query.list();
        List<CaseProject> retList = new ArrayList<>();
        for (Object project:list) {
            retList.add((CaseProject) project);
        }
        return retList;
    }

    @Override
    public CaseProject getProjectByName(String projectName) {
        String hql="from CaseProject project where project.casename ='"+projectName+"'";
        return (CaseProject) findObjectByHQL(hql);
    }

}
