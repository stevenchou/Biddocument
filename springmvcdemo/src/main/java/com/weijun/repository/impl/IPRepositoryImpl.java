package com.weijun.repository.impl;

import com.weijun.entity.IP.TradeMark;
import com.weijun.repository.IPRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <h3>springmvcdemo</h3>
 * <p>知识产权相关数据库操作</p>
 *
 * @author : 周江
 * @date : 2020-06-08 17:25
 **/

@Repository
@Transactional
public class IPRepositoryImpl implements IPRepository {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = LogManager.getLogger(IPRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public TradeMark load(Long id) {
        return null;
    }

    @Override
    public TradeMark get(Long id) {
        return null;
    }

    @Override
    public List<TradeMark> findAll() {
        return null;
    }

    @Override
    public void persist(TradeMark entity) {

    }

    @Override
    public void save(TradeMark entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(TradeMark entity) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {
        Query query = getCurrentSession().createQuery(hql);
        List<Object> list = query.list();

        return list;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        return null;
    }

}
