package com.weijun.repository.impl;

import com.weijun.entity.HSCustom;
import com.weijun.entity.OperateLog;
import com.weijun.entity.RelativeCompany;
import com.weijun.repository.CustomRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class CustomRepositoryImpl implements CustomRepository {
    @Autowired
    private SessionFactory sessionFactory;
    private static final Logger logger = LogManager.getLogger(CustomRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }


    @Override
    public HSCustom load(Long id) {
        return (HSCustom) getCurrentSession().load(HSCustom.class,id);
    }

    @Override
    public HSCustom get(Long id) {
        String hql="from HSCustom custom where custom.customid ='"+id+"'";
        return (HSCustom) findObjectByHQL(hql);
    }

    @Override
    public List<HSCustom> findAll() {
        List<HSCustom> list = new ArrayList<HSCustom>();
        Session session = null;
        try {
            session = getCurrentSession();
            Criteria criteria=session.createCriteria(HSCustom.class);
            list= criteria.list();
            for (HSCustom custom : list) {
                logger.info(custom);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void persist(HSCustom entity) {

    }

    @Override
    public void save(HSCustom entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(HSCustom entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {
        Query query = getCurrentSession().createQuery(hql);
        List<Object> list = query.list();

        return list;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        return null;
    }

    @Override
    public List<OperateLog> customInLog(int topN) {
        Session session = getCurrentSession();
        Criteria criteria=session.createCriteria(OperateLog.class);
        criteria.add(Restrictions.eq("operateobjecttype",HSCustom.class.getName()));
        criteria.setMaxResults(topN);
        List<OperateLog> operateLogList= criteria.list();

        return  operateLogList;
    }

    @Override
    public void updateCustom(HSCustom custom) {
        getCurrentSession().update(custom);
    }

    @Override
    public void saveRelativeCompany(RelativeCompany relativeCompany) {
        getCurrentSession().save(relativeCompany);
    }
}
