package com.weijun.repository.impl;

import com.weijun.entity.UserRole;
import com.weijun.repository.UserRoleRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class UserRoleRepositoryImpl implements UserRoleRepository {
    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = LogManager.getLogger(UserRoleRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }
    @Override
    public UserRole load(Long id) {
        return null;
    }

    @Override
    public UserRole get(Long id) {
        return null;
    }

    @Override
    public List<UserRole> findAll() {
        List<UserRole> list = null;
        Session session = null;
        try {
            session = getCurrentSession();
            Criteria criteria=session.createCriteria(UserRole.class);
            list= criteria.list();

            for (UserRole userRole : list) {
                logger.info(userRole);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void persist(UserRole entity) {

    }

    @Override
    public void save(UserRole entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(UserRole entity) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {
        return null;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        return null;
    }
}
