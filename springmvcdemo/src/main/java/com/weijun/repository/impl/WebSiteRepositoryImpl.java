package com.weijun.repository.impl;

import com.weijun.entity.lawfirm.CNInfoAchievement;
import com.weijun.repository.WebSiteRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <h3>springmvcdemo</h3>
 * <p>处理网站信息</p>
 *
 * @author : 周江
 * @date : 2020-06-21 21:16
 **/
@Repository
@Transactional
public class WebSiteRepositoryImpl implements WebSiteRepository {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = LogManager.getLogger(WebSiteRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }
    @Override
    public CNInfoAchievement load(Long id) {
        return null;
    }

    @Override
    public CNInfoAchievement get(Long id) {
        return null;
    }

    @Override
    public List<CNInfoAchievement> findAll() {
        return null;
    }

    @Override
    public void persist(CNInfoAchievement entity) {

    }

    @Override
    public void save(CNInfoAchievement entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(CNInfoAchievement entity) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {
        return null;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        return null;
    }
}
