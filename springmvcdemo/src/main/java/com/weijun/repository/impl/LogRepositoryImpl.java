package com.weijun.repository.impl;

import com.weijun.entity.OperateLog;
import com.weijun.repository.LogRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class LogRepositoryImpl implements LogRepository {

    @Autowired
    private SessionFactory sessionFactory;
    private static final Logger logger = LogManager.getLogger(LogRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public OperateLog load(Long id) {
        return null;
    }

    @Override
    public OperateLog get(Long id) {
        return null;
    }

    @Override
    public List findAll() {
        return null;
    }

    @Override
    public void persist(OperateLog entity) {

    }

    @Override
    public void save(OperateLog entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(OperateLog entity) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {
        return null;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public OperateLog findObjectByHQL(String hql) {
        return null;
    }

    @Override
    public OperateLog findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public OperateLog findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        return null;
    }
}
