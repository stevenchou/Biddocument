package com.weijun.repository.impl;

import com.weijun.entity.Reimburse;
import com.weijun.repository.ReimburseRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
@Repository
@Transactional
public class ReimburseRepositoryImpl implements ReimburseRepository {
    @Autowired
    private SessionFactory sessionFactory;
    private static final Logger logger = LogManager.getLogger(ReimburseRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public Reimburse load(Long id) {
        return (Reimburse) getCurrentSession().load(Reimburse.class,id);
    }

    @Override
    public Reimburse get(Long id) {
        return null;
    }

    @Override
    public List<Reimburse> findAll() {
        return null;
    }

    @Override
    public void persist(Reimburse entity) {

    }

    @Override
    public void save(Reimburse entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(Reimburse entity) {

    }

    @Override
    public void delete(Long id) {
        Reimburse reimburse= load(id);
        getCurrentSession().delete(reimburse);
    }

    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {

        Query query = getCurrentSession().createQuery(hql);
        List<Object> list = query.list();

        return list;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {
        sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
    }

    @Override
    public Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {

        Query query = getCurrentSession().createSQLQuery(sql);
        return query.list();
    }

    @Override
    public List<Reimburse> queryReimbursesByLawyerNameAndState(String hql, List<String> selectedLawyerNameList, List<String> reimburseStateList, List<Date> selectedDateList) {
        Query query=getCurrentSession().createQuery(hql);
        query.setParameterList("selectedLawyerNameList", selectedLawyerNameList);
        query.setParameterList("reimburseStateList",reimburseStateList);
        query.setParameterList("selectedDateList",selectedDateList);
        List<Reimburse> retList = query.list();
        return  retList;
    }
}
