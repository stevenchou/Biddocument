package com.weijun.repository.impl;

import com.weijun.entity.User;
import com.weijun.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Repository
@Transactional
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private SessionFactory sessionFactory;

    private static final Logger logger = LogManager.getLogger(UserRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public User load(Long id) {
        return (User) getCurrentSession().load(User.class,id);
    }

    @Override
    public User get(Long id) {
        return (User) getCurrentSession().get(User.class,id);
    }

    @Override
    public List<User> findAll() {


        List<User> list = null;
        Session session = null;
        try {
            session = getCurrentSession();
            Criteria criteria=session.createCriteria(User.class);
            list= criteria.list();

            for (User user : list) {
                logger.info(user);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void persist(User entity) {
        getCurrentSession().persist(entity);
    }

    @Override
    public void save(User entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(User entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(Long id) {
        User user = load(id);
        getCurrentSession().delete(user);
    }

    @Override
    public void flush() {
        getCurrentSession().flush();
    }

    public List findAllByHQL(String hql) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        return query.list();
    }

    public List findAllByHQL(String hql, Object[] args) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        for (int i = 0; i < args.length; i++) {
            logger.info(args[i]);
            query.setParameter(i, args[i]);
        }
        return query.list();
    }


    public Object findObjectByHQL(String hql) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public Object findObjectByHQL(String hql, Object[] args) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        System.out.println(args[0]);
        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
            query.setParameter(i, args[i]);
        }
        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public List findPage(final String hql,final int page,final int size,final Object[] args) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        for (int i = 0; i < args.length; i++) {
            query.setParameter(i, args[i]);
        }
        query.setMaxResults(size);
        query.setFirstResult(page);
        List<Object> list = query.list();
        return list;
    }

    public List findPage(final String hql, final int page, final int size) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        query.setMaxResults(size);
        query.setFirstResult(page);
        List<Object> list = query.list();
        return list;
    }

    public void delObject(Object object) {
        sessionFactory.getCurrentSession().delete(object);
    }


    public void updateObject(Object object) {
        sessionFactory.getCurrentSession().update(object);
    }


    public void updateObjectByHQL(String hql) {
        sessionFactory.getCurrentSession().createQuery(hql).executeUpdate();
    }


    public Object findObjectBySQL(String sql) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);

        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public List findAllBySql(String sql) {
        Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        return query.list();
    }

    public void updateObjectByHQL(String hql, Object[] params) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        query.executeUpdate();
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
