package com.weijun.repository.impl;

import com.weijun.entity.MailPasswd;
import com.weijun.repository.MailRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class MailRepositoryImpl implements MailRepository {
    @Autowired
    private SessionFactory sessionFactory;
    private static final Logger logger = LogManager.getLogger(MailRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public MailPasswd load(Long id) {
        return (MailPasswd) getCurrentSession().load(MailPasswd.class,id);
    }

    @Override
    public MailPasswd get(Long id) {
        return (MailPasswd) getCurrentSession().load(MailPasswd.class,id);
    }

    @Override
    public List<MailPasswd> findAll() {
        return null;
    }

    @Override
    public void persist(MailPasswd entity) {
        getCurrentSession().persist(entity);
    }

    @Override
    public void save(MailPasswd entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(MailPasswd entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {
        return null;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql) {
        Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List list = query.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public Object findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public Object findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        return null;
    }
}
