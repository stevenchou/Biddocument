package com.weijun.repository.impl;

import com.weijun.entity.FileAttributeType;
import com.weijun.repository.BidDocuRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class BidDocuRepositoryImpl implements BidDocuRepository {
    @Autowired
    private SessionFactory sessionFactory;
    private static final Logger logger = LogManager.getLogger(LawyerRepositoryImpl.class.getName());
    private Session getCurrentSession() {
        return this.sessionFactory.getCurrentSession();
    }
    @Override
    public FileAttributeType load(Long id) {
        return (FileAttributeType) getCurrentSession().load(FileAttributeType.class,id);
    }

    @Override
    public FileAttributeType get(Long id) {
        return null;
    }

    @Override
    public List<FileAttributeType> findAll() {
        List<FileAttributeType> list = null;
        Session session = null;
        try {
            session = getCurrentSession();
            Criteria criteria=session.createCriteria(FileAttributeType.class);
            list= criteria.list();

            for (FileAttributeType attributeType : list) {
                logger.info(attributeType);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void persist(FileAttributeType entity) {

    }

    @Override
    public void save(FileAttributeType entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(FileAttributeType entity) {

    }

    @Override
    public void delete(Long id) {

    }


    @Override
    public void flush() {

    }

    @Override
    public List findAllByHQL(String hql) {
        return null;
    }

    @Override
    public List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public FileAttributeType findObjectByHQL(String hql) {
        return null;
    }

    @Override
    public FileAttributeType findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size, Object[] args) {
        return null;
    }

    @Override
    public List findPage(String hql, int page, int size) {
        return null;
    }

    @Override
    public void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    public void updateObjectByHQL(String hql) {

    }

    @Override
    public FileAttributeType findObjectBySQL(String sql) {
        return null;
    }

    @Override
    public List findAllBySql(String sql) {
        return null;
    }

    @Override
    public void newFileAttrType(FileAttributeType fileAttributeType) {
        getCurrentSession().save(fileAttributeType);
    }

    @Override
    public List<FileAttributeType> getAllFileAttributeType() {
        return findAll();
    }
}
