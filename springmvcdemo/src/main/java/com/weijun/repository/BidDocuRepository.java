package com.weijun.repository;

import com.weijun.entity.FileAttributeType;

import java.util.List;

public interface BidDocuRepository extends DomainRepository<FileAttributeType,Long> {
    void newFileAttrType(FileAttributeType fileAttributeType);



    @Override
    default List findAll() {
        return null;
    }

    @Override
    default void persist(FileAttributeType entity) {

    }

    @Override
    default void save(FileAttributeType entity) {

    }


    @Override
    default void flush() {

    }

//    @Override
//    default List findAllByHQL(String hql) {
//        return null;
//    }

    @Override
    default List findAllByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    default FileAttributeType findObjectByHQL(String hql) {
        return null;
    }

    @Override
    default FileAttributeType findObjectByHQL(String hql, Object[] args) {
        return null;
    }

    @Override
    default List findPage(final String hql, final int page, final int size, final Object[] args) {
        return null;
    }

    @Override
    default List findPage(final String hql, final int page, final int size) {
        return null;
    }

    @Override
    default void updateObjectByHQL(String hql, Object[] params) {

    }

    @Override
    default void updateObjectByHQL(String hql) {

    }

    @Override
    default FileAttributeType findObjectBySQL(String sql) {
        return null;
    }

    @Override
    default List findAllBySql(String sql) {
        return null;
    }

    List<FileAttributeType> getAllFileAttributeType();
}
