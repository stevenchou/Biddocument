package com.weijun.repository;

import com.weijun.entity.UserRole;

public interface UserRoleRepository extends DomainRepository<UserRole,Long> {
}
