package com.weijun.repository;

import com.weijun.entity.OfficeHonor;

public interface OfficeHonorRepository extends DomainRepository<OfficeHonor,Long> {
}
