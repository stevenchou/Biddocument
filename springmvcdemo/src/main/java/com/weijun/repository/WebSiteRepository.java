package com.weijun.repository;

import com.weijun.entity.lawfirm.CNInfoAchievement;

public interface WebSiteRepository extends DomainRepository<CNInfoAchievement,Long> {
}
