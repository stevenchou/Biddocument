package com.weijun.repository;

import com.weijun.entity.Reimburse;

import java.util.Date;
import java.util.List;

public interface ReimburseRepository extends DomainRepository<Reimburse,Long>{

    List<Reimburse> queryReimbursesByLawyerNameAndState(String hql, List<String> selectedLawyerNameList, List<String> reimburseStateList, List<Date> selectedDateList);
}
