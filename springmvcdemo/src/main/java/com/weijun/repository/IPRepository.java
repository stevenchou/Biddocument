package com.weijun.repository;

import com.weijun.entity.IP.TradeMark;

/**
 * <h3>springmvcdemo</h3>
 * <p></p>
 *
 * @author : 周江
 * @date : 2020-06-08 17:18
 **/
public interface IPRepository extends DomainRepository<TradeMark,Long> {
}

