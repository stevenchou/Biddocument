package com.weijun.service;

import com.weijun.entity.HSCustom;
import com.weijun.entity.RelativeCompany;

import java.util.List;

public interface CustomService {
    void saveCustom(HSCustom hsCustom);

    List<HSCustom> getAllCustom();

    List<HSCustom> getFrequentCustom(int topN);

    HSCustom getCustomByID(Long customID);

    HSCustom getCustomByName(String customName);

    List<HSCustom> getAllPersonalCustom();

    void updateCustom(HSCustom custom);

    void saveOrUpdateCustom(HSCustom custom);

    void saveRelativeCompany(RelativeCompany relativeCompany);
}
