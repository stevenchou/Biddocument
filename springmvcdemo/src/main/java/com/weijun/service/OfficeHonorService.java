package com.weijun.service;

import com.weijun.entity.Lawyer;
import com.weijun.entity.OfficeHonor;

import java.util.List;

public interface OfficeHonorService {
    void saveOfficeHonor(OfficeHonor officeHonor);

    List<OfficeHonor> getAllOfficeHonorList();

    void deleteOfficeHonorByid(Long officehonorid);
}
