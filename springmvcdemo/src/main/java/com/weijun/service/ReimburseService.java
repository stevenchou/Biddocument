package com.weijun.service;

import com.weijun.entity.Reimburse;

import java.util.List;

public interface ReimburseService {
    void saveReimburse(Reimburse reimburse);

    List<Reimburse> getMyReimburseData(String currentLawyerName);

    void deleteReimburseByid(long reimburseID);

    List<Reimburse> getALLSubmittedReimburseData();

    void approveReimburseByid(long reimburseID);

    List<Reimburse> queryReimburses(List<String> selectedLawyerNameList, List<String> reimburseStateList, String beginDate, String endDate);
}
