package com.weijun.service;

import com.weijun.entity.CertificateCard;
import com.weijun.entity.Lawyer;

import java.math.BigInteger;
import java.util.List;

public interface LawyerService {
    Long saveLawyer();

    void addLawyer(Lawyer lawyer);
    List<Lawyer> getAllLawyerList();

    void deleteLawyerByid(Long lawyerid);
    List<Lawyer> parterwithsocialposition();

    Lawyer getLawyerByName(String lawyername);

    void updateLawyer(Lawyer lawyer);

    void updateLawyercertificate(CertificateCard certificateCard);

    void updateLawyerModifyTime(List<Lawyer> lawyerList);

    void updateLawyerModifyTime(String lawyerName);

    void updateLawyerModifyTime(Long lawyerID);

    List<BigInteger> getLawyerNameBySocialPosition();

    List<Lawyer> getLawyersBySocialPosition(List<BigInteger> lawyerNameHasSocialPosition);
}
