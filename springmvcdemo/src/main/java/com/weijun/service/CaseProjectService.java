package com.weijun.service;

import com.weijun.entity.CaseProject;
import com.weijun.entity.HSCustom;
import com.weijun.entity.Lawyer;
import com.weijun.entity.ProjectField;

import java.util.Date;
import java.util.List;

public interface CaseProjectService {
    void addCustom(HSCustom custom);

    List<HSCustom> getCustomList();

    List<String> getProjectTypeList();

    void addProjectField(ProjectField fields);

    HSCustom getCustombyName(String customName);

    List<ProjectField> getProjectTypeListByNames(String[] projecttypes);

    void save(CaseProject caseProject);

    List<CaseProject> getAllProject();

    void deleteProjectByid(long l);

    List<CaseProject> getprojectByYearAndType(List<Date> selectedYear, List<String> selectedType);

    List<CaseProject> getMyProjects(Lawyer lawyer);

    CaseProject getProjectByID(Long projectid);

    CaseProject getProjectByName(String projectName);


    String getProjectNameByFileName(String filename);

    List<CaseProject> getprojectByYearAndType(String projectBeginDate, String projectEndDate, List<String> typeList);
}
