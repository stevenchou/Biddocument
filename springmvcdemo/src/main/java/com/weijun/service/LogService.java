package com.weijun.service;

import com.weijun.entity.OperateLog;

public interface LogService {
    void writeUserLog(OperateLog operateLog);
}
