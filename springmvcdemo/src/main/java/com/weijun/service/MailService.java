package com.weijun.service;

import com.weijun.entity.MailPasswd;

public interface MailService {
    void saveMailPass(MailPasswd mailPasswd);

    MailPasswd getMailPasswdByLawyerName(String lawyername);
}
