package com.weijun.service;

import com.weijun.entity.FileAttributeType;

import java.util.List;

public interface BidDocuService {
    void newFileAttrType(FileAttributeType fileAttributeType);

    List<FileAttributeType> getAllFileAttributeType();
}
