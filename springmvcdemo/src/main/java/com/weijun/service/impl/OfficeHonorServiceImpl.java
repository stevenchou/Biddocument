package com.weijun.service.impl;

import com.weijun.entity.Lawyer;
import com.weijun.entity.OfficeHonor;
import com.weijun.repository.OfficeHonorRepository;
import com.weijun.service.OfficeHonorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfficeHonorServiceImpl implements OfficeHonorService {
    @Autowired
    private OfficeHonorRepository officeHonorRepository;
    @Override
    public void saveOfficeHonor(OfficeHonor officeHonor) {
        officeHonorRepository.save(officeHonor);
    }


    @Override
    public List<OfficeHonor> getAllOfficeHonorList() {
        return officeHonorRepository.findAll();
    }

    @Override
    public void deleteOfficeHonorByid(Long officehonorid) {
        officeHonorRepository.delete(officehonorid);

    }
}
