package com.weijun.service.impl;

import com.weijun.entity.Reimburse;
import com.weijun.repository.DomainRepository;
import com.weijun.repository.ReimburseRepository;
import com.weijun.service.ReimburseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ReimburseServiceImpl implements ReimburseService {
    @Autowired
    ReimburseRepository reimburseRepository;
    @Override
    public void saveReimburse(Reimburse reimburse) {
        reimburseRepository.save(reimburse);
    }

    @Override
    public List<Reimburse> getMyReimburseData(String currentLawyerName) {
        String hql="FROM Reimburse AS reimburse where reimburse.applicantName ='"+currentLawyerName+"' AND reimburse.state='submitted' order by reimburse.generateDate asc";
        List<Reimburse> retList= reimburseRepository.findAllByHQL(hql);

        return retList;
    }

    @Override
    public void deleteReimburseByid(long reimburseID) {
//        String hql = "Delete FROM Reimburse Where id="+reimburseID;
        reimburseRepository.delete(reimburseID);
    }

    @Override
    public List<Reimburse> getALLSubmittedReimburseData() {
        String hql="FROM Reimburse AS reimburse where reimburse.state='submitted'";
        List<Reimburse> retList=reimburseRepository.findAllByHQL(hql);

        return retList;
    }

    @Override
    public void approveReimburseByid(long reimburseID) {
        String hql="update Reimburse reimburse set reimburse.state='approved' where reimburse.id ="+reimburseID;
        reimburseRepository.updateObjectByHQL(hql);
    }

    @Override
    public List<Reimburse> queryReimburses(List<String> selectedLawyerNameList, List<String> reimburseStateList, String beginDate, String endDate) {
        List<Date> selectedDateList=getDatesByBeginAndEnd(beginDate,endDate);
        String hql="FROM Reimburse AS reimburse WHERE reimburse.applicantName IN (:selectedLawyerNameList) AND reimburse.state IN(:reimburseStateList)"+
                " AND reimburse.applyDate IN (:selectedDateList) ORDER BY reimburse.applicantName,reimburse.generateDate asc";
        List<Reimburse> reimburseList=reimburseRepository.queryReimbursesByLawyerNameAndState(hql,selectedLawyerNameList, reimburseStateList,selectedDateList);
        return reimburseList;
    }

    private List<Date> getDatesByBeginAndEnd(String beginDate, String endDate) {
        String sql="SELECT DISTINCT applyDate FROM t_reimburse WHERE applyDate BETWEEN'"+beginDate+"' AND '"+endDate+"'";
        List<Date> dateList=reimburseRepository.findAllBySql(sql);
        return dateList;
    }
}
