package com.weijun.service.impl;

import com.weijun.service.TestService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TestServiceImpl implements TestService{
    public String test(){
        return "welcome";
    }
}
