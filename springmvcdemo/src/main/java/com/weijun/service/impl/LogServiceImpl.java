package com.weijun.service.impl;

import com.weijun.entity.OperateLog;
import com.weijun.repository.LogRepository;
import com.weijun.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    LogRepository logRepository;
    public void writeUserLog(OperateLog operateLog){
        logRepository.save(operateLog);
    }
}
