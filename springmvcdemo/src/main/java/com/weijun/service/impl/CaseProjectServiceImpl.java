package com.weijun.service.impl;

import com.weijun.entity.CaseProject;
import com.weijun.entity.HSCustom;
import com.weijun.entity.Lawyer;
import com.weijun.entity.ProjectField;
import com.weijun.repository.CaseProjectRepository;
import com.weijun.service.CaseProjectService;
import com.weijun.util.DateTool;
import com.weijun.util.ListTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Service
public class CaseProjectServiceImpl implements CaseProjectService {
    @Autowired
    private CaseProjectRepository caseProjectRepository;

    @Override
    public void addCustom(HSCustom custom) {
        caseProjectRepository.saveCustom(custom);
    }

    @Override
    public List<HSCustom> getCustomList() {
        return caseProjectRepository.getCustomList();
    }

    @Autowired
    private ListTool listTool;

    @Autowired
    private DateTool dateTool;

    private static final Logger logger = LogManager.getLogger(CaseProjectServiceImpl.class.getName());
    @Override
    public List<String> getProjectTypeList() {
        String sql="select projettypename from t_projectfields";
        List<String> projecttypes=caseProjectRepository.findAllBySql(sql);
        return projecttypes;
    }

    @Override
    public void addProjectField(ProjectField fields) {
        caseProjectRepository.saveProjectField(fields);
    }

    @Override
    public HSCustom getCustombyName(String customName) {
        String hql="from HSCustom custom where custom.customname ='"+customName+"'";
        return (HSCustom) caseProjectRepository.findObjectByHQL(hql);
    }

    @Override
    public List<ProjectField> getProjectTypeListByNames(String[] projecttypes) {
        String hql="from ProjectField fields where fields.projettypename in (:strs)";
        List<Object> fieldsList=caseProjectRepository.getProjectTypeListByNames(hql,projecttypes);
        List<ProjectField> retList = new ArrayList<>();
        for (Object projecttype:fieldsList) {
            retList.add((ProjectField)projecttype);
        }
        return retList;
    }

    @Override
    public void save(CaseProject caseProject) {
        caseProjectRepository.save(caseProject);
    }

    @Override
    public List<CaseProject> getAllProject() {
        return caseProjectRepository.findAll();
    }

    @Override
    public void deleteProjectByid(long projectID) {
        caseProjectRepository.deleteProjectByid(projectID);
    }

    public List<String> getProjectYearList() {
        List<String> stringList=new ArrayList<>();
        String hql="select distinct signaturedate from CaseProject";
        List<Date> dates= caseProjectRepository.findAllByHQL(hql);
        for (Date date:dates
             ) {
            String originStr=date.toString();
            stringList.add(originStr);
        }
        HashSet h = new HashSet(stringList);
        stringList.clear();
        stringList.addAll(h);
        return stringList;
    }

    @Override
    public List<CaseProject> getprojectByYearAndType(List<Date> selectedYear, List<String> selectedType) {
        List<ProjectField> fields=getFieldsByname(selectedType);

        List<CaseProject> projects=caseProjectRepository.getprojectByYearAndType(selectedYear,fields);
        return projects;
    }

    private List<ProjectField> getFieldsByname(List<String> selectedType){
        return caseProjectRepository.getFieldsByname(selectedType);
    }

    @Override
    public List<CaseProject> getMyProjects(Lawyer lawyer) {
        String sql="SELECT DISTINCT id FROM `t_case_teamnumber` WHERE `teamnumbers_lawyerid`="+lawyer.getLawyerid();
        List<BigInteger> ids =caseProjectRepository.findAllBySql(sql);
        String hql ="FROM CaseProject project WHERE project.caseprojectid IN (:strs)";
        List<Long> idslongList=listTool.listToLongList(ids);
        List<CaseProject> retList =caseProjectRepository.getMyProjects(hql,idslongList);
        return retList;
    }

    @Override
    public CaseProject getProjectByID(Long projectid) {
        return caseProjectRepository.get(projectid);
    }



    @Override
    public CaseProject getProjectByName(String projectName) {
        return caseProjectRepository.getProjectByName(projectName);
    }



    @Override
    /**
     * 根据文件名，获取到项目名称
     */
    public String getProjectNameByFileName(String filename) {
        logger.info("get Project Name By File Name,the filename is "+filename);

        //projects指的是springmvcdemo upload projects这个目录
        int pos=filename.indexOf("projects");
        //10为projects的长度加上前后两个分隔符
        String subString=filename.substring(pos+9);

        String[] strings=subString.split("\\\\");
        return strings[0];
    }

    @Override
    public List<CaseProject> getprojectByYearAndType(String projectBeginDate, String projectEndDate, List<String> typeList) {
        List<ProjectField> fields=getFieldsByname(typeList);
        Date begin = dateTool.StrToDate(projectBeginDate);
        Date end = dateTool.StrToDate(projectEndDate);

        String smallDate=projectBeginDate;
        String bigDate=projectEndDate;

        if(begin.after(end)){
            smallDate=projectEndDate;
            bigDate=projectBeginDate;
        }
        List<Date> selectedDates=getDatesByBeginAndEnd(smallDate,bigDate);
        List<CaseProject> projects=caseProjectRepository.getprojectByYearAndType(selectedDates,fields);
        return projects;
    }

    private List<Date> getDatesByBeginAndEnd(String smallDate, String bigDate) {
        String sql="SELECT DISTINCT signaturedate FROM t_caseproject WHERE signaturedate BETWEEN'"+smallDate+"' AND '"+bigDate+"'";
        List<Date> dateList=caseProjectRepository.findAllBySql(sql);
        return dateList;
    }
}
