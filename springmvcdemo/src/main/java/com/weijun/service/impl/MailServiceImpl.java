package com.weijun.service.impl;

import com.weijun.entity.MailPasswd;
import com.weijun.repository.MailRepository;
import com.weijun.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private MailRepository mailRepository;
    @Override
    public void saveMailPass(MailPasswd mailPasswd) {
        mailRepository.save(mailPasswd);
    }

    @Override
    public MailPasswd getMailPasswdByLawyerName(String lawyername) {
        String hql="from MailPasswd AS mailpasswd where mailpasswd.lawyerNmae='"+lawyername+"'";
        return (MailPasswd) mailRepository.findObjectByHQL(hql);
    }
}
