package com.weijun.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weijun.dao.BaseDao;
import com.weijun.entity.IP.TradeMark;
import com.weijun.entity.IP.CopyRight;
import com.weijun.repository.CopyRightRepository;
import com.weijun.repository.IPRepository;
import com.weijun.util.Constant;
import com.weijun.util.DateTool;
import com.weijun.util.HttpHelper;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.client.methods.HttpHead;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <h3>springmvcdemo</h3>
 * <p>知识产权相关的服务</p>
 *
 * @author : 周江
 * @date : 2020-06-07 12:35
 **/

@Service
public class IPService {
    @Autowired
    IPRepository ipRepository;
    @Autowired
    CopyRightRepository copyRightRepository;
    @Autowired
    DateTool dateTool;

    @Autowired
    private BaseDao dao;

    private static final String appkey = "46e7fbcf3f7a41eba6357466493ebea3";
    private static final String seckey = "028A97BDCF4A9FEF1342B956315CC8FB";

    public void saveMarks(JSONArray jsonArray) {
        for(int i=0;i<jsonArray.size();i++){
            JSONObject job = jsonArray.getJSONObject(i);   // 遍历 jsonarray 数组，把每一个对象转成 json 对象
            TradeMark mark=new TradeMark();
            mark.setID(job.getString("ID"));
            mark.setRegNo(job.getString("RegNo"));
            mark.setName(job.getString("Name"));
            mark.setCategoryId(job.getIntValue("CategoryId"));
            mark.setCategory(job.getString("Category"));
            mark.setPerson(job.getString("Person"));
            mark.setHasImage(job.getBoolean("HasImage"));
            mark.setFlow(job.getString("Flow"));
            mark.setImageUrl(job.getString("ImageUrl"));
            mark.setFlowStatus(job.getString("FlowStatus"));
            mark.setFlowStatusDesc(job.getString("FlowStatusDesc"));
            mark.setAppDate(job.getString("AppDate"));
            mark.setStatus(job.getString("Status"));
            long currentStamp=dateTool.getCurrentDatestamp();
            mark.setUpdateStamp(currentStamp);
            ipRepository.save(mark);
        }
    }

    private void saveCopyRights(JSONArray jsonArray) {
        for(int i=0;i<jsonArray.size();i++){
            JSONObject job = jsonArray.getJSONObject(i);
            CopyRight cp=new CopyRight();
            cp.setOwner(job.getString("Owner"));
            cp.setCategory(job.getString("Category"));
            cp.setName(job.getString("Name"));
            cp.setRegisterNo(job.getString("RegisterNo"));
            cp.setRegisterDate(job.getString("RegisterDate"));
            cp.setPublishDate(job.getString("PublishDate"));
            cp.setFinishDate(job.getString("FinishDate"));
            long currentStamp=dateTool.getCurrentDatestamp();
            cp.setUpdateStamp(currentStamp);


            copyRightRepository.save(cp);
        }
    }
    public List getMarkListByCompanyname(String companyName) {
        String hql="FROM TradeMark mark WHERE mark.Person='"+companyName+"'";
        List companyMarkList= ipRepository.findAllByHQL(hql);
        return companyMarkList;
    }

    public JSON getMarkListFromQCCAndSaveToDB(String companyName) {
        String reqInterNme = "http://api.qichacha.com/tm/SearchByApplicant";
        String paramStr = "keyword="+companyName;
        String status = "";
        HttpHead reqHeader = new HttpHead();
        String[] autherHeader = RandomAuthentHeader();
        reqHeader.setHeader("Token", autherHeader[0]);
        reqHeader.setHeader("Timespan", autherHeader[1]);
        final String reqUri = reqInterNme.concat("?key=").concat(appkey).concat("&").concat(paramStr);
        JSONArray jsonArray=null;
        try {
            String tokenJson = HttpHelper.httpGet(reqUri, reqHeader.getAllHeaders());
            JSONObject jsonObject=JSONObject.parseObject(tokenJson);
            jsonArray= (JSONArray) jsonObject.get("Result");
            saveMarks(jsonArray);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }




    // 获取Auth Code
    protected static final String[] RandomAuthentHeader() {
        String timeSpan = String.valueOf(System.currentTimeMillis() / 1000);
        String[] authentHeaders = new String[] { DigestUtils.md5Hex(appkey.concat(timeSpan).concat(seckey)).toUpperCase(), timeSpan };
        return authentHeaders;
    }


    public List<CopyRight> getCopyRightListByOwnerName(String owner) {
        String hql="FROM CopyRight copyright WHERE copyright.Owner='"+owner+"'";
        List copyrightList= copyRightRepository.findAllByHQL(hql);
        return copyrightList;
    }

    public JSON getCopyRightsFromQCCAndSaveToDB(String owner) {
//        由于从企查查一次最多返回50个数据，因此需要分页
        JSONArray jsonArray=null;
        String reqInterNme = "http://api.qichacha.com/CopyRight/SearchCopyRight";
        String paramStr = "searchKey="+owner;
        HttpHead reqHeader = new HttpHead();
        String[] autherHeader = RandomAuthentHeader();
        reqHeader.setHeader("Token", autherHeader[0]);
        reqHeader.setHeader("Timespan", autherHeader[1]);
        final String reqUri = reqInterNme.concat("?key=").concat(appkey).concat("&").
                concat(paramStr).concat("&").concat("pageSize=50&pageIndex=1");

        try {
            String QCCReturn = HttpHelper.httpGet(reqUri, reqHeader.getAllHeaders());
            JSONObject jsonObject=JSONObject.parseObject(QCCReturn);
            jsonArray= (JSONArray) jsonObject.get("Result");
            saveCopyRights(jsonArray);
//            获取TotalRecords
            int PageSize=jsonObject.getIntValue("PageSize");
            JSONObject paging = (JSONObject) jsonObject.get("Paging");
            int TotalRecords=paging.getIntValue("TotalRecords");

//            企查查每页最多返回50个，超过50，说明肯定是多页，需要继续查
            if(TotalRecords> Constant.QCCMaxNumberBySingleQuery_50){
                int pageNumber= (int) Math.ceil(TotalRecords/Constant.QCCMaxNumberBySingleQuery_50_double);
//                只有一页的情况下直接返回
//                从第二页开始，继续查询
                for (int i=2;i<=pageNumber;i++){
                    String pagePara="pageSize="+Constant.QCCMaxNumberBySingleQuery_50+"&pageIndex="+i;
//                    拼接URL
                    String tmpURL=reqInterNme.concat("?key=").concat(appkey).concat("&").concat(paramStr).concat("&").concat(pagePara);
                    String tmpQCCReturn = HttpHelper.httpGet(tmpURL, reqHeader.getAllHeaders());
                    JSONObject job=JSONObject.parseObject(tmpQCCReturn);
                    JSONArray tmpResult = (JSONArray) job.get("Result");
                    saveCopyRights(tmpResult);
                    jsonArray.addAll(tmpResult);
                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }


    public List getAllMarks() {
        String hql="from TradeMark";
        return dao.findAllByHQL(hql);
    }
}
