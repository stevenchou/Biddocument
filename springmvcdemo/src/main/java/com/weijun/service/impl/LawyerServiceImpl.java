package com.weijun.service.impl;

import com.weijun.entity.CertificateCard;
import com.weijun.entity.Lawyer;
import com.weijun.repository.DomainRepository;
import com.weijun.repository.LawyerRepository;
import com.weijun.service.LawyerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LawyerServiceImpl implements LawyerService {

    @Autowired
    private LawyerRepository lawyerRepository;

    @Override
    public Long saveLawyer() {
        return null;
    }

    @Override
    public void addLawyer(Lawyer lawyer) {
        lawyerRepository.save(lawyer);
    }

    public List<Lawyer> getAllLawyerList(){
        return lawyerRepository.findAll();
    }

    @Override
    public void deleteLawyerByid(Long lawyerid) {
        lawyerRepository.delete(lawyerid);
    }

    @Override
    public List<Lawyer> parterwithsocialposition(){
        //select s.name,s.tel,s.address,s.star from Seller s
//        String hql="select lawyer.lawyername FROM Lawyer lawyer WHERE lawyer.position IN('管理合伙人','合伙人')";
        String hql="FROM Lawyer AS lawyer WHERE lawyer.position IN('管理合伙人律师','高级合伙人律师','合伙人律师','专职律师')";
        List<Lawyer> lawyerlist= lawyerRepository.findAllByHQL(hql);
        return lawyerlist;
    }

    @Override
    public Lawyer getLawyerByName(String lawyername) {
        String hql="FROM Lawyer AS lawyer WHERE lawyer.lawyername='"+lawyername +"'";
        Lawyer lawyer= (Lawyer) lawyerRepository.findObjectByHQL(hql);
        return lawyer;
    }

    @Override
    public void updateLawyer(Lawyer lawyer) {
        lawyerRepository.saveOrUpdate(lawyer);
    }

    @Override
    public void updateLawyercertificate(CertificateCard certificateCard) {
        lawyerRepository.saveLawyerCertificate(certificateCard);
    }


    public List<Lawyer> getResponsibleLawyers() {
        String hql="FROM Lawyer AS lawyer WHERE lawyer.position IN('管理合伙人律师','高级合伙人律师','合伙人律师','专职律师')";
        List<Lawyer> lawyerlist=lawyerRepository.findAllByHQL(hql);
        return lawyerlist;
    }

    public List<Lawyer> getResponsibleLawyersByNames(String[] lawyerNames){
        String hql="from Lawyer lawyer where lawyer.lawyername in (:strs)";
        List<Object> lawyerList=lawyerRepository.getProjectTypeListByNames(hql,lawyerNames);
        List<Lawyer> retList = new ArrayList<>();
        for (Object lawyer:lawyerList) {
            retList.add((Lawyer) lawyer);
        }
        return retList;
    }
    public List<Lawyer> getSignatureLawyers() {
        String hql="FROM Lawyer AS lawyer WHERE lawyer.position IN('管理合伙人律师')";
        List<Lawyer> lawyerlist=lawyerRepository.findAllByHQL(hql);
        return lawyerlist;
    }

    @Override
    public void updateLawyerModifyTime(Long lawyerID){
        Lawyer lawyer=lawyerRepository.get(lawyerID);
        lawyer.setUpdatedate(new Date());
        lawyerRepository.save(lawyer);
    }

    @Override
    public List<BigInteger> getLawyerNameBySocialPosition() {
        String sql="SELECT DISTINCT id FROM t_socialposition";
        List<BigInteger> lawyerIDs=lawyerRepository.findAllBySql(sql);
        return lawyerIDs;
    }

    @Override
    public List<Lawyer> getLawyersBySocialPosition(List<BigInteger> lawyerNameHasSocialPosition) {
        String IDs="";
        for (BigInteger id :lawyerNameHasSocialPosition
             ) {
            IDs+="'"+id+"',";
        }
        //删除IDs最后一个逗号
        if (IDs.length()>0){
            IDs=IDs.substring(0,IDs.length()-1);
        }
        String hql="FROM Lawyer AS lawyer WHERE lawyer.lawyerid IN("+IDs+")";
        List<Lawyer> lawyerlist=lawyerRepository.findAllByHQL(hql);
        return lawyerlist;
    }

    @Override
    public void updateLawyerModifyTime(String lawyerName){
        Lawyer lawyer=getLawyerByName(lawyerName);
        lawyer.setUpdatedate(new Date());
        lawyerRepository.save(lawyer);
    }

    @Override
    public void updateLawyerModifyTime(List<Lawyer> lawyerList){
        for (Lawyer lawyer:lawyerList
             ) {
            lawyer.setUpdatedate(new Date());
            lawyerRepository.save(lawyer);
        }
    }
}
