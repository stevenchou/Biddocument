package com.weijun.service.impl;

import com.weijun.entity.FileAttributeType;
import com.weijun.repository.BidDocuRepository;
import com.weijun.service.BidDocuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BidDocuServiceImpl implements BidDocuService {
    @Autowired
    BidDocuRepository bidDocuRepository;
    @Override
    public void newFileAttrType(FileAttributeType fileAttributeType) {
        bidDocuRepository.newFileAttrType(fileAttributeType);
    }

    @Override
    public List<FileAttributeType> getAllFileAttributeType() {
        return bidDocuRepository.getAllFileAttributeType();
    }
}
