package com.weijun.service.impl;

import com.weijun.dao.BaseDao;
import com.weijun.entity.Permission;
import com.weijun.entity.Role;
import com.weijun.entity.User;
import com.weijun.entity.UserRole;
import com.weijun.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class AccountService {
//    @Autowired
//    private UserRepository userRepository;
    @Autowired
    private BaseDao dao;
    @Autowired
    private UserRoleRepository userRoleRepository;

    public User getUserByUserName(String username){
        User user = (User) dao.findObjectByHQL("from User where username=?",new Object[]{username});
        return user;

    }

    public List<String> getPermissionsByUserName(String username) {
        /**
         * create by: 周江
         * description: 通过用户名获取权限资源
         * create time: 5/30/2020 1:02 AM
         *
          * @Param: username 用户名
         * @return java.util.List<java.lang.String>
         */

//        System.out.println("调用");
        User user = getUserByUserName(username);
        if(user==null){
            return null;
        }
        List<String> list = new ArrayList<String>();
        for(UserRole userRole:user.getUserRoles()){
            Role role = userRole.getRole();
            List<Permission> permissions = dao.findAllByHQL("FROM Permission WHERE roleId = ?", new Object[] { role.getId() });
            for (Permission p : permissions) {
                list.add(p.getUrl());
            }
        }
        return list;
    }

    public Role getRoleByName(String roleName) {
        String hql="from Role where name = ?";
         return (Role) dao.findObjectByHQL(hql,new Object[]{roleName});
    }

    public void saveUser(User newUser) {
        dao.addObject(newUser);

    }

    public void saveUserRole(UserRole userRole) {
        userRoleRepository.save(userRole);
    }

    public List<User> getAllUser() {
        String hql="from User";
        return dao.findAllByHQL(hql);
    }

    public User getUserByID(Long currentUserId) {
        String hql="from User user where user.id="+currentUserId;
        return (User) dao.findObjectByHQL(hql);
    }
}
