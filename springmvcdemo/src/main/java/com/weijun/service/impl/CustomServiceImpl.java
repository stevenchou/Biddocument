package com.weijun.service.impl;

import com.weijun.entity.HSCustom;
import com.weijun.entity.OperateLog;
import com.weijun.entity.RelativeCompany;
import com.weijun.repository.CustomRepository;
import com.weijun.service.CustomService;
import com.weijun.util.Constant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CustomServiceImpl implements CustomService {
    @Autowired
    CustomRepository customRepository;
    @Autowired
    Constant constant;

    private static final Logger logger = LogManager.getLogger(CustomServiceImpl.class.getName());
    @Override
    public void saveCustom(HSCustom hsCustom) {
        customRepository.save(hsCustom);
    }

    @Override
    public List<HSCustom> getAllCustom() {
        return customRepository.findAll();
    }

    @Override
    public List<HSCustom> getFrequentCustom(int topN) {
        List<OperateLog> operateLogList=customRepository.customInLog(topN);
        //取得operateLogList中custom的名字
        List<String> customNameList=getAllCustomNameFromLog(operateLogList);
//            根据名字清单获取custom信息
        List<HSCustom> customList=getCustomsByNames(customNameList);
        if (operateLogList.size()<topN){
            addItemsFromCustomTable(customList,customNameList,topN);
        }
        return customList;
    }

    private void addItemsFromCustomTable(List<HSCustom> customList, List<String> customNameList,int topN) {
        List<HSCustom> allCustoms=getAllCustom();
        for (HSCustom custom:allCustoms
             ) {
            int count = Collections.frequency(customNameList,custom.getCustomname());
            if (customList.size()<topN&&count<1){
                customList.add(custom);
            }else if(customList.size()==topN){
                return;
            }
        }

    }

    private List<HSCustom> getCustomsByNames(List<String> customNameList) {
        List<HSCustom> customList =new ArrayList<HSCustom>();
        for (String customName:customNameList
             ) {
            HSCustom custom=getCustomByName(customName);
            customList.add(custom);
        }
        return customList;
    }

    private List<String> getAllCustomNameFromLog(List<OperateLog> operateLogList) {
        List<String> customNameList=new ArrayList<String>();
        for (OperateLog operateLog:operateLogList
             ) {
            String customName=operateLog.getOperateobjectname();
            if(customNameList.contains(customName)){
                logger.info("the custom name is already selected");
            }
            else{
                customNameList.add(customName);
            }

        }
        return customNameList;
    }


    public HSCustom getCustomByID(Long customID){
        return customRepository.get(customID);
    }

    @Override
    public HSCustom getCustomByName(String customName) {
        String hql="from HSCustom custom where custom.customname ='"+customName+"'";

        return (HSCustom) customRepository.findObjectByHQL(hql);
    }

    @Override
    public List<HSCustom> getAllPersonalCustom() {
        String hql ="FROM HSCustom AS psrsonalCustom where psrsonalCustom.customtype ='"+constant.customtype_personalCustom+"'";
        List<HSCustom> retList= customRepository.findAllByHQL(hql);
        return retList;
    }

    @Override
    public void updateCustom(HSCustom custom) {
        customRepository.updateCustom(custom);
    }

    @Override
    public void saveOrUpdateCustom(HSCustom custom) {
        customRepository.saveOrUpdate(custom);
    }

    @Override
    public void saveRelativeCompany(RelativeCompany relativeCompany) {
        customRepository.saveRelativeCompany(relativeCompany);
    }


}
