package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name="t_personcustominfo")
@Entity
public class PersonalCustomInfo {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long personalcustominfoid;

    @Column(name = "gender")
    String gender;


    public Long getPersonalcustominfoid() {
        return personalcustominfoid;
    }

    public void setPersonalcustominfoid(Long personalcustominfoid) {
        this.personalcustominfoid = personalcustominfoid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
