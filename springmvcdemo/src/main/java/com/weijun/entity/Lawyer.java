package com.weijun.entity;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Table(name="t_lawyer")
@Entity
public class Lawyer {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long lawyerid;

    @Column(name="lawyername",unique = true)
    String lawyername;

    @Column(name = "gender")
    char[] gender;

    @Column(name = "birthday")
    Date birthday;

    //从业日期
    @Column(name = "employeddate")
    Date employeddate;

    @Column(name = "updatedate")
    Date updatedate;
    //律所职务
    @Column(name = "position")
    String position;

    @Column(name="telephone")
    String telephone;
    //身份证号
    @Column(name = "IDcardno")
    String IDcardno;

    //邮件
    @Column(name="email")
    String email;

    //身份证保存路径
    @Column(name = "IDcardpath")
    String IDcardpath;

    //个人照片保存路径
    @Column(name = "personalimgpath")
    String personalimgpath;

    @Fetch(FetchMode.SELECT)
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "t_eduinfo",joinColumns = @JoinColumn(name = "id"))
    @Column(name = "eduinfo")
    List<String> eduinfolist = new ArrayList<String>();

    @Fetch(FetchMode.SELECT)
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "t_workexp",joinColumns = @JoinColumn(name = "id"))
    @Column(name = "workexp")
    List<String> workiexplist = new ArrayList<String>();

    @Fetch(FetchMode.SELECT)
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "t_professionalfield",joinColumns = @JoinColumn(name = "id"))
    @Column(name="professionalfield")
    List<String> professionalfieldlist= new ArrayList<String>();

    @Fetch(FetchMode.SELECT)
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "t_socialposition",joinColumns = @JoinColumn(name = "id"))
    @Column(name="socialposition")
    List<String> socialpositionlist= new ArrayList<String>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "certificateCardId", unique = true)
    CertificateCard certificateCard;

    public Lawyer(){
    }
    public Lawyer(String lawyername){
        setLawyername(lawyername);
    }

    public String getLawyername() {
        return lawyername;
    }

    public void setLawyername(String lawyername) {
        this.lawyername = lawyername;
    }


    public char[] getGender() {
        return gender;
    }

    public void setGender(char[] gender) {
        this.gender = gender;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getEmployeddate() {
        return employeddate;
    }

    public void setEmployeddate(Date employeddate) {
        this.employeddate = employeddate;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getIDcardno() {
        return IDcardno;
    }

    public void setIDcardno(String IDcardno) {
        this.IDcardno = IDcardno;
    }

    public String getIDcardpath() {
        return IDcardpath;
    }

    public void setIDcardpath(String IDcardpath) {
        this.IDcardpath = IDcardpath;
    }

    public String getPersonalimgpath() {
        return personalimgpath;
    }

    public void setPersonalimgpath(String personalimgpath) {
        this.personalimgpath = personalimgpath;
    }
    public List<String> getEduinfolist() {
        return eduinfolist;
    }

    public void setEduinfolist(List<String> eduinfolist) {
        this.eduinfolist = eduinfolist;
    }

    public List<String> getWorkiexplist() {
        return workiexplist;
    }

    public void setWorkiexplist(List<String> workiexplist) {
        this.workiexplist = workiexplist;
    }

    public List<String> getProfessionalfieldlist() {
        return professionalfieldlist;
    }

    public void setProfessionalfieldlist(List<String> professionalfieldlist) {
        this.professionalfieldlist = professionalfieldlist;
    }

    public List<String> getSocialpositionlist() {
        return socialpositionlist;
    }

    public void setSocialpositionlist(List<String> socialpositionlist) {
        this.socialpositionlist = socialpositionlist;
    }

    public CertificateCard getCertificateCard() {
        return certificateCard;
    }

    public void setCertificateCard(CertificateCard certificateCard) {
        this.certificateCard = certificateCard;
    }

    public Long getLawyerid() {
        return lawyerid;
    }

    public void setLawyerid(Long lawyerid) {
        this.lawyerid = lawyerid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }
}
