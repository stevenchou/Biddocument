package com.weijun.entity;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Table(name="t_caseproject")
@Entity
public class CaseProject {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long caseprojectid;

    @Column(name = "casename")
    String casename;

    @Column(name = "casebrief")
    String casebrief;

    @ManyToOne
    @JoinColumn(name = "custom")
    HSCustom custom;


    @Column(name = "signaturedate")
    Date signaturedate;

    @Column(name = "terminationdate")
    Date terminationdate;

    //合同中签约律师
    @ManyToOne
    @JoinColumn(name = "signatureLawyer")
    Lawyer signatureLawyer;

    //负责律师
    @Fetch(FetchMode.SELECT)
    @ManyToMany(targetEntity =Lawyer.class, fetch = FetchType.EAGER)
    @JoinTable(name = "t_case_responsiblelawyer",joinColumns = @JoinColumn(name = "id"))
    @Column(name="responsibleLawyers")
    List<Lawyer> responsibleLawyers= new ArrayList<Lawyer>();

    //team number，包括合同中的律师以及实际服务的律师
    @Fetch(FetchMode.SELECT)
    @ManyToMany(targetEntity =Lawyer.class, fetch = FetchType.EAGER)
    @JoinTable(name = "t_case_teamnumber",joinColumns = @JoinColumn(name = "id") )
    @Column(name="teamnumbers")
    List<Lawyer> teamnumbers =new ArrayList<Lawyer>();

    //完整的合同文件
    @Column(name = "evidencefilepath")
    String evidencefilepath;

//    处理后，截取的部分合同文件
    @Fetch(FetchMode.SELECT)
    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "t_partcontract",joinColumns = @JoinColumn(name = "id"))
    @Column(name = "partcontract")
    List<String> partcontractList = new ArrayList<String>();


    @Fetch(FetchMode.SELECT)
    @ManyToMany(targetEntity =ProjectField.class, fetch = FetchType.EAGER)
    @JoinTable(name = "t_project_fields",joinColumns = @JoinColumn(name = "id"))
    @Column(name = "projectfields")
    List<ProjectField> projectfields=new ArrayList<ProjectField>();



    public String getCasename() {
        return casename;
    }

    public void setCasename(String casename) {
        this.casename = casename;
    }

    public String getCasebrief() {
        return casebrief;
    }

    public void setCasebrief(String casebrief) {
        this.casebrief = casebrief;
    }

    public HSCustom getCustom() {
        return custom;
    }

    public void setCustom(HSCustom custom) {
        this.custom = custom;
    }

    public Date getSignaturedate() {
        return signaturedate;
    }

    public void setSignaturedate(Date signaturedate) {
        this.signaturedate = signaturedate;
    }

    public Date getTerminationdate() {
        return terminationdate;
    }

    public void setTerminationdate(Date terminationdate) {
        this.terminationdate = terminationdate;
    }

    public String getEvidencefilepath() {
        return evidencefilepath;
    }

    public void setEvidencefilepath(String evidencefilepath) {
        this.evidencefilepath = evidencefilepath;
    }

    public List<ProjectField> getProjectfields() {
        return projectfields;
    }

    public void setProjectfields(List<ProjectField> projectfields) {
        this.projectfields = projectfields;
    }

    public Long getCaseprojectid() {
        return caseprojectid;
    }

    public void setCaseprojectid(Long caseprojectid) {
        this.caseprojectid = caseprojectid;
    }

    public List<Lawyer> getResponsibleLawyers() {
        return responsibleLawyers;
    }

    public void setResponsibleLawyers(List<Lawyer> responsibleLawyers) {
        this.responsibleLawyers = responsibleLawyers;
    }

    public List<Lawyer> getTeamnumbers() {
        return teamnumbers;
    }

    public void setTeamnumbers(List<Lawyer> teamnumbers) {
        this.teamnumbers = teamnumbers;
    }

    public Lawyer getSignatureLawyer() {
        return signatureLawyer;
    }

    public void setSignatureLawyer(Lawyer signatureLawyer) {
        this.signatureLawyer = signatureLawyer;
    }

    public List<String> getPartcontractList() {
        return partcontractList;
    }

    public void setPartcontractList(List<String> partcontractList) {
        this.partcontractList = partcontractList;
    }


}
