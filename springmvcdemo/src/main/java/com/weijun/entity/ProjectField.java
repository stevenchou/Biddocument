package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name="t_projectfields")
@Entity
public class ProjectField {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long id;
    @Column(name = "projettypename",nullable=false)
    String projettypename;

    @Column(name = "projecttypebrief")
    String projecttypebrief;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjettypename() {
        return projettypename;
    }

    public void setProjettypename(String projettypename) {
        this.projettypename = projettypename;
    }

    public String getProjecttypebrief() {
        return projecttypebrief;
    }

    public void setProjecttypebrief(String projecttypebrief) {
        this.projecttypebrief = projecttypebrief;
    }
}
