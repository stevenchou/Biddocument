package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name="t_systoken")
@Entity
public class SysToken {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Integer id;

    @Column(name="userid")
    Integer UserId;
    @Column(name ="token")
    String Token;

    @Column(name="updatetime")
    Date UpdateTime;
    @Column(name="expiretime")
    Date ExpireTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public Date getUpdateTime() {
        return UpdateTime;
    }

    public void setUpdateTime(Date updateTime) {
        UpdateTime = updateTime;
    }

    public Date getExpireTime() {
        return ExpireTime;
    }

    public void setExpireTime(Date expireTime) {
        ExpireTime = expireTime;
    }
}
