package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name="t_companycustominfo")
@Entity
public class CompanyCustomInfo {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long companycustominfoid;

    @Column(name = "contactname")
    String contactname;

    @Column(name = "contactphone")
    String contactphone;

    @Column(name = "contactgender")
    String contactgender;


    public Long getCompanycustominfoid() {
        return companycustominfoid;
    }

    public void setCompanycustominfoid(Long companycustominfoid) {
        this.companycustominfoid = companycustominfoid;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getContactphone() {
        return contactphone;
    }

    public void setContactphone(String contactphone) {
        this.contactphone = contactphone;
    }

    public String getContactgender() {
        return contactgender;
    }

    public void setContactgender(String contactgender) {
        this.contactgender = contactgender;
    }

}
