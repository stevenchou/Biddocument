package com.weijun.entity;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Table(name="t_hscustom")
@Entity
public class HSCustom {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long customid;

    @Column(name = "customname",unique = true,nullable=false)
    String customname;

    @Column(name = "custombrief")
    String custombrief;

    @Column(name="telephone")
    String telephone;

    //邮件
    @Column(name="email")
    String email;

    //地址
    @Column(name = "address")
    String address;

    //单位客户或个人客户
    @Column(name = "customtype")
    String customtype;

    @Column(name = "imgpath")
    String imgpath;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "companycustominfo", unique = true)
    CompanyCustomInfo companycustominfo;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "personalcustominfo", unique = true)
    PersonalCustomInfo personalcustominfo;


    //关联公司
    @Fetch(FetchMode.SELECT)
    @ManyToMany(targetEntity =RelativeCompany.class, fetch = FetchType.EAGER,cascade =CascadeType.PERSIST)
    @JoinTable(name = "t_custom_relativeCompany")
    @Column(name="relativeCompany")
    List<RelativeCompany> relativeCompanyList= new ArrayList<RelativeCompany>();

    public Long getId() {
        return customid;
    }

    public void setId(Long id) {
        this.customid = id;
    }

    public String getCustomname() {
        return customname;
    }

    public void setCustomname(String customname) {
        this.customname = customname;
    }

    public String getCustombrief() {
        return custombrief;
    }

    public void setCustombrief(String custombrief) {
        this.custombrief = custombrief;
    }

    public Long getCustomid() {
        return customid;
    }

    public void setCustomid(Long customid) {
        this.customid = customid;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomtype() {
        return customtype;
    }

    public void setCustomtype(String customtype) {
        this.customtype = customtype;
    }

    public CompanyCustomInfo getCompanycustominfo() {
        return companycustominfo;
    }

    public void setCompanycustominfo(CompanyCustomInfo companycustominfo) {
        this.companycustominfo = companycustominfo;
    }

    public PersonalCustomInfo getPersonalcustominfo() {
        return personalcustominfo;
    }

    public void setPersonalcustominfo(PersonalCustomInfo personalcustominfo) {
        this.personalcustominfo = personalcustominfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImgpath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    public List<RelativeCompany> getRelativeCompanyList() {
        return relativeCompanyList;
    }

    public void setRelativeCompanyList(List<RelativeCompany> relativeCompanyList) {
        this.relativeCompanyList = relativeCompanyList;
    }
}
