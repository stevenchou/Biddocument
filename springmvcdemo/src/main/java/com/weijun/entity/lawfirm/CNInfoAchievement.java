package com.weijun.entity.lawfirm;

import com.weijun.entity.ProjectField;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <h3>springmvcdemo</h3>
 * <p>巨潮资讯网上可查的华商业绩</p>
 *
 * @author : 周江
 * @date : 2020-06-21 20:19
 **/
@Data
@Table(name="t_cninfoachievement")
@Entity
public class CNInfoAchievement {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long cninfoachievementid;

    @Column(name = "companyCode")
    String companyCode;
    @Column(name = "filename",unique = true,nullable = false)
    String filename;
    @Column(name ="date")
    String date;

    @Column(name = "fileURL")
    String fileURL;

    @Fetch(FetchMode.SELECT)
    @ManyToMany(targetEntity = ProjectField.class, fetch = FetchType.EAGER)
    @JoinTable(name = "t_project_fields",joinColumns = @JoinColumn(name = "id"))
    @Column(name = "projectfields")
    List<ProjectField> projectfields=new ArrayList<ProjectField>();

    public Long getCninfoachievementid() {
        return cninfoachievementid;
    }

    public void setCninfoachievementid(Long cninfoachievementid) {
        this.cninfoachievementid = cninfoachievementid;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<ProjectField> getProjectfields() {
        return projectfields;
    }

    public void setProjectfields(List<ProjectField> projectfields) {
        this.projectfields = projectfields;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }
}
