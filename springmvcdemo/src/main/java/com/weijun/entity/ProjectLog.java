package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name="t_projectlog")
@Entity
public class ProjectLog {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long projectLogID;

    @ManyToOne
    @JoinColumn(name = "project")
    CaseProject project;
}
