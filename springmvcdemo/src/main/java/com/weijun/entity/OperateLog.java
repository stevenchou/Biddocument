package com.weijun.entity;

import com.weijun.util.OperateType;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name="t_userlog")
@Entity
public class OperateLog {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long userLogID;

    @Column(name="username")
    String username;

    @Column(name = "operatetime")
    Date operatetime;

    //被操作的对象所属的类型
    @Column(name = "operateobjecttype")
    String operateobjecttype;

    @Column(name = "operateobjectname")
    String operateobjectname;

    //操作类型，如增删改查
    @Column(name = "operatetype")
    OperateType operateType;

    public Long getUserLogID() {
        return userLogID;
    }

    public void setUserLogID(Long userLogID) {
        this.userLogID = userLogID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getOperatetime() {
        return operatetime;
    }

    public void setOperatetime(Date operatetime) {
        this.operatetime = operatetime;
    }

    public String getOperateobjecttype() {
        return operateobjecttype;
    }

    public void setOperateobjecttype(String operateobjecttype) {
        this.operateobjecttype = operateobjecttype;
    }

    public String getOperateobjectname() {
        return operateobjectname;
    }

    public void setOperateobjectname(String operateobjectname) {
        this.operateobjectname = operateobjectname;
    }

    public OperateType getOperateType() {
        return operateType;
    }

    public void setOperateType(OperateType operateType) {
        this.operateType = operateType;
    }
}
