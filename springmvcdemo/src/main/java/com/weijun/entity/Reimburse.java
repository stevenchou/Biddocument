package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

//报销
@Data
@Table(name="t_reimburse")
@Entity
public class Reimburse {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long id;

    //报销项产生时间
    @Column(name = "generateDate")
    Date generateDate;

//    报销申请日期
    @Column(name = "applyDate")
    Date applyDate;

    //报销人
    @Column(name = "applicantName")
    String applicantName;

//    状态，分为：提交，已报销
    @Column(name="state")
    String state;

    //报销类别
    @Column(name = "category")
    String category;

    @Column(name = "detail",unique = true)
    String detail;

    @Column(name = "amount")
    Long amount;

    @Column(name = "caseNum")
    String caseNum;

    @Column(name = "caseName")
    String caseName;

    @Column(name = "undertaker")
    String undertaker;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGenerateDate() {
        return generateDate;
    }

    public void setGenerateDate(Date generateDate) {
        this.generateDate = generateDate;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getCaseNum() {
        return caseNum;
    }

    public void setCaseNum(String caseNum) {
        this.caseNum = caseNum;
    }

    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    public String getUndertaker() {
        return undertaker;
    }

    public void setUndertaker(String undertaker) {
        this.undertaker = undertaker;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }
}
