package com.weijun.entity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
@Data
@Table(name="t_user")
@Entity
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    Integer id;

    @Column(name="username")
    String username;
    @Column(name ="password")
    String password;

    @Column(name="createdate")
    Date createDate;

    @ManyToOne
    @JoinColumn(name="lawyer")
    Lawyer lawyer;

    @OneToMany(mappedBy="user",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    @Column(name = "userroles")
    List<UserRole> userRoles;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public List<UserRole> getUserRoles() {
        return userRoles;
    }
    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public Lawyer getLawyer() {
        return lawyer;
    }

    public void setLawyer(Lawyer lawyer) {
        this.lawyer = lawyer;
    }
}
