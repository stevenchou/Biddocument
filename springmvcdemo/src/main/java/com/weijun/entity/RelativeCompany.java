package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;

//关联公司
@Data
@Table(name="t_relativeCompany")
@Entity
public class RelativeCompany {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long id;

    @Column(name = "relativeCompanyName")
    String relativeCompanyName;

    @Column(name = "relativeCompanyCreditCode",unique = true)
    String relativeCompanyCreditCode;

    @Column(name = "positionInRelativeCompany")
    String positionInRelativeCompany;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRelativeCompanyName() {
        return relativeCompanyName;
    }

    public void setRelativeCompanyName(String relativeCompanyName) {
        this.relativeCompanyName = relativeCompanyName;
    }

    public String getRelativeCompanyCreditCode() {
        return relativeCompanyCreditCode;
    }

    public void setRelativeCompanyCreditCode(String relativeCompanyCreditCode) {
        this.relativeCompanyCreditCode = relativeCompanyCreditCode;
    }

    public String getPositionInRelativeCompany() {
        return positionInRelativeCompany;
    }

    public void setPositionInRelativeCompany(String positionInRelativeCompany) {
        this.positionInRelativeCompany = positionInRelativeCompany;
    }
}
