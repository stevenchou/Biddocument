package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name="t_fileattributetype")
@Entity
public class FileAttributeType {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long id;

    @Column(name = "attributename",unique = true,nullable=false)
    String attributename;

    @Column(name = "attributebrief")
    String attributebrief;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttributename() {
        return attributename;
    }

    public void setAttributename(String attributename) {
        this.attributename = attributename;
    }

    public String getAttributebrief() {
        return attributebrief;
    }

    public void setAttributebrief(String attributebrief) {
        this.attributebrief = attributebrief;
    }
}
