package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name="t_officehonor")
@Entity
public class OfficeHonor {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long id;

    @Column(name = "honoryear")
    Integer honoryear;

    @Column(name = "honorcontent")
    String honorcontent;

    @Column(name = "honorfilepath")
    String honorfilepath;

    public Integer getHonoryear() {
        return honoryear;
    }

    public void setHonoryear(Integer honoryear) {
        this.honoryear = honoryear;
    }

    public String getHonorcontent() {
        return honorcontent;
    }

    public void setHonorcontent(String honorcontent) {
        this.honorcontent = honorcontent;
    }

    public String getHonorfilepath() {
        return honorfilepath;
    }

    public void setHonorfilepath(String honorfilepath) {
        this.honorfilepath = honorfilepath;
    }
}
