package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name="t_certificate")
@Entity
public class CertificateCard {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long certificatecardid;

    @Column(name="certificateID")
    String certificateID;

    @Column(name="certificatedate")
    String certificatedate;
    @Column(name="certificatecardpath")
    String certificatecardpath;

    public String getCertificateID() {
        return certificateID;
    }

    public void setCertificateID(String certificateID) {
        this.certificateID = certificateID;
    }

    public String getCertificatedate() {
        return certificatedate;
    }

    public void setCertificatedate(String certificatedate) {
        this.certificatedate = certificatedate;
    }

    public String getCertificatecardpath() {
        return certificatecardpath;
    }

    public void setCertificatecardpath(String certificatecardpath) {
        this.certificatecardpath = certificatecardpath;
    }

    public Long getCertificatecardid() {
        return certificatecardid;
    }

    public void setCertificatecardid(Long certificatecardid) {
        this.certificatecardid = certificatecardid;
    }
}
