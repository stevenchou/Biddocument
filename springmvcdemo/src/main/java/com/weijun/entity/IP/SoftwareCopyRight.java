package com.weijun.entity.IP;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * <h3>springmvcdemo</h3>
 * <p>软件著作权类</p>
 *
 * @author : 周江
 * @date : 2020-06-15 21:54
 **/

@Data
@Table(name="t_softwarecopyright")
@Entity
public class SoftwareCopyRight {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long softwarecopyrightid;

//    分类号
    @Column(name = "Category")
    String Category;

//    发布日期
    @Column(name = "PublishDate")
    String PublishDate;

    //    版本号
    @Column(name = "VersionNo")
    String VersionNo;

//    登记号
    @Column(name = "RegisterNo")
    String RegisterNo;

//    登记批准日期
    @Column(name = "RegisterAperDate")
    String RegisterAperDate;

//    软件全称
    @Column(name = "Name")
    String Name;

//    软件简称
    @Column(name = "ShortName")
    String ShortName;

//    软件著作权人
    @Column(name = "Owner")
    String Owner;

    @Column(name = "updateStamp")
    @ColumnDefault("0")
    long updateStamp;

}
