package com.weijun.entity.IP;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * <h3>springmvcdemo</h3>
 * <p>著作权</p>
 *
 * @author : 周江
 * @date : 2020-06-13 15:52
 **/
@Data
@Table(name="t_copyright")
@Entity
public class CopyRight {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long copyrightid;

//    作品著作权人
    @Column(name = "Owner")
    String Owner;
//    类别
    @Column(name = "Category")
    String Category;

//    作品名称
    @Column(name = "Name")
    String Name;

    @Column(name = "RegisterNo",unique=true, nullable=false)
    String RegisterNo;
    @Column(name = "RegisterDate")
    String RegisterDate;
//    首次发布日期
    @Column(name = "PublishDate")
    String PublishDate;
//    创作完成日期
    @Column(name="FinishDate")
    String FinishDate;

    @Column(name = "updateStamp")
    @ColumnDefault("0")
    long updateStamp;

    public Long getCopyrightid() {
        return copyrightid;
    }

    public void setCopyrightid(Long copyrightid) {
        this.copyrightid = copyrightid;
    }

    public String getOwner() {
        return Owner;
    }

    public void setOwner(String owner) {
        Owner = owner;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getRegisterNo() {
        return RegisterNo;
    }

    public void setRegisterNo(String registerNo) {
        RegisterNo = registerNo;
    }

    public String getRegisterDate() {
        return RegisterDate;
    }

    public void setRegisterDate(String registerDate) {
        RegisterDate = registerDate;
    }

    public String getPublishDate() {
        return PublishDate;
    }

    public void setPublishDate(String publishDate) {
        PublishDate = publishDate;
    }

    public String getFinishDate() {
        return FinishDate;
    }

    public void setFinishDate(String finishDate) {
        FinishDate = finishDate;
    }

    public long getUpdateStamp() {
        return updateStamp;
    }

    public void setUpdateStamp(long updateStamp) {
        this.updateStamp = updateStamp;
    }
}
