package com.weijun.entity.IP;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * <h3>springmvcdemo</h3>
 * <p>商标简要信息</p>
 *
 * @author : 周江
 * @date : 2020-06-06 10:52
 **/
@Data
@Table(name="t_trademark")
@Entity
public class TradeMark {
    /**
     * 前部分为简略信息
     */

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long companymarkid;

    /**
     * 企查查网站上的商标ID
     */
    @Column(name = "ID")
    String ID;

    @Column(name = "RegNo")
    String RegNo;
    @Column(name = "Name")
    String Name;
//    类别号
    @Column(name = "CategoryId")
    Integer CategoryId;
//    类别
    @Column(name = "Category")
    String Category;
//    申请人
    @Column(name = "Person")
    String Person;
//    是否有图片
    @Column(name = "HasImage")
    Boolean HasImage;
//    商标续展
    @Column(name = "Flow")
    String Flow;
//    图片地址
    @Column(name="ImageUrl")
    String ImageUrl;
//    流程状态代码
    @Column(name = "FlowStatus")
    String FlowStatus;
//    流程状态描述
    @Column(name = "FlowStatusDesc")
    String FlowStatusDesc;

//    申请日期
    @Column(name="AppDate")
    String AppDate;
//    商标状态
    @Column(name = "Status")
    String Status;

    @Column(name = "updateStamp")
    @ColumnDefault("0")
    long updateStamp;

    public Long getCompanymarkid() {
        return companymarkid;
    }

    public void setCompanymarkid(Long companymarkid) {
        this.companymarkid = companymarkid;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getRegNo() {
        return RegNo;
    }

    public void setRegNo(String regNo) {
        RegNo = regNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(Integer categoryId) {
        CategoryId = categoryId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getPerson() {
        return Person;
    }

    public void setPerson(String person) {
        Person = person;
    }

    public Boolean getHasImage() {
        return HasImage;
    }

    public void setHasImage(Boolean hasImage) {
        HasImage = hasImage;
    }

    public String getFlow() {
        return Flow;
    }

    public void setFlow(String flow) {
        Flow = flow;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getFlowStatus() {
        return FlowStatus;
    }

    public void setFlowStatus(String flowStatus) {
        FlowStatus = flowStatus;
    }

    public String getFlowStatusDesc() {
        return FlowStatusDesc;
    }

    public void setFlowStatusDesc(String flowStatusDesc) {
        FlowStatusDesc = flowStatusDesc;
    }

    public String getAppDate() {
        return AppDate;
    }

    public void setAppDate(String appDate) {
        AppDate = appDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public long getUpdateStamp() {
        return updateStamp;
    }

    public void setUpdateStamp(long updateStamp) {
        this.updateStamp = updateStamp;
    }
}
