package com.weijun.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name="t_mailpasswd")
@Entity
public class MailPasswd {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long mailPasswdID;

    @Column(name="lawyerNmae",unique = true)
    String lawyerNmae;

    @Column(name="Email")
    String Email;

    @Column(name="EmailPasswd")
    String EmailPasswd;

    public String getLawyerNmae() {
        return lawyerNmae;
    }

    public void setLawyerNmae(String lawyerNmae) {
        this.lawyerNmae = lawyerNmae;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getEmailPasswd() {
        return EmailPasswd;
    }

    public void setEmailPasswd(String emailPasswd) {
        EmailPasswd = emailPasswd;
    }
}
