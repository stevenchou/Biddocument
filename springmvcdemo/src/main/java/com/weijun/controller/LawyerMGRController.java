package com.weijun.controller;

import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONArray;
import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONObject;
import com.weijun.entity.CertificateCard;
import com.weijun.entity.Lawyer;
import com.weijun.service.LawyerService;
import com.weijun.util.FilelTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class LawyerMGRController {
    @Autowired
    private LawyerService lawyerService;

    @Autowired
    ServletContext context;

    @Autowired
    FilelTool filelTool;

    private static final Logger logger = LogManager.getLogger(LawyerMGRController.class.getName());

    @RequestMapping(value = "uploadlawyerinfo", method = RequestMethod.GET)
    public String uploadlawyerinfo() {
        return "/Lawyer/uploadlawyerinfo";
    }

    @RequestMapping(value = "uploadlawyerinfo", method = RequestMethod.POST)
    public String uploadlawyerinfopost(String lawyername, String gender, String birthday, String employeddate,
                                       String position, String telephone, String IDcardno,
                                       String email, String[] eduinfolist, String[] workiexplist, String[] professionalfieldlist,
                                       @RequestParam(value = "inputIDcard") MultipartFile inputIDcard, @RequestParam(value = "inputpersonalimg") MultipartFile inputpersonalimg) {
        logger.info("Fetching file");
        //存储图片的物理路径
        String pic_path = context.getRealPath("") + File.separator + "upload" + File.separator;


        String inputIDcardName = inputIDcard.getOriginalFilename();
        logger.info("the inputIDcard is " + inputIDcardName + ",now save it");
        filelTool.saveFileToServer(inputIDcard, pic_path + "/IDcard/");

        String inputpersonalimgName = inputpersonalimg.getOriginalFilename();
        logger.info("the inputpersonalimg is " + inputpersonalimgName + ",now save it");
        filelTool.saveFileToServer(inputpersonalimg, pic_path + "/personimg/");

        Lawyer lawyer = new Lawyer();
        lawyer.setLawyername(lawyername);
        lawyer.setGender(gender.toCharArray());
        lawyer.setEmail(email);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date birthdayDate = sdf.parse(birthday);
            lawyer.setBirthday(birthdayDate);

            Date empdate = sdf.parse(employeddate);
            lawyer.setEmployeddate(empdate);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
        lawyer.setUpdatedate(new Date());
        lawyer.setPosition(position);
        lawyer.setTelephone(telephone);
        lawyer.setIDcardno(IDcardno);
        lawyer.setIDcardpath("/upload/IDcard/" + inputIDcardName);
        lawyer.setPersonalimgpath("/upload/personimg/" + inputpersonalimgName);
        lawyer.setEduinfolist(Arrays.asList(eduinfolist));
        lawyer.setWorkiexplist(Arrays.asList(workiexplist));
        lawyer.setProfessionalfieldlist(Arrays.asList(professionalfieldlist));
        //将律师信息添加到数据库
        lawyerService.addLawyer(lawyer);

//        跳转至律师列表
        return "redirect:/lawyerlist";
    }


    @RequestMapping(value = "/getlawyerdata", method = RequestMethod.GET)
    public void getlawyerdata(HttpServletResponse response) {
        List<Lawyer> lawyerList = lawyerService.getAllLawyerList();
        JSONArray retjson = new JSONArray();
        for (Lawyer lawyer : lawyerList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("DB_id", lawyer.getLawyerid());
            jsonObject.put("lawyername", lawyer.getLawyername());
            jsonObject.put("gender", lawyer.getGender());
            jsonObject.put("birthday", lawyer.getBirthday());
            jsonObject.put("employdate", lawyer.getEmployeddate());
            jsonObject.put("telephone", lawyer.getTelephone());
            jsonObject.put("ID_num", lawyer.getIDcardno());
            jsonObject.put("ID_cardpath", lawyer.getIDcardpath());
            jsonObject.put("personalimg", lawyer.getPersonalimgpath());
            jsonObject.put("position", lawyer.getPosition());
            if (null != lawyer.getCertificateCard()) {

                jsonObject.put("certificatecardNo", lawyer.getCertificateCard().getCertificatecardid());
                jsonObject.put("certificatecardpath", lawyer.getCertificateCard().getCertificatecardpath());
            }
            jsonObject.put("eduinfos", lawyer.getEduinfolist().toString());
            jsonObject.put("workexperiences", lawyer.getWorkiexplist().toString());
            jsonObject.put("professionfields", lawyer.getProfessionalfieldlist().toString());
            retjson.add(jsonObject);
        }
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();

            out.print(retjson.toJSONString());
            logger.info("return value of lawyer information is " + retjson.toJSONString());
            out.flush();
        } catch (IOException e) {
            logger.error(e);
        } finally {
            if (null != out) {
                out.close();
            }
        }

        lawyerService.updateLawyerModifyTime(lawyerList);
    }

    @RequestMapping(value = "/lawyerlist", method = RequestMethod.GET)
    public ModelAndView lawyerlist_get() {

        ModelAndView modelAndView = new ModelAndView("/Lawyer/lawyerlist");
        return modelAndView;

        // TODO: 2018/8/4 应更新lawyer的修改时间
    }

    /*
     * This method will delete an lawyer by it's id.
     */
    @RequestMapping(value = {"/delete-{lawyerid}-lawyer"}, method = RequestMethod.POST)
    public void deleteEmployee(@PathVariable String lawyerid, HttpServletResponse response) {
        lawyerService.deleteLawyerByid(Long.parseLong(lawyerid));
        response.setCharacterEncoding("UTF-8");
        response.setStatus(200);
        PrintWriter out = null;
        try {
            out = response.getWriter();

            out.print("Success");
            logger.info("return value of gettestjson is Success");
            out.flush();
        } catch (IOException e) {
            logger.error(e);
        } finally {
            if (null != out) {
                out.close();
            }
        }
// TODO: 2018/8/4 应记用户操作日志
    }

    //    @RequestMapping(value = "/uploadposition",method = RequestMethod.GET)
//    public String uploadposition_get(){
//        return "/Lawyer/uploadposition";
//    }
    @RequestMapping(value = "uploadposition", method = RequestMethod.GET)
    public ModelAndView parterwithsocialposition_get() {
        List<Lawyer> lawyerlist = lawyerService.getAllLawyerList();
        ModelAndView modelAndView = new ModelAndView("/Lawyer/uploadposition");
        modelAndView.addObject("lawyerlist", lawyerlist);
        return modelAndView;
//        页面加载时获取lawyer信息，不用更新修改时间
    }

    @RequestMapping(value = "uploadposition", method = RequestMethod.POST)
    public String parterwithsocialposition_post(String[] socialPosition, HttpServletRequest request, HttpServletResponse response) {
        String lawyername = request.getParameter("lawyername");
        Lawyer lawyer = lawyerService.getLawyerByName(lawyername);
        lawyer.setSocialpositionlist(Arrays.asList(socialPosition));
        lawyerService.updateLawyer(lawyer);
        String message = "更新律师的社会职务成功";
        logger.info(message);
        return "redirect:/socialPositionList";
    }

    @RequestMapping(value = "socialPositionList",method = RequestMethod.GET)
    public ModelAndView socialPositionList(){
        ModelAndView modelAndView = new ModelAndView("/Lawyer/socialPositionList");
        List<BigInteger> lawyerNameHasSocialPosition=lawyerService.getLawyerNameBySocialPosition();
        List<Lawyer> lawyerListHasSocialPosition=lawyerService.getLawyersBySocialPosition(lawyerNameHasSocialPosition);
        modelAndView.addObject("lawyerListHasSocialPosition", lawyerListHasSocialPosition);

        return modelAndView;
    }

    @RequestMapping(value = "uploadcertificate", method = RequestMethod.GET)
    public ModelAndView uploadcertificate_get() {
        List<Lawyer> lawyerlist = lawyerService.parterwithsocialposition();
        ModelAndView modelAndView = new ModelAndView("/Lawyer/uploadcertificate");
        modelAndView.addObject("lawyernames", lawyerlist);

        return modelAndView;
    }


    @RequestMapping(value = "uploadcertificate", method = RequestMethod.POST)
    public ModelAndView uploadcertificate_post(HttpServletRequest request, String certificateID, String certificatedate, @RequestParam(value = "certificatecard") MultipartFile certificatecard) {
        String lawyername = request.getParameter("lawyername");
        Lawyer lawyer = lawyerService.getLawyerByName(lawyername);

        //存储执业证证
        //存储图片的物理路径
        String pic_path = context.getRealPath("") + File.separator + "upload" + File.separator;

        FilelTool filelTool = new FilelTool();

        String certificatecardName = certificatecard.getOriginalFilename();
        logger.info("the certificatecard is " + certificatecard + ",now save it");
        filelTool.saveFileToServer(certificatecard, pic_path + "/certificatecard/");

        CertificateCard certificateCard = new CertificateCard();
        certificateCard.setCertificateID(certificateID);
        certificateCard.setCertificatedate(certificatedate);
        certificateCard.setCertificatecardpath("/upload/certificatecard/" + certificatecardName);

        lawyer.setCertificateCard(certificateCard);
        lawyerService.updateLawyer(lawyer);
        String message = "更新律师的执业证成功";
        ModelAndView modelAndView = new ModelAndView("/allMsgPage");
        modelAndView.addObject("message", message);

//        更新lawyer修改时间
        lawyerService.updateLawyerModifyTime(lawyername);
        return modelAndView;
    }

    @RequestMapping(value = "gettestjson", method = RequestMethod.GET)
    public void gettestjson(HttpServletResponse response) {
        JSONArray retjson = new JSONArray();
        for (int i = 0; i < 30; i++) {
            JSONObject jo1 = new JSONObject();
            jo1.put("age", i);
            jo1.put("name", "yang");
            jo1.put("sex", "男");
            retjson.add(jo1);
        }

        response.setCharacterEncoding("UTF-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();

            out.print(retjson.toJSONString());
            logger.info("return value of gettestjson is " + retjson.toJSONString());
            out.flush();
        } catch (IOException e) {
            logger.error(e);
        } finally {
            if (null != out) {
                out.close();
            }
        }
    }


}
