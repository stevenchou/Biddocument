package com.weijun.controller;

import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONArray;
import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONObject;
import com.weijun.entity.*;
import com.weijun.service.CaseProjectService;
import com.weijun.service.CustomService;
import com.weijun.service.impl.AccountService;
import com.weijun.service.impl.LawyerServiceImpl;
import com.weijun.util.*;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/****
 * 项目controller，管理每一个项目
 *
 * @author 周江
 *
 */
@Controller
public class ProjectController {
    @Autowired
    private CaseProjectService caseProjectService;

    @Autowired
    private LawyerServiceImpl lawyerService;

    @Autowired
    private AccountService accountService;

    @Autowired
    ServletContext context;

    @Autowired
    DateTool dateTool;

    @Autowired
    FilelTool filelTool;

    @Autowired
    SVNTool svnTool;

    @Autowired
    CustomService customService;

    @Autowired
    LogTool logTool;

    private static final Logger logger = LogManager.getLogger(ProjectController.class.getName());


    /**
     * @author: 周江
     * 新建项目，显示newproject.jsp
     *
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping(value = "newproject",method = RequestMethod.GET)
    public ModelAndView newproject_get(){
        ModelAndView modelAndView=new ModelAndView("/caseproject/newproject");

        List<Lawyer> signaturelawyers=lawyerService.getSignatureLawyers();
        modelAndView.addObject("signaturelawyers",signaturelawyers);

        List<Lawyer> responsiblelawyers=lawyerService.getResponsibleLawyers();
        modelAndView.addObject("responsiblelawyers",responsiblelawyers);

        List<Lawyer> teamAlllawyers=lawyerService.getAllLawyerList();
        modelAndView.addObject("teamAlllawyers",teamAlllawyers);

        List<HSCustom> customList= caseProjectService.getCustomList();
        modelAndView.addObject("customslist",customList);


        List<String> projecttypelist= caseProjectService.getProjectTypeList();
        modelAndView.addObject("projecttypelist",projecttypelist);
        return modelAndView;
    }


    @RequestMapping(value = "newproject",method = RequestMethod.POST)
    /***
     * create by: 周江
     * description: 新建项目
     * create time: 5/30/2020 11:03 AM
     *
      * @Param: request http请求
     * @Param: contractfile 合同作为附件
     * @Param: partcontractList 截取的关键页面，作为后续制作标书用
     * @return java.lang.String
     */

    public String newproject_post(HttpServletRequest request, @RequestParam(value="contractfile") MultipartFile contractfile,@RequestParam(value = "partcontract")List<MultipartFile> partcontractList){
        CaseProject caseProject =new CaseProject();
        String projectName =request.getParameter("projectname");
        caseProject.setCasename(projectName);
//        String allprojecttype= request.getParameter("allprojecttype");
        String projectbrief =request.getParameter("projectbrief");
        caseProject.setCasebrief(projectbrief);

        String customName=request.getParameter("allcustom");
        HSCustom custom=caseProjectService.getCustombyName(customName);
        caseProject.setCustom(custom);
        //记录用户日志
        logTool.writeOperateLog(HSCustom.class.getName(),customName,OperateType.query);

        String[] projecttypes=request.getParameterValues("projecttype");
        List<ProjectField> fieldsList=caseProjectService.getProjectTypeListByNames(projecttypes);
        caseProject.setProjectfields(fieldsList);

        String signaturelawyer =request.getParameter("signaturelawyer");
        Lawyer signLawyer = lawyerService.getLawyerByName(signaturelawyer);
        caseProject.setSignatureLawyer(signLawyer);
        //        记录用户日志
        logTool.writeOperateLog(Lawyer.class.getName(),signaturelawyer,OperateType.query);

        String[] responsiblelawyers=request.getParameterValues("responsiblelawyers");
        List<Lawyer>responLawyers =lawyerService.getResponsibleLawyersByNames(responsiblelawyers);
        caseProject.setResponsibleLawyers(responLawyers);

        String[] teamAlllawyers=request.getParameterValues("teamAlllawyers");
        List<Lawyer> teamLawyers =lawyerService.getResponsibleLawyersByNames(teamAlllawyers);
        caseProject.setTeamnumbers(teamLawyers);
        logTool.writeOperateLog(Lawyer.class.getName(),Arrays.asList(teamAlllawyers),OperateType.query);
        String signaturedate =request.getParameter("signaturedate");
        String terminationdate=request.getParameter("terminationdate");
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date signdate = sdf.parse(signaturedate);
            caseProject.setSignaturedate(signdate);

            Date termdate =sdf.parse(terminationdate);
            caseProject.setTerminationdate(termdate);
        }
        catch (ParseException e)
        {
            System.out.println(e.getMessage());
        }

        logger.info("Fetching the project contract file");
        //存储图片的物理路径
        String pic_path = context.getRealPath("") +  "upload" + File.separator+"projects"+ File.separator+projectName;


        String contractfileName = contractfile.getOriginalFilename();
        logger.info("the contractfile is " + contractfileName+",now save it");
        filelTool.saveFileToServer(contractfile,pic_path+File.separator+"contract"+File.separator);

        String contractFilePath="upload/projects/"+projectName+"/contract/"+contractfileName;
        caseProject.setEvidencefilepath(contractFilePath);

        //标书中截取的部分合同文件
        List<String> partContractFileNameList=new ArrayList<String>();
        String partContractFilePathBase="upload/projects/"+projectName+"/partcontract/";
        for (MultipartFile file:partcontractList) {
            String filename=file.getOriginalFilename();
            partContractFileNameList.add(partContractFilePathBase+filename);
            logger.info("the partContractFilePath is "+partContractFilePathBase+filename);
            filelTool.saveFileToServer(file,pic_path+File.separator+"partcontract"+File.separator);
        }
//        文件保存后，需要更新到SVN服务器
//        svnTool.svnConnitAddedFile(context.getRealPath("batfile"),context.getRealPath("upload"));
        logTool.writeOperateLog(CaseProject.class.getName(),caseProject.getCasename(),OperateType.create);

        caseProject.setPartcontractList(partContractFileNameList);
        caseProjectService.save(caseProject);
        return "redirect:/myprojects";

    }

    @RequestMapping(value = "myprojects",method = RequestMethod.GET)
    public ModelAndView projectlist_get(){
        //get the current username
        String currentusername= (String) SecurityUtils.getSubject().getPrincipal();
        List<CaseProject> caseProjectList=null;
//        显示所有案例
        if("admin".equals(currentusername)){
            caseProjectList=caseProjectService.getAllProject();
        }
        else{
            User currentUser = accountService.getUserByUserName(currentusername);
            caseProjectList= caseProjectService.getMyProjects(currentUser.getLawyer());

        }
        ModelAndView modelAndView =new ModelAndView("/caseproject/projectsthumbnail");
        modelAndView.addObject("myProjectList",caseProjectList);

        //记录操作日志
        logTool.writeProjectsOperateLog(caseProjectList,OperateType.query);
        return modelAndView;
    }
    @RequestMapping(value = "getprojectdata",method = RequestMethod.GET)
    public void getprojectdata(HttpServletResponse response){
        List<CaseProject> projects = caseProjectService.getAllProject();
        JSONArray retJson = paddingCaseProjectToJsonObject(projects);

        ResponseTool tool=new ResponseTool();
        tool.writeJSONtoClient(response,retJson);

        //记录操作日志
        logTool.writeProjectsOperateLog(projects,OperateType.query);
    }



    private JSONArray paddingCaseProjectToJsonObject(List<CaseProject> projects) {
        JSONArray retJson = new JSONArray();
        for (CaseProject project:projects) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("caseprojectid",project.getCaseprojectid());
            jsonObject.put("casename",project.getCasename());
            jsonObject.put("casebrief",project.getCasebrief());
            jsonObject.put("custom",project.getCustom().getCustomname());
            jsonObject.put("signaturedate",project.getSignaturedate());
            jsonObject.put("terminationdate",project.getTerminationdate());
            jsonObject.put("signatureLawyer",project.getSignatureLawyer().getLawyername());
//            获取项目负责律师
            String responsibleLawyers="";
            for (Lawyer lawyer:project.getResponsibleLawyers()
                 ) {
                if(null!=lawyer.getLawyername()){
                    responsibleLawyers += lawyer.getLawyername()+",";
                }

            }
            jsonObject.put("responsibleLawyers",responsibleLawyers);

//            获取团队成员
            String teamnumbers="";
            for (Lawyer lawyer:project.getTeamnumbers()
                    ) {
                if(null!=lawyer.getLawyername()){
                    teamnumbers += lawyer.getLawyername()+",";
                }

            }
            jsonObject.put("teamnumbers",teamnumbers);
            jsonObject.put("evidencefilepath",project.getEvidencefilepath());

            //get the projectfields
            String projectfields="";
            for (ProjectField field:project.getProjectfields()
                 ) {
                if(null!=field.getProjettypename()){
                    projectfields +=field.getProjettypename()+",";
                }

            }
            jsonObject.put("projectfields",projectfields);
//            jsonObject.put("",);

            retJson.add(jsonObject);
        }
        return retJson;
    }

    @RequestMapping(value = "newprojecttype",method = RequestMethod.GET)
    public String newprojecttype_get(){

        return "caseproject/newprojecttype";
    }



    @RequestMapping(value = "newprojecttype",method = RequestMethod.POST)
    public void newprojecttype_post(ProjectField fields, HttpServletRequest request, HttpServletResponse response){
        caseProjectService.addProjectField(fields);
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.print(fields.getProjettypename());
            out.flush();
        } catch (IOException e) {
            logger.error(e);
        } finally {
            if (null != out) {
                out.close();
            }
        }

        logTool.writeOperateLog(ProjectField.class.getName(),fields.getProjettypename(),OperateType.create);
    }


    @RequestMapping(value = {"/delete-{projectid}-project"}, method = RequestMethod.GET)
    /**
     * create by: 周江
     * description: 根据项目ID，删除项目，此方法应慎重调用，权限必须是admin
     * create time: 5/29/2020 6:14 PM
     *
      * @Param: projectid 项目ID
     * @Param: response HttpServletResponse
     * @return void
     */

    public void deleteEmployee(@PathVariable String projectid, HttpServletResponse response) {

        caseProjectService.deleteProjectByid(Long.parseLong(projectid));

        ResponseTool tool = new ResponseTool();
        tool.writeStringtoClient(response, "Success");

        // TODO: 2018/8/4 应该删除服务器上文件，并提交SVN

//        记录操作日志
        CaseProject caseProject=caseProjectService.getProjectByID(Long.parseLong(projectid));
        logTool.writeOperateLog(CaseProject.class.getName(),caseProject.getCasename(),OperateType.delete);
    }


    @RequestMapping(value = "selectprojects",method = RequestMethod.GET)
    public void getprojectByYearAndType(String projectBeginDate,String projectEndDate, String selectbytype,HttpServletResponse response) {
        logger.info("get projects by selected years and type");


        List<String> typeList=Arrays.asList(selectbytype.split(","));
        List<CaseProject> projects=caseProjectService.getprojectByYearAndType(projectBeginDate,projectEndDate,typeList);

        JSONArray retJson = paddingCaseProjectToJsonObject(projects);

        ResponseTool tool=new ResponseTool();
        tool.writeJSONtoClient(response,retJson);

//        记录操作日志
        logTool.writeProjectsOperateLog(projects,OperateType.query);
    }


    @RequestMapping(value = {"/myprojects-{projectid}-detail"}, method = RequestMethod.GET)
    public ModelAndView projectDetail(@PathVariable String projectid, HttpServletResponse response) {
        CaseProject caseProject=caseProjectService.getProjectByID(Long.valueOf(projectid));
        ModelAndView modelAndView =new ModelAndView("/caseproject/projectdetail");

        modelAndView.addObject("projectdetail",caseProject);

        //办理此案件的团队成员直接从caseProject中取,现成的，不用再单独查询

//        获取最常用的一部分客户
        List<HSCustom> customList =customService.getFrequentCustom(Constant.TOPN);
        modelAndView.addObject("customList",customList);

//        记录操作日志
        logTool.writeProjectOperateLog(caseProject,OperateType.query);
        return modelAndView;
    }

    @RequestMapping(value = {"/getProjectFileTreeJSON"}, method = RequestMethod.GET)
    public void getProjectFileTreeJSON(String projectname,HttpServletResponse response){
        logger.info("now we get project file tree,and return json,the project name is "+projectname);
        JSONArray retJson = new JSONArray();
        JSONObject rootobj=new JSONObject();
        rootobj.put("name",projectname);
        rootobj.put("open",true);
        JSONArray sonArray=new JSONArray();
        String searchPath=context.getRealPath("") +  "upload" + File.separator+"projects"+ File.separator+projectname+ File.separator;
        getFileListame(searchPath,sonArray);
        rootobj.put("children",sonArray);
        rootobj.put("path",searchPath);
        retJson.add(rootobj);
        logger.info("the tree view of the project "+projectname+" is "+retJson.toJSONString());
        ResponseTool responseTool =new ResponseTool();
        responseTool.writeJSONtoClient(response,retJson);

//        记录
        logTool.writeOperateLog(CaseProject.class.getName(),projectname,OperateType.query);
    }

    private void getFileListame(String strPath,JSONArray array) {

        File dir = new File(strPath);
        File[] files = dir.listFiles(); // 该文件目录下文件全部放入数组
        if (files != null) {
            for (int i = 0; i < files.length; i++) {

                if (files[i].isDirectory()) { // 判断是文件还是文件夹
                    JSONObject directoryobj=new JSONObject();
                    directoryobj.put("name",files[i].getName());
                    JSONArray jsonArray = new JSONArray();
//                    System.out.println(files[i].getName());
                    getFileListame(files[i].getAbsolutePath(),jsonArray); // 获取文件绝对路径
                    directoryobj.put("children",jsonArray);
                    directoryobj.put("path",files[i].getAbsolutePath());
                    directoryobj.put("isParent",true);
                    array.add(directoryobj);
                }else {
                    JSONObject fileobj = new JSONObject();
                    fileobj.put("name",files[i].getName());
                    fileobj.put("path",files[i].getAbsolutePath());
                    array.add(fileobj);
//                    System.out.println(files[i].getName());
                    //System.out.println("the parent is "+files[i].getParent().);
                }

            }

        }

    }


    @RequestMapping(value = "deleteProjectfile",method = RequestMethod.GET)
    public void deleteProjectfile(String filename,HttpServletResponse response){
        logger.info("the client delete the file in ztree,the file must delete on server,the file have to delete is "+filename);
        File file = new File(filename); // 输入要删除的文件位置
        if(file.exists()){
//            f.delete();这个函数不能删除子目录
            try {
                FileUtils.deleteDirectory(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!file.exists()){
            logger.info("delete the file successfully!"+filename);
            ResponseTool responseTool =new ResponseTool();
            responseTool.writeStringtoClient(response,"delete file successfully");

            //svn delete and commit
            if(svnTool.isSVNRunning()){
                logger.info("the svn server is running,now we commit the delete");
                logger.info("the svn bat file path is "+context.getRealPath("batfile"));
                logger.info("the upload path is "+context.getRealPath("upload"));
                svnTool.svnConnitDeletedFile(context.getRealPath("batfile"),context.getRealPath("upload"));
            }

        }else {
            logger.info("delete the file failed!"+filename);
            ResponseTool responseTool =new ResponseTool();
            responseTool.writeStringtoClient(response,"delete file failed");
        }

//        记录操作日志
        String projectName=caseProjectService.getProjectNameByFileName(filename);
        logTool.writeOperateLog(CaseProject.class.getName(),projectName,OperateType.update);
    }


//    前台创建了目录，服务器相应地应创建文件夹
    @RequestMapping(value = "createProjecfolder",method = RequestMethod.GET)
    public void createProjecfolder(String foldername,HttpServletResponse response){
        logger.info("the client creat new folder by ztree,we must create folder in server,the folder name is "+foldername);
        File f = new File(foldername);
        if (!f.exists()){
            f.mkdirs();
            ResponseTool responseTool =new ResponseTool();
            responseTool.writeStringtoClient(response,"create the folder successfully");

            //svn add and commit
            if(svnTool.isSVNRunning()){
                logger.info("the svn server is running,now we commit the addition");
                logger.info("the svn bat file path is "+context.getRealPath("batfile"));
                logger.info("the upload path is "+context.getRealPath("upload"));
                svnTool.svnConnitAddedFile(context.getRealPath("batfile"),context.getRealPath("upload"));
            }
        }

//        记录操作日志
        String projectName=caseProjectService.getProjectNameByFileName(foldername);
        logTool.writeOperateLog(CaseProject.class.getName(),projectName,OperateType.update);
    }

    @RequestMapping(value = "modifyProjectfileName",method = RequestMethod.GET)
    public void modifyProjectfileName(String oldName,String newName,HttpServletResponse response){
        logger.info("the client modify file name by ztree,we must modify in server,the old name is "+oldName+",the new name is "+newName);
        File f = new File(oldName);
        File newFile=new File(newName);
        if (f.exists()){
            f.renameTo(newFile);
            logger.info("modify the filename successfully!");
            ResponseTool responseTool =new ResponseTool();
            responseTool.writeStringtoClient(response,"modify the file name successfully");

            //svn commit，修改文件夹名字时，需要提交两次
            svnTool.svnModifyFile(context.getRealPath("batfile"),context.getRealPath("upload"));
            svnTool.svnModifyFile(context.getRealPath("batfile"),context.getRealPath("upload"));

        }else {
            logger.info("the old file is not exist,you can not modify it's name");
            ResponseTool responseTool =new ResponseTool();
            responseTool.writeStringtoClient(response,"modify the file name failed");

        }
        //        记录操作日志
        String projectName=caseProjectService.getProjectNameByFileName(newName);
        logTool.writeOperateLog(CaseProject.class.getName(),projectName,OperateType.update);
    }

    @RequestMapping(value = "uploadFile",method = RequestMethod.POST)
    public void uploadFile(@RequestParam(value = "uploadfile", required = false) MultipartFile file,@RequestParam(value = "savePath", required = false)String savePath,HttpServletResponse response){
        logger.info("the upload file save path is "+savePath+",and the file name is "+file.getOriginalFilename());
        filelTool.saveFileToServer(file,savePath+File.separator);
        ResponseTool responseTool=new ResponseTool();

        File uploadedFile=new File(savePath+File.separator+file.getOriginalFilename());
        if (uploadedFile.exists()){
            responseTool.writeStringtoClient(response,"upload the "+file.getOriginalFilename()+" successfully!");
//            SVN更新修改
            svnTool.svnConnitAddedFile(context.getRealPath("batfile"),context.getRealPath("upload"));
        }else{
            responseTool.writeStringtoClient(response,"upload the "+file.getOriginalFilename()+" failed!");
        }

//        记录操作日志
        String projectName=caseProjectService.getProjectNameByFileName(savePath+File.separator+file.getOriginalFilename());
        logTool.writeOperateLog(CaseProject.class.getName(),projectName,OperateType.update);
        int posOfupload=savePath.indexOf("upload");
        String filePath=savePath.substring(posOfupload)+file.getOriginalFilename();
        logTool.writeOperateLog(File.class.getName(),filePath,OperateType.fileCreate);
    }
}
