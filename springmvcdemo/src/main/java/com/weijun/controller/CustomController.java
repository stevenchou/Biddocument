package com.weijun.controller;

import com.weijun.entity.CompanyCustomInfo;
import com.weijun.entity.HSCustom;
import com.weijun.entity.PersonalCustomInfo;
import com.weijun.service.CustomService;
import com.weijun.util.DateTool;
import com.weijun.util.FilelTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

@Controller
public class CustomController {
    private static final Logger logger= LogManager.getLogger(CustomController.class.getName());
    @Autowired
    DateTool dateTool;
    @Autowired
    FilelTool filelTool;
    @Autowired
    ServletContext context;

    @Autowired
    CustomService customService;

    @RequestMapping(value = "newcustom",method = RequestMethod.GET)
    public ModelAndView newCustom(){
        ModelAndView modelAndView =new ModelAndView("custom/newCustom");
        return modelAndView;
    }
    @RequestMapping(value = "newpersonalcustom",method = RequestMethod.GET)
    public ModelAndView newPersonalCustom(){
        ModelAndView modelAndView =new ModelAndView("custom/newPersonalCustom");
        return modelAndView;
    }

    @RequestMapping(value = "newcompanycustom",method = RequestMethod.GET)
    public ModelAndView newCompanyCustom(){
        ModelAndView modelAndView =new ModelAndView("custom/newCompanyCustom");
        return modelAndView;
    }


    @RequestMapping(value = "newcustom",method = RequestMethod.POST)
    public String newcustom_post(HttpServletRequest request) {
        HSCustom hsCustom=new HSCustom();
        String customName=request.getParameter("customName");
        hsCustom.setCustomname(customName);

        String customBrief=request.getParameter("customBrief");
        hsCustom.setCustombrief(customBrief);

        String customEmail=request.getParameter("customEmail");
        hsCustom.setEmail(customEmail);

        String customAddress=request.getParameter("customAddress");
        hsCustom.setAddress(customAddress);

        String contactname=request.getParameter("contactname");
        if (""==contactname||null==contactname){
            //个人客户
            hsCustom.setCustomtype("personalCustom");
            PersonalCustomInfo personalCustomInfo =new PersonalCustomInfo();

            String gender=request.getParameter("customGender");
            personalCustomInfo.setGender(gender);

            MultipartFile customImg=((MultipartHttpServletRequest) request).getFile("customImg");
            String personCustomImgDBPath="upload"+File.separator+"customs"+File.separator+customImg.getOriginalFilename();
            String personCustomImgServerPath=context.getRealPath("")+File.separator+"upload"+File.separator+"customs"+File.separator;
            filelTool.saveFileToServer(customImg,personCustomImgServerPath);
            hsCustom.setImgpath(personCustomImgDBPath);

            hsCustom.setPersonalcustominfo(personalCustomInfo);
        }else {
            //公司客户
            hsCustom.setCustomtype("companyCustom");
            CompanyCustomInfo companyCustomInfo =new CompanyCustomInfo();
            companyCustomInfo.setContactname(contactname);

            String contactphone =request.getParameter("contactphone");
            companyCustomInfo.setContactphone(contactphone);

            String contactgender =request.getParameter("contactgender");
            companyCustomInfo.setContactgender(contactgender);

            MultipartFile companyLogo=((MultipartHttpServletRequest) request).getFile("companyLogo");
            String companyLogoDBPath="upload"+File.separator+"customs"+File.separator+companyLogo.getOriginalFilename();
            String companyLogoServerPath=context.getRealPath("upload")+File.separator+"customs"+File.separator;
            filelTool.saveFileToServer(companyLogo,companyLogoServerPath);
            hsCustom.setImgpath(companyLogoDBPath);

            hsCustom.setCompanycustominfo(companyCustomInfo);
        }

        //hsCustom构造好，填入数据库
        customService.saveCustom(hsCustom);
        return "redirect:/customlist";
    }

    @RequestMapping(value = "/customlist",method = RequestMethod.GET)
    public ModelAndView customlist(){
        ModelAndView modelAndView =new ModelAndView("custom/customList");
        List<HSCustom> customList=customService.getAllCustom();
        modelAndView.addObject("customList",customList);
        return modelAndView;
    }
}
