package com.weijun.controller;

import com.weijun.entity.*;
import com.weijun.service.CaseProjectService;
import com.weijun.service.CustomService;
import com.weijun.service.LawyerService;
import com.weijun.service.MailService;
import com.weijun.service.impl.AccountService;
import com.weijun.util.Constant;
import com.weijun.util.MailTool;
import com.weijun.util.ResponseTool;
import com.weijun.util.SecurityTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class EmailController {
    @Autowired
    SecurityTool securityTool;
    @Autowired
    AccountService accountService;
    @Autowired
    CustomService customService;
    @Autowired
    CaseProjectService caseProjectService;
    @Autowired
    private LawyerService lawyerService;
    @Autowired
    private ResponseTool responseTool;
    @Autowired
    private MailService mailService;
    @Autowired
    private MailTool mailTool;

    private static final Logger logger = LogManager.getLogger(EmailController.class.getName());

    @RequestMapping(value = {"/emailto-{projectName}"},method = RequestMethod.GET)
    public ModelAndView emailToOthers(@PathVariable String projectName){
        logger.info("email to others,the projectName is "+projectName);
        ModelAndView modelAndView =new ModelAndView("email/email");
        CaseProject caseProject=caseProjectService.getProjectByName(projectName);
        modelAndView.addObject("teamNumbers",caseProject.getTeamnumbers());

        //get current user
        String currentUserName=securityTool.getCurrentUser();
        User currentUser=accountService.getUserByUserName(currentUserName);
        modelAndView.addObject("currentUser",currentUser);

        //获取最常用的一部分客户
        List<HSCustom> customList =customService.getFrequentCustom(Constant.TOPN);
        modelAndView.addObject("customList",customList);

        //邮件常用问候语

        String greetings="------------------<br>顺祝<br>";
        greetings+="&emsp; 近祺!";
        greetings+="<br><br>";
        greetings+=currentUser.getLawyer().getLawyername()+"<br>";
        greetings+="华商林李黎（前海）联营律师事务所<br>";
        greetings+="电话："+currentUser.getLawyer().getTelephone()+"<br>";
        greetings+="电邮："+currentUser.getLawyer().getEmail()+"<br>";
        greetings+="深圳市前海合作区万科企业公馆3号馆1层B单元";
        modelAndView.addObject("greetings",greetings);
        return modelAndView;
    }

    //send the email to receiver
    @RequestMapping(value = "sendMail",method = RequestMethod.GET)
    public void sendMail(HttpServletRequest request,HttpServletResponse response){
        logger.info("now send the mail to receiver");

        String currentUserName=securityTool.getCurrentUser();

        try {
            Session session=mailTool.generateSession(currentUserName);
            // 通过session得到transport对象
            Transport ts = session.getTransport();
            User currentUser=accountService.getUserByUserName(currentUserName);
            String userEmail=currentUser.getLawyer().getEmail();
            logger.info("the sender's email is "+userEmail);
            MailPasswd mailPasswd=mailService.getMailPasswdByLawyerName(currentUser.getLawyer().getLawyername());
//        ts.connect("smtp.exmail.qq.com","zhoujiang@hs-lll.cn", "mhs2RSC7pjg4RbKq");
            ts.connect("smtp.exmail.qq.com",userEmail, mailPasswd.getEmailPasswd());
            // 创建邮件
            Message message = mailTool.createSimpleMail(session,request,userEmail);
            // 发送邮件
            ts.sendMessage(message, message.getAllRecipients());
            ts.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("send the mail successfully!");
        responseTool.writeStringtoClient(response,"send the mail successfully!");
    }

    @RequestMapping(value = "mailpasswd",method = RequestMethod.GET)
    public ModelAndView mailPassword(){
        logger.info("get the lawyers' email password");
        ModelAndView modelAndView=new ModelAndView("/email/mailpasswd");
        List<Lawyer> lawyerlist = lawyerService.getAllLawyerList();
        modelAndView.addObject("lawyerlist",lawyerlist);
        return modelAndView;
    }

    @RequestMapping(value = "mailpasswd",method = RequestMethod.POST)
    public ModelAndView mailPassword_post(String lawyername,String lawyerEmail,String emailpasswd){
        MailPasswd mailPasswd=new MailPasswd();
        mailPasswd.setLawyerNmae(lawyername);
        mailPasswd.setEmail(lawyerEmail);
        mailPasswd.setEmailPasswd(emailpasswd);
        mailService.saveMailPass(mailPasswd);
        ModelAndView modelAndView=new ModelAndView("/allMsgPage");
        modelAndView.addObject("message","config email password successfully!");
        return modelAndView;
    }
//    @RequestMapping(value = "mailpasswdlist",method = RequestMethod.GET)
//    public ModelAndView mailpasswdlist(){
//        List<MailPasswd> mailPasswdList=mailService.getMailAasswdList();
//    }

    @RequestMapping(value = "getLawyerEmail",method = RequestMethod.GET)
    public void getLawyerEmail(String lawyername, HttpServletResponse response){
        logger.info("the selected lawyer name is "+lawyername);
        Lawyer lawyer=lawyerService.getLawyerByName(lawyername);
        if (null!=lawyer){
            responseTool.writeStringtoClient(response,lawyer.getEmail());
        }else {
            responseTool.writeStringtoClient(response,"can not get the email");
        }

    }
}
