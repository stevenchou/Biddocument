package com.weijun.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.util.TypeUtils;
import com.weijun.entity.IP.CopyRight;
import com.weijun.entity.IP.TradeMark;
import com.weijun.service.impl.IPService;
import com.weijun.util.Constant;
import com.weijun.util.DateTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <h3>springmvcdemo</h3>
 * <p>知识产权controller</p>
 *
 * @author : 周江
 * @date : 2020-06-06 12:21
 **/

@Controller
public class IPController {
    private static final Logger logger = LogManager.getLogger(IPController.class.getName());

    @Autowired
    DateTool dateTool;
    @Autowired
    IPService ipService;

    @CrossOrigin
    @RequestMapping(value = "/getCompanyMarkBrief",method = RequestMethod.GET)
    @ResponseBody
    public JSON getCompanyMarkBrief(String companyName, HttpServletResponse response){
//
//        response.setHeader("Access-Control-Allow-Origin","*");
//        response.setHeader("Access-Control-Allow-Methods","POST");
//        response.setHeader("Access-Control-Allow-Headers","Access-Control");
//        response.setHeader("Allow","POST");
//        获取商标简要信息
        List<TradeMark> companyMarkList=ipService.getMarkListByCompanyname(companyName);
        if (0==companyMarkList.size()){
            //数据库中没有相关数据，需要直接从企查查获取
            return ipService.getMarkListFromQCCAndSaveToDB(companyName);
        }
//        数据库中查询结果有数据，根据数据的更新时间，判断是否要去企查查获取最新的数据
        else {
            long updateStamp=companyMarkList.get(0).getUpdateStamp();
            //        获取请求时间
            long currentStamp=dateTool.getCurrentDatestamp();
            boolean isoverTime=dateTool.isOverTime(currentStamp,updateStamp,Constant.overtimeDays);
            if(isoverTime){
//            数据库中的数据更新时间跟现在的时间相比，已经超过限额
                JSON markArray=ipService.getMarkListFromQCCAndSaveToDB(companyName);
                return markArray;
            }
            else {
//            没有超期，直接返回前面查询的结果
                TypeUtils.compatibleWithJavaBean = true;
                TypeUtils.compatibleWithFieldName = true;
                JSON array= (JSON) JSON.toJSON((companyMarkList));
                return array;
            }
        }

    }

    @CrossOrigin
    @RequestMapping(value = "/getCopyRights",method = RequestMethod.POST)
    @ResponseBody
    public JSON  getCopyRights(String owner){

//        获取商标简要信息

//        从数据库中查询，暂不需要分页
        List<CopyRight> copyRightList=ipService.getCopyRightListByOwnerName(owner);
        if (0==copyRightList.size()){
            //数据库中没有相关数据，需要直接从企查查获取
            return ipService.getCopyRightsFromQCCAndSaveToDB(owner);
        }
//        数据库中查询结果有数据，根据数据的更新时间，判断是否要去企查查获取最新的数据
        else {
            long updateStamp=copyRightList.get(0).getUpdateStamp();
            //        获取请求时间
            long currentStamp=dateTool.getCurrentDatestamp();
            boolean isoverTime=dateTool.isOverTime(currentStamp,updateStamp, Constant.overtimeDays);
            if(isoverTime){
//            数据库中的数据更新时间跟现在的时间相比，已经超过限额
                JSON markArray=ipService.getCopyRightsFromQCCAndSaveToDB(owner);
                return markArray;
            }
            else {
//            没有超期，直接返回前面查询的结果
//                下面两行，避免将首字母转换成小写
                TypeUtils.compatibleWithJavaBean = true;
                TypeUtils.compatibleWithFieldName = true;
                JSON array= (JSON) JSON.toJSON((copyRightList));
                return array;
            }
        }

    }


    @RequestMapping(value = "/getSoftWareCopyRights",method = RequestMethod.GET)
    @ResponseBody
    public JSON  getSoftWareCopyRights(String owner){
        return null;
    }

    @RequestMapping(value = "/getAllMarks",method = RequestMethod.GET)
    @ResponseBody
    public JSON  getAllMarks(){
        List<TradeMark> tradeMarkList=ipService.getAllMarks();
        String tmp="";
        return null;
    }
}
