package com.weijun.controller;

import com.weijun.entity.OfficeHonor;
import com.weijun.service.OfficeHonorService;
import com.weijun.util.FilelTool;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.util.List;

@Controller
public class OfficeController {
    @Autowired
    ServletContext context;

    @Autowired
    OfficeHonorService officeHonorService;

    @Autowired
    FilelTool filelTool;

    private static final Logger logger = LogManager.getLogger(OfficeController.class.getName());

    @RequestMapping(value = "uploadofficehonor",method = RequestMethod.GET)
    public String uploadofficehonor_get()
    {
        return "officehonor/uploadofficehonor";
    }

    @RequestMapping(value = "uploadofficehonor",method = RequestMethod.POST)
    public String uploadofficehonor_post(String honoryear,String honorcontent,@RequestParam(value="inputhonorfile")MultipartFile inputhonorfile)
    {
        logger.info("Fetching file");
        //存储图片的物理路径
        String pic_path = context.getRealPath("") + File.separator + "upload" + File.separator;


        if (inputhonorfile==null){
            logger.info("can not get inputhonorfile!");
        }
        String inputhonorfileName = inputhonorfile.getOriginalFilename();
        logger.info("the inputIDcard is " + inputhonorfileName+",now save it");
        filelTool.saveFileToServer(inputhonorfile,pic_path+"/officehonor/");

        OfficeHonor officeHonor =new OfficeHonor();
        officeHonor.setHonoryear(Integer.valueOf(honoryear));
        officeHonor.setHonorcontent(honorcontent);
        officeHonor.setHonorfilepath("/upload/officehonor/"+inputhonorfileName);

        officeHonorService.saveOfficeHonor(officeHonor);
        return "redirect:/officehonorlist";
    }

    @RequestMapping(value = "officehonorlist",method = RequestMethod.GET)
    public String officehonorlist_get(ModelMap modelMap){
        List<OfficeHonor> officeHonors = officeHonorService.getAllOfficeHonorList();
        modelMap.addAttribute("officeHonors", officeHonors);
        return "/officehonor/showofficeHonors";
    }

    @RequestMapping(value = { "/delete-{officehonorid}-honor" }, method = RequestMethod.GET)
    public String deleteOfficeHonor(@PathVariable String officehonorid) {
        officeHonorService.deleteOfficeHonorByid(Long.parseLong(officehonorid));
        return "redirect:/officehonorlist";
    }
}
