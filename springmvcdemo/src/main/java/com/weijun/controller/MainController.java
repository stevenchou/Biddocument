package com.weijun.controller;

import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONObject;
import com.weijun.entity.User;
import com.weijun.service.TestService;
import com.weijun.service.impl.AccountService;
import com.weijun.util.ResponseTool;
import com.weijun.util.SecurityTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class MainController {
    @Autowired
    SecurityTool securityTool;

    @Autowired
    AccountService accountService;

    @Autowired
    ResponseTool responseTool;
    private static final Logger logger = LogManager.getLogger(MainController.class.getName());

    @RequestMapping(value ={ "/","/index"} )
    public ModelAndView index(HttpServletRequest request) {
//        返回home页面
        ModelAndView modelAndView=new ModelAndView("home");
        return modelAndView;
    }
    @RequestMapping(value = "/staticPage", method = RequestMethod.GET)
    public String staticPage() {
        return "redirect:/staticpage/final.html";
    }


    @Autowired
    private TestService testService;

    @RequestMapping(value = "test", method = RequestMethod.GET)
    public String test(){
//        实际返回的是views/test.jsp ,spring-mvc.xml中配置过前后缀
        return "test";
    }

    @RequestMapping(value = "springtest", method = RequestMethod.GET)
    public String springTest(){
        return testService.test();
    }


    @RequestMapping(value = "currentUserImg",method = RequestMethod.GET)
    public void currentUserImg(HttpServletResponse response){
        //get current user
        String currentUserName=securityTool.getCurrentUser();
        User currentUser=accountService.getUserByUserName(currentUserName);
        String userImg=currentUser.getLawyer().getPersonalimgpath();

//        JSONArray jsonArray =new JSONArray();
        JSONObject jsonObject =new JSONObject();
        jsonObject.put("userName",currentUserName);
        jsonObject.put("userImg",userImg);

        responseTool.writeStringtoClient(response,jsonObject.toJSONString());
    }

    @RequestMapping(value = "welcome", method = RequestMethod.GET)
    public ModelAndView welcome(){

        ModelAndView model = new ModelAndView("welcome");
        return model;
    }
}
