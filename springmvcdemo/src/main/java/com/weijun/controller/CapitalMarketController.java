package com.weijun.controller;

import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONArray;
import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONObject;
import com.weijun.entity.RelativeCompany;
import com.weijun.entity.HSCustom;
import com.weijun.service.CustomService;
import com.weijun.util.ResponseTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/****
 * 资本市场controller，配置客户和公司的关联关系
 *
 * @author 周江
 *
 */
@Controller
public class CapitalMarketController {
    @Autowired
    CustomService customService;
    @Autowired
    ResponseTool responseTool;

    private static final Logger logger = LogManager.getLogger(CapitalMarketController.class.getName());

    @RequestMapping(value = "personalNewRelativeCompany",method = RequestMethod.GET)
    /**
     * create by: 周江
     * description: 个人用户新增关联公司
     * create time: 5/30/2020 12:52 AM
     *
      * @Param: null
     * @return org.springframework.web.servlet.ModelAndView
     */

    public ModelAndView personalAddRelation(){
        ModelAndView modelAndView=new ModelAndView("/capitalmarket/personalNewRelativeCompany");
        List<HSCustom> personalCustomList=customService.getAllPersonalCustom();
        modelAndView.addObject("personalCustomList",personalCustomList);
        return modelAndView;
    }


    @RequestMapping(value = "personalNewRelativeCompany",method = RequestMethod.POST)
    /**
     * create by: 周江
     * description: 将客户（名字）与公司关联起来
     * create time: 5/30/2020 1:04 AM
     *
      * @Param: relativeCompany 关联的公司，是一个类RelativeCompany
     * @Param: customName 客户名字
     * @Param: response HttpServletResponse
     * @return void
     */

    public void newPersonalRelativeCompany(RelativeCompany relativeCompany, String customName, HttpServletResponse response){
        HSCustom custom=customService.getCustomByName(customName);
        List<RelativeCompany> relativeCompanyList=custom.getRelativeCompanyList();

        customService.saveRelativeCompany(relativeCompany);
        relativeCompanyList.add(relativeCompany);
        custom.setRelativeCompanyList(relativeCompanyList);

        customService.updateCustom(custom);
        responseTool.writeStringtoClient(response,"添加关联公司成功，如需继续添加，请输入新的关联公司信息！");
    }

    @RequestMapping(value = "getRelativeCompaniesInfo",method = RequestMethod.GET)
    public ModelAndView relativeCompaniesInfo(){
        List<HSCustom> personalCustomList=customService.getAllPersonalCustom();
        ModelAndView modelAndView =new ModelAndView("/capitalmarket/getRelativeCompaniesInfo");
        modelAndView.addObject("personalCustomList",personalCustomList);
        return modelAndView;
    }

    @RequestMapping(value = "getRelativeCompaniesByCustomName",method = RequestMethod.GET)
    public void getRelativeCompaniesByCustomName(HttpServletRequest request,HttpServletResponse response){
        String customName=request.getParameter("customName");
        HSCustom custom=customService.getCustomByName(customName);

        List<RelativeCompany> relativeCompanyList=custom.getRelativeCompanyList();
        JSONArray retJson = new JSONArray();
        for (RelativeCompany company:relativeCompanyList
             ) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("companyName",company.getRelativeCompanyName());
            jsonObject.put("creditCode",company.getRelativeCompanyCreditCode());
            retJson.add(jsonObject);
        }
        responseTool.writeJSONtoClient(response,retJson);
    }

}
