package com.weijun.controller;

import com.alibaba.fastjson.JSONObject;
import com.weijun.entity.Lawyer;
import com.weijun.entity.Role;
import com.weijun.entity.User;
import com.weijun.entity.UserRole;
import com.weijun.service.LawyerService;
import com.weijun.service.impl.AccountService;
import com.weijun.shiro.LoginDTO;
import com.weijun.util.LogTool;
import javafx.beans.binding.MapExpression;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;


/****
 * 用户登录Controller
 *
 * @author 周江
 *
 */
@Controller
public class LoginController {
    // 处理用户业务类
    @Autowired
    private AccountService accountService;
    @Autowired
    private LawyerService lawyerService;

    @Autowired
    private LogTool logTool;

    private static final Logger logger = LogManager.getLogger(LoginController.class.getName());
    /***
     * 跳转到登录页面
     *
     * @return 返回login.jsp，用户输入账号密码即可登录
     */
    @RequestMapping(value = "login",method = RequestMethod.GET)
    @ResponseBody
    public JSONObject Login_get() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msg", "请使用POST方法先登录");
        return jsonObject;
    }


    /***
     * create by: 周江
     * description: 实现用户登录，登录完成后，重定向到home.jsp
     * create time: 5/30/2020 12:59 AM
     *
      * @Param: username 用户名
     * @Param: password 密码
     * @Param: redirectAttributes 重定向之后还能带参数跳转
     * @return org.springframework.web.servlet.ModelAndView
     */
    @RequestMapping(value ="login",method = RequestMethod.POST,produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public JSONObject Login(String username, String password) {
        JSONObject jsonObject = new JSONObject();
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            subject.login(token);
            jsonObject.put("token", subject.getSession().getId());
            jsonObject.put("msg", "登录成功");
        } catch (IncorrectCredentialsException e) {
            jsonObject.put("msg", "密码错误");
        } catch (LockedAccountException e) {
            jsonObject.put("msg", "登录失败，该用户已被冻结");
        } catch (AuthenticationException e) {
            jsonObject.put("msg", "该用户不存在");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @RequestMapping(value = "logout")
    @ResponseBody
    public JSONObject Logout() {
        JSONObject jsonObject = new JSONObject();
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
//        if(subject.isAuthenticated()){
//            subject.logout();
//        }
        jsonObject.put("msg", "该用户已经退出系统");
        return jsonObject;
    }

//    没有登录时调用后台API就返回此错误
    @RequestMapping(value = "/unlogin")
    @ResponseBody
    public JSONObject UnLogin() {
        JSONObject jsonObject = new JSONObject();
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        jsonObject.put("msg", "该用户尚未登录");
        return jsonObject;
    }

    /**
     * home页面，用户的工作台页面
     * @return 返回home.jsp
     * */
    @RequestMapping(value = "/home")
    public ModelAndView home(){
        ModelAndView modelAndView=new ModelAndView("home");
        return modelAndView;
    }

    /**
     * 未通过认证的请求会被拦截
     * @return unauthorized.jsp
     */
    @RequestMapping("unauthorized")
    @ResponseBody
    public JSONObject unauthorized(){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("msg", "本次访问没有权限");
        return jsonObject;
    }



    @RequestMapping(value="newuser",method = RequestMethod.GET)
    public ModelAndView newuser_get(){
        /**
         * create by: 周江
         * description: 创建一个新的用户，get方法返回页面newuser.jsp
         * create time: 5/29/2020 5:55 PM
         *
          * @Param:
         * @return org.springframework.web.servlet.ModelAndView
         */

        List<Lawyer> lawyers=lawyerService.getAllLawyerList();
        ModelAndView modelAndView = new ModelAndView("user/newuser");
        modelAndView.addObject("lawyers",lawyers);
        return modelAndView;
    }

    @RequestMapping(value="newuser",method = RequestMethod.POST)
    /**
     * create by: 周江
     * description: 创建一个新用户，完成后返回lawyer的清单
     * create time: 5/29/2020 6:01 PM
     *
      * @Param: username 用户名
     * @Param: password 密码
     * @Param: lawyers lawyer name
     * @return java.lang.String 创建完毕，返回lawyer的清单页面userlist.jsp
     */

    public String newuser_post(String username,String password,String lawyers){
        logger.info("it is going to create a new user,the user name is " +lawyers);
        Lawyer lawyer = lawyerService.getLawyerByName(lawyers);
        User newUser = new User();
        newUser.setCreateDate(new Date());
        newUser.setUsername(username);
        newUser.setPassword(password);
        newUser.setLawyer(lawyer);
        accountService.saveUser(newUser);

        User savedUser =accountService.getUserByUserName(username);
        //默认生成的用户都只有律师权限
        Role lawyerRole =accountService.getRoleByName("lawyer");

        UserRole userRole =new UserRole();
        userRole.setRole(lawyerRole);
        userRole.setUser(savedUser);
        accountService.saveUserRole(userRole);

        return "redirect:/userlist";
    }

    @RequestMapping(value = "userlist",method = RequestMethod.GET)
    public ModelAndView userList(){
        List<User> userList=accountService.getAllUser();
        ModelAndView modelAndView =new ModelAndView("/user/userlist");
        modelAndView.addObject("userList",userList);
        return modelAndView;
    }
}