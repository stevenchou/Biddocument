package com.weijun.controller;


import com.weijun.service.BidDocuService;
import com.weijun.service.impl.CaseProjectServiceImpl;
import com.weijun.util.LogTool;
import com.weijun.util.OperateType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;

@Controller

public class BidDocuController {
    private static final long serialVersionUID = -758686623642845302L;
    private static final Logger logger = LogManager.getLogger(BidDocuController.class.getName());
    @Autowired
    ServletContext context;
    @Autowired
    CaseProjectServiceImpl caseProjectService;
    @Autowired
    BidDocuService bidDocuService;
    @Autowired
    LogTool logTool;

    @RequestMapping(value = "openFile")
    public ModelAndView openFile(@RequestParam("filename") String filename, HttpServletRequest request, HttpServletResponse response) {
        logger.info("the file to open is "+filename);
//        PageOfficeCtrl poCtrl = new PageOfficeCtrl(request);
//        request.setAttribute("poCtrl", poCtrl);
//        //设置服务页面
//        poCtrl.setServerPage(request.getContextPath() + "/poserver.zz");
//        poCtrl.setSaveFilePage("savefile");
//        //打开word
//        poCtrl.webOpen(filename, OpenModeType.docAdmin, "steven");
//        poCtrl.setTagId("PageOfficeCtrl1");//此行必须
//        javascript:POBrowser.openWindow('URL','Features','Argument');
        ModelAndView modelAndView = new ModelAndView("biddocument/loadpageoffice");
//        获取project type，用户根据project type选择筛选
        List<String> projecttypelist=caseProjectService.getProjectTypeList();
        modelAndView.addObject("projecttypelist",projecttypelist);
//        记录操作日志
        logTool.writeOperateLog(File.class.getName(),filename,OperateType.fileOpen);
        return modelAndView;
    }

//    @RequestMapping(name = "openStvWDBrowser")
//    public ModelAndView  openStvWDBrowser(){
//        ModelAndView modelAndView = new ModelAndView("biddocument/POBrowser");
//        return modelAndView;
//    }


    @RequestMapping("infoMenu")
    public String infoMenu(Model model) {
        return "infoMenu";
    }






}
