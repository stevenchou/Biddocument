package com.weijun.controller;

import com.weijun.entity.lawfirm.CNInfoAchievement;
import com.weijun.repository.WebSiteRepository;
import com.weijun.service.impl.WebSiteService;
import com.weijun.util.OSTool;
import com.weijun.util.SpiderTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import static java.lang.Thread.sleep;

@Controller
public class SpiderController {
    private static final Logger logger = LogManager.getLogger(SpiderController.class.getName());
    @Autowired
    SpiderTool spiderTool;
    @Autowired
    OSTool osTool;
    @Autowired
    WebSiteRepository webSiteRepository;
    @Autowired
    WebSiteService webSiteService;

    @RequestMapping(value = "getMarks")
    public void getMarks(){
        String driverPath=osTool.tomcatRoot()+"\\tool\\chromedriver.exe";
        WebDriver driver = spiderTool.startSpiderDriver(driverPath);
        String url="http://wcjs.sbj.cnipa.gov.cn/txnS02.do";
        driver.get(url);//打开指定网站
        try {
           sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String markQueryPath="/html/body/div[3]/div[1]/ul/li[2]/table";
        driver.findElement(By.xpath(markQueryPath)).click();
        String imgPath="//*[@id=\"AREA\"]/div/img";
//        验证码存在
        if(spiderTool.isElementExist(driver,imgPath)){
            logger.info("there is verify img,please recognise");
        }
//        验证码不存在或已经通过，进行后续处理
        logger.info("no verify img or go through it");
        driver.quit();
    }


    /**
     * 访问巨潮网，获取广东华商律师事务所的所有业绩，查询关键字是：广东华商律师
     */
    @RequestMapping(value = "getCNInfoAchievement",method = RequestMethod.GET)
    public void getCNInfoAchievement(){
        String driverPath=osTool.tomcatRoot()+"\\tool\\chromedriver.exe";
        WebDriver driver = spiderTool.startSpiderDriver(driverPath);
        String url="http://www.cninfo.com.cn/new/index";
        driver.get(url);//打开指定网站

        String cninfoSearchPath="/html/body/header/div/div[4]/div/div[1]/input";
        WebElement cninfoSearchEle = driver.findElement(By.xpath(cninfoSearchPath));
        cninfoSearchEle.sendKeys("广东华商律师");
        String cninfoSearchBtnPath="/html/body/header/div/div[4]/div/div[1]/span";
        WebElement cninfoSearchBtnEle = driver.findElement(By.xpath(cninfoSearchBtnPath));
        cninfoSearchBtnEle.click();

//      搜索结果页面
        String btnNextPath="//*[@id=\"fulltext-search\"]/div/div/div[2]/div[4]/div[2]/div/button[2]";
        String btnPrevPath="//*[@id=\"fulltext-search\"]/div/div/div[2]/div[4]/div[2]/div/button[1]";

        WebElement btnNextEle = driver.findElement(By.xpath(btnNextPath));
        while(btnNextEle.isEnabled()){
            logger.info("the nextFlag is enable");
//                遍历每一页，下载相关文件
            download(driver);
            btnNextEle.click();
            try {
//                sleep 5 秒
                sleep(5*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        driver.quit();

    }

    private void download(WebDriver driver) {
        String tablePath = "//*[@id=\"fulltext-search\"]/div/div/div[2]/div[4]/div[1]/div/div[3]/table";
        WebElement tableEle = driver.findElement(By.xpath(tablePath));
        List<WebElement> trs = tableEle.findElements(By.tagName("tr"));
        for (WebElement element:trs
             ) {
            List<WebElement> tds = element.findElements(By.tagName("td"));

            logger.info("the company code is "+tds.get(0).getText());
            logger.info("the file name is "+tds.get(1).getText());
            logger.info("the file date is "+tds.get(2).getText());

            CNInfoAchievement cnInfoAchievement=new CNInfoAchievement();
            cnInfoAchievement.setCompanyCode(tds.get(0).getText());
            cnInfoAchievement.setFilename(tds.get(1).getText());
            cnInfoAchievement.setDate(tds.get(2).getText());


//            在新页面获取文件的URL
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);",element);
            element.click();

            //获取当前页面句柄
            String handle = driver.getWindowHandle();
            //获取所有句柄，循环判断是否等于当前句柄
            for (String handles:driver.getWindowHandles()) {
                if (handles.equals(handle))
                    continue;
//                跳转到新Tab页
                driver.switchTo().window(handles);
                try {
                    sleep(2*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String URLPath="/html/body/div[1]/div[4]/div/div/a";
                WebElement URLEle = driver.findElement(By.xpath(URLPath));
                String fileURL=URLEle.getAttribute("href");
                logger.info("the fileURL is "+fileURL);
                cnInfoAchievement.setFileURL(fileURL);
                try{
                    webSiteRepository.save(cnInfoAchievement);
                }catch (Exception exception){
                    exception.printStackTrace();
                }

//                关闭新页签
                driver.close();

            }
            //                跳转回原始界面
            driver.switchTo().window(handle);

        }


    }

//    @RequestMapping(value = "showCNInfoAchievements",method = RequestMethod.GET)
//    public ModelAndView showCNInfoAchievements(){
//
//    }

}
