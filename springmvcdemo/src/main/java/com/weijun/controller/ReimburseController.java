package com.weijun.controller;

import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONArray;
import com.alibaba.rocketmq.shade.com.alibaba.fastjson.JSONObject;
import com.weijun.entity.Lawyer;
import com.weijun.entity.Reimburse;
import com.weijun.service.ReimburseService;
import com.weijun.service.impl.AccountService;
import com.weijun.service.impl.LawyerServiceImpl;
import com.weijun.util.DateTool;
import com.weijun.util.ResponseTool;
import com.weijun.util.SecurityTool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class ReimburseController {
    @Autowired
    DateTool dateTool;

    @Autowired
    SecurityTool securityTool;

    @Autowired
    AccountService accountService;

    @Autowired
    ReimburseService reimburseService;

    @Autowired
    ResponseTool responseTool;

    @Autowired
    private LawyerServiceImpl lawyerService;

    private static final Logger logger = LogManager.getLogger(ReimburseController.class.getName());

    @RequestMapping(value = "reimburse",method = RequestMethod.GET)
    public ModelAndView reimburse(){
        ModelAndView modelAndView=new ModelAndView("reimburse/reimburse");
        return modelAndView;
    }

    @RequestMapping(value = "reimburse",method = RequestMethod.POST)
    public ModelAndView reimburse_post(HttpServletRequest request,HttpServletResponse response){
        Reimburse reimburse=new Reimburse();

        String generateDateStr=request.getParameter("generateDate");
        Date generateDate=dateTool.StrToDate(generateDateStr);
        reimburse.setGenerateDate(generateDate);
        //申请报销的时间
        reimburse.setApplyDate(new Date());

        String category =request.getParameter("category");
        reimburse.setCategory(category);

        String detail=request.getParameter("detail");
        reimburse.setDetail(detail);

        String amount=request.getParameter("amount");
        reimburse.setAmount(Long.parseLong(amount));

        String caseNum =request.getParameter("caseNum");
        reimburse.setCaseNum(caseNum);

        String caseName =request.getParameter("caseName");
        reimburse.setCaseName(caseName);

        String undertaker=request.getParameter("undertaker");
        reimburse.setUndertaker(undertaker);

        //申请人
        String currentUserName=securityTool.getCurrentUser();
        Lawyer lawyer=accountService.getUserByUserName(currentUserName).getLawyer();
        reimburse.setApplicantName(lawyer.getLawyername());

        reimburse.setState("submitted");

        reimburseService.saveReimburse(reimburse);

        ModelAndView modelAndView=new ModelAndView("reimburse/reimburse");
        modelAndView.addObject("msg","操作成功，如需报销，请继续！");
        return modelAndView;
    }

    @RequestMapping(value = "/myReimburse",method = RequestMethod.GET)
    public ModelAndView myReimburse(){
        ModelAndView modelAndView=new ModelAndView("reimburse/myReimburse");
        return modelAndView;
    }

    @RequestMapping(value = "/approveReimburse",method = RequestMethod.GET)
    public ModelAndView allReimburse(){
        ModelAndView modelAndView=new ModelAndView("reimburse/approveReimburse");
        return modelAndView;
    }

    @RequestMapping(value = "/queryReimburse",method = RequestMethod.GET)
    public ModelAndView exportReimburse(){
        ModelAndView modelAndView=new ModelAndView("reimburse/queryReimburse");
        List<Lawyer> teamAlllawyers=lawyerService.getAllLawyerList();
        modelAndView.addObject("Alllawyers",teamAlllawyers);
        return modelAndView;
    }

    @RequestMapping(value = "/queryReimburseByConstraint",method = RequestMethod.GET)
    public void queryReimburse(HttpServletRequest request,HttpServletResponse response){

        String beginDateStr=request.getParameter("begindate");
        Date beginDate= dateTool.StrToDate(beginDateStr);
        String endDateStr=request.getParameter("enddate");
        Date endDate= dateTool.StrToDate(endDateStr);

        String smallDate=beginDateStr;
        String bigDate=endDateStr;

        if(beginDate.after(endDate)){
            smallDate=endDateStr;
            bigDate=beginDateStr;
        }

        String[] selectedLawyerNames=request.getParameterValues("selectedlawyers");
        List<String> selectedLawyerNameList=Arrays.asList(selectedLawyerNames);

        String[] reimburseState=request.getParameterValues("reimburseState");
        List<String> reimburseStateList=Arrays.asList(reimburseState);
        List<Reimburse> reimburseList=reimburseService.queryReimburses(selectedLawyerNameList,reimburseStateList,smallDate,bigDate);
        JSONArray retJson =paddingReimburseListToJsonObject(reimburseList);

        responseTool.writeJSONtoClient(response,retJson);

    }



    @RequestMapping(value = "getMyReimburseData",method = RequestMethod.GET)
    public void getMyReimburseData(HttpServletResponse response){
        //当前用户对应的律师
        String currentUserName=securityTool.getCurrentUser();
        String currentLawyerName=accountService.getUserByUserName(currentUserName).getLawyer().getLawyername();

        List<Reimburse> reimburseList=reimburseService.getMyReimburseData(currentLawyerName);
        JSONArray retJson =paddingReimburseListToJsonObject(reimburseList);

        responseTool.writeJSONtoClient(response,retJson);
    }

    @RequestMapping(value = "getALLReimburseData",method = RequestMethod.GET)
    public void getALLReimburseData(HttpServletResponse response){
        //当前用户对应的律师
        List<Reimburse> reimburseList=reimburseService.getALLSubmittedReimburseData();
        JSONArray retJson =paddingReimburseListToJsonObject(reimburseList);

        responseTool.writeJSONtoClient(response,retJson);
    }

    @RequestMapping(value = {"/delete-{reimburseID}-reimburse"}, method = RequestMethod.GET)
    public void deleteReimburse(@PathVariable String reimburseID, HttpServletResponse response) {
        reimburseService.deleteReimburseByid(Long.parseLong(reimburseID));
        responseTool.writeStringtoClient(response, "删除报销项成功！");
    }

    @RequestMapping(value = {"/approve-{reimburseID}-reimburse"}, method = RequestMethod.GET)
    public void approveReimburse(@PathVariable String reimburseID, HttpServletResponse response) {
        reimburseService.approveReimburseByid(Long.parseLong(reimburseID));
        responseTool.writeStringtoClient(response, "审批报销成功！");
    }

        private JSONArray paddingReimburseListToJsonObject(List<Reimburse> reimburseList) {
        List list=new ArrayList();
        JSONArray retJson = new JSONArray();
        String groupLawyerName="";
        long groupAmount=0;
        long totalAmount=0;
        for (Reimburse reimburse:reimburseList) {
            String currentLawyerName=reimburse.getApplicantName();
            long currentAmount=reimburse.getAmount();
            totalAmount+=currentAmount;
            if ((groupLawyerName.equals(currentLawyerName))||("".equals(groupLawyerName))){
                groupLawyerName=currentLawyerName;
                paddingReimburse(retJson, reimburse);
                groupAmount+=reimburse.getAmount();
            }else {
                //插入前一组小结
                paddingGroupInfo(retJson,groupLawyerName,groupAmount);
                groupLawyerName=reimburse.getApplicantName();
                groupAmount=0;

                paddingReimburse(retJson, reimburse);
                groupAmount+=reimburse.getAmount();
            }

        }
            //插入最后一组小结
            paddingGroupInfo(retJson,groupLawyerName,groupAmount);
            groupLawyerName="";
            groupAmount=0;
            //插入总计
            paddingTotalInfo(retJson,totalAmount);
        return retJson;
    }

    private void paddingTotalInfo(JSONArray retJson, long totalAmount) {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("detail","本次报销费用总计：");
        jsonObject.put("amount",totalAmount);

        retJson.add(jsonObject);
    }

    private void paddingReimburse(JSONArray retJson, Reimburse reimburse) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("reimburseID",reimburse.getId());
        jsonObject.put("generateDate",reimburse.getGenerateDate());
        jsonObject.put("category",reimburse.getCategory());
        jsonObject.put("detail",reimburse.getDetail());
        jsonObject.put("amount",reimburse.getAmount());
        jsonObject.put("caseNum",reimburse.getCaseNum());
        jsonObject.put("caseName",reimburse.getCaseName());
        jsonObject.put("undertaker",reimburse.getUndertaker());
        jsonObject.put("status",reimburse.getState());
        jsonObject.put("applyDate",reimburse.getApplyDate());
        jsonObject.put("applicantName",reimburse.getApplicantName());
        retJson.add(jsonObject);

    }


    private void paddingGroupInfo(JSONArray retJson, String groupLawyerName,long groupAmount) {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("detail",groupLawyerName+"费用小计：");
        jsonObject.put("amount",groupAmount);

        retJson.add(jsonObject);
    }

}
