
$(document).ready(function() {
    //文件上传组件加载
    initfileinput();

    //js验证输入
    bootStrapValidate();


});

function initfileinput() {
    //加载文件上传组件
    $('.uploadfile').fileinput({
        // uploadUrl:'/uploadlawyerinfo',
        showCaption: true,
        showCancel: false,
        showUpload: false, //是否显示上传按钮
        showUploadedThumbs: false,
        showClose: false,
        autoReplace: true,
        maxFileCount: 1,
        overwriteInitial: true,
        dropZoneEnabled: false,//是否显示拖拽区域
        showPreview: true, //是否显示预览
        language: 'zh',
        // initialPreview:"<img src='/back_t.jpg' class='file-preview-image' />",
        initialPreviewCount: 1,
        initialPreviewShowDelete: false
    });
// 文件上传组建初始化完毕
}

function bootStrapValidate() {
    // js验证输入
    $('#newCustomForm').bootstrapValidator({
        // message: 'This value is not valid',
        // feedbackIcons: {
        //     valid: 'glyphicon glyphicon-ok',
        //     invalid: 'glyphicon glyphicon-remove',
        //     validating: 'glyphicon glyphicon-refresh'
        // },
        fields: {
            customName: {
                /*键名和input name值对应*/
                message: 'custom name can not be empty',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'custom name can not be empty'
                    }
                }
            },
            customBrief: {
                message: 'The customBrief is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'custom brief can not be empty'
                    }
                }
            },
            customPhone: {
                message: 'The telephone is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'telephone can not be empty'
                    },
                    stringLength: {
                        min: 11,
                        max: 11,
                        message: 'telephone number length must be 11 characters.'
                    },
                    regexp: {
                        regexp: /^1[0-9]{10}$/,
                        message: 'please input right telephone number.'
                    }
                }
            },
            customEmail: {
                message: 'The email is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'email  can not be empty'
                    },
                    emailAddress: {
                        message: '邮箱地址格式有误'
                    }
                }
            },
            customAddress: {
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'customname can not be empty'
                    }
                }
            },
            customGender: {
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'customname can not be empty'
                    }
                }
            },
            customImg: {
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'customname can not be empty'
                    }
                }
            }
        }
    });
}