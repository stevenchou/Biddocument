﻿$(document).ready(function () {
    sessionStorage.workProjectName=$("#projectName").text();
    console.log("sessionStorage.workProjectName is "+sessionStorage.workProjectName);

    sessionStorage.fileNames="";

    $(".thumbnail").click(function () {
        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        var childNodes = zTree.transformToArray(zTree.getNodes());
        var fileNames="";
        for ( var j = 0; j < childNodes.length; j++) {
            console.log(childNodes[j].path);
            if (childNodes[j].checked&&(!childNodes[j].isParent)) {
                fileNames+=getProjectRelativePath(childNodes[j].path);
                fileNames+=",";

            }
        }
        //去掉最后一个逗号
        var fileNamesSub=fileNames.substring(0,fileNames.length-1);
        sessionStorage.fileNames=fileNamesSub;

    })

    $(".getemail").click(function () {
        // 获取email地址
        var email=$(this).attr('title');
        var name=$(this).attr('alt');
        sessionStorage.email=email;
        sessionStorage.name=name;

    })
})