function initfileinput() {
    //加载截取合同文件上传组件，仅限于常用图片文件
    $('.uploadfile').fileinput({
        // uploadUrl:'/uploadlawyerinfo',
        showCaption: true,
        showCancel: false,
        showUpload: false, //是否显示上传按钮
        showUploadedThumbs: false,
        showClose: false,
        autoReplace: true,
        maxFileCount: 1,
        overwriteInitial: true,
        dropZoneEnabled: false,//是否显示拖拽区域
        showPreview: true, //是否显示预览
        language: 'zh',
        // initialPreview:"<img src='/back_t.jpg' class='file-preview-image' />",
        initialPreviewCount: 1,
        initialPreviewShowDelete: false
    });
// 文件上传组建初始化完毕
}
function inputgroupadd(obj,inputgroupname){
    html = '<div class="input-group saltIp col-md-12">'+
        '<input type="file" class="file-loading uploadfile form-control" id="saltIp" name="'+inputgroupname+'" >'+
        '<span class="input-group-btn">'+
        '<button class="btn btn-info" type="button" data-toggle="tooltip" title="delete" id="delInputGrpItem"><span class="glyphicon glyphicon-minus"></span></button>'+
        '</span>'+
        '</div>'
    obj.insertAdjacentHTML('beforebegin',html);
    initfileinput();
}

$(document).ready(function() {
    // 加载日期选择插件
    $('.form_date').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    initfileinput();



    $("#allprojecttype option[value='0']").attr("selected","selected");
    // 缺一不可
    $('#allprojecttype').selectpicker('refresh');
    $('#allprojecttype').selectpicker('render');

    // add a new projecttype
    $("#newprojecttype").click(function() {
        console.log("add a new projecttype,the name is " + $("#newprojecttype").val());

        // 利用ajax动态创建project type
        $.ajax({
            type: "POST",
            url: "newprojecttype",
            data: {projettypename: $("#projettypename").val(), projecttypebrief: $("#projecttypebrief").val()},
            // dataType: "json",
            success: function (data) {
                $("#allprojecttype").append("<option>" + data + "</option>");
                $('#allprojecttype').selectpicker('val',data);
                // 缺一不可
                $('#allprojecttype').selectpicker('refresh');
                $('#allprojecttype').selectpicker('render');

                alert("add new projecttype successfully! [" + data + "]");
                console.log("add new projecttype successfully! [" + data + "]");

            },
            error: function (errdata) {
                alert("create projecttype failed!");
                console.log("create projecttype failed!"+errdata);
            }
        });
    });
    // finished add new projecttype


    // js验证输入
    $('#newprojectform').bootstrapValidator({
        // message: 'This value is not valid',
        // feedbackIcons: {
        //     valid: 'glyphicon glyphicon-ok',
        //     invalid: 'glyphicon glyphicon-remove',
        //     validating: 'glyphicon glyphicon-refresh'
        // },
        fields: {
            projectname: {
                /*键名username和input name值对应*/
                message: 'project name can not be empty',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'project name can not be empty'
                    }
                }
            },
            projectbrief: {
                message: 'The projectbrief is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'project brief can not be empty'
                    }
                }
            },
            customname: {
                message: 'The customname is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'customname can not be empty'
                    }
                }
            },
            allcustom: {
                message: 'The allcustom is not valid',
                validators: {
                    notEmpty:{
                        message:'allcustom can not be empty.'
                    }
                }
            },
            customname: {
                message: 'The customname is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'customname can not be empty'
                    }
                }
            },
            custombrief: {
                message: 'The custom brief is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'custom brief can not be empty'
                    }
                }
            },
            signaturelawyer:{
                message: 'The signaturelawyer is not valid',
                validators: {
                    choice: {
                        min: 1,
                        max: 1,
                        message: 'Please choose 1 signaturelawyer.'
                    }
                }
            },
            responsiblelawyers:{
                message: 'The responsiblelawyer is not valid',
                validators: {
                    choice: {
                        min: 1,
                        max: 1024,
                        message: 'Please choose at lest 1 responsiblelawyer.'
                    }
                }
            },
            teamAlllawyers:{
                message: 'The teamAlllawyer is not valid',
                validators: {
                    choice: {
                        min: 2,
                        max: 1024,
                        message: 'Please choose at lest 2 team number.'
                    }
                }
            },
            signaturedate: {
                message: 'The signaturedate is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'signaturedate can not be empty'
                    }
                }
            },
            terminationdate: {
                message: 'The terminationdate is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'terminationdate can not be empty'
                    }
                }
            },
            contractfile: {
                message: 'The contractfile is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'contractfile can not be empty'
                    }
                }
            },
            allprojecttype: {
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'customname can not be empty'
                    }
                }
            }
        }
    });

    // input group中，点击删除按钮，删除一个input输入框
    $(document).on('click','#delInputGrpItem',function(){
        var el = this.parentNode.parentNode
        var saltIp = $(this).parent().parent().find('#saltIp').val()
        if (saltIp == ""){
            el.parentNode.removeChild(el)
            return
        }
        alertify.confirm('您确定要删除选中的命令？',
            function(e){
                if(e){
                    $.ajax({
                        'url':'/url',
                        'type':'POST',
                        'async':false,
                        'dataType':'json',
                        'data':{'type':'delSaltIp','projectId':projectId,'saltIp':saltIp},
                        'success':function(result){
                            if (result.code){
                                el.parentNode.removeChild(el)
                            }else {
                                showError(result.msg)
                            }
                        }
                    })

                }
            })

    });
    // end function


    // 解决bootstrapValidator与datetimepicker混合使用时日期验证不刷新的问题
    $('#signaturedate').change(function() {
        $('#newprojectform').data('bootstrapValidator').updateStatus('signaturedate',
            'NOT_VALIDATED',null).validateField('signaturedate');
    });
    $('#terminationdate').change(function() {
        $('#newprojectform').data('bootstrapValidator').updateStatus('terminationdate',
            'NOT_VALIDATED',null).validateField('terminationdate');
    });

});