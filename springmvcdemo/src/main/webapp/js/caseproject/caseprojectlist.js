﻿$(document).ready(function () {
    console.log("initialize the projects table.")
    $('#projectlist').bootstrapTable({
        url: "/getprojectdata",
        method: 'get',
        cache: false,
        height: 680,
        striped: true,
        pagination: true,
        pageSize: 2,
        pageNumber: 1,
        pageList: [2, 5, 10, 20], sidePagination: 'client',
        showColumns: true,
        showRefresh: false,
        showExport: true,
        exportTypes: ['csv', 'txt', 'xml'],
        search: false,
        clickToSelect: true,
        columns:
            [
                {field: "checked", checkbox: true},
                // {field:"name",title:"测试姓名",align:"center",valign:"middle",sortable:"true"},
                // {field:"age",title:"年龄",align:"center",valign:"middle",sortable:"true"},
                // {field:"sex",title:"性别",align:"center",valign:"middle",sortable:"true"},
                {field: "caseprojectid", title: "caseprojectid", visible: false},
                {field: "casename", title: "项目名称", align: "center", valign: "middle", sortable: "true"},
                {field: "casebrief", title: "项目简介", align: "center", valign: "middle", sortable: "true"},
                {field: "custom", title: "客户", align: "center", valign: "middle", sortable: "true"},
                {field: "signaturedate", title: "签约日期", align: "center", valign: "middle", sortable: "true"},
                {field: "terminationdate", title: "届满日期", align: "center", valign: "middle"},
                {field: "signatureLawyer", title: "签约律师", align: "center", valign: "middle"},
                {field: "responsibleLawyers", title: "负责律师", align: "center", valign: "middle"},
                {field: "teamnumbers", title: "服务团队", align: "center", valign: "middle"},
                {field: "evidencefilepath", title: "合同路径", align: "center", valign: "middle"},
                {
                    title: "操作",
                    align: 'center',
                    valign: 'middle',
                    width: 200, // 定义列的宽度，单位为像素px
                    events: operateEvents,
                    formatter: operateFormatter(),
                }

            ],

    });

    function operateFormatter(value, row, index) {
        console.log("enter the operation!");
        return [
            // '<button type="button" class="RoleOfDel btn btn-info  " >删除</button>',
            '<button type="button" class="RoleOfEdit btn btn-info  " >编辑</button>'
        ].join('');

    }


});

window.operateEvents = {
    'click .RoleOfDel': function (e, value, row, index) {
        var url = "/delete-" + row.caseprojectid + "-project";
        console.log("delete the row,id is " + row.caseprojectid + ",the request url is " + url);
        //delete the project by id
        $.ajax({
            type: "POST",
            url: url,

            success: function (data) {
                console.log("delete the project,the return msg from server is " + data);
                $("#projectlist").bootstrapTable('refresh');
            },
            error: function (errdata) {
                alert("delete lawyer failed!");
                console.log("delete lawyer failed!" + errdata);
            }
        });
    },
    'click .RoleOfEdit': function (e, value, row, index) {
        console.log("edit the row,id is " + row.DB_id)
    }
}

$(window).resize(function () {
    $('#projectlist').bootstrapTable('resetView');
});



