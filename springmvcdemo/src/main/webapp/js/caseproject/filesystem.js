var setting = {
    view: {
        // addHoverDom: addHoverDom,
        // removeHoverDom: removeHoverDom,
        addDiyDom: addDiyDom,
        selectedMulti: false,
        dblClickExpand: false
    },
    check: {
        enable: true
    },
    callback: {
        beforeRemove: beforeRemove,
        onRemove: onRemove,
        beforeRename: beforeRename,
    },
    edit: {
        enable: true,
        showRemoveBtn: showRemoveBtn,
        showRenameBtn: showRenameBtn
    }
};
var className = "dark";
var uploadPath;

var addCount = 1;

function removeTreeNode() {
    console.log("remove the node")
    var nodes = zTree.getSelectedNodes();
    if (nodes && nodes.length>0) {
        if (nodes[0].children && nodes[0].children.length > 0) {
            var msg = "要删除的节点是父节点，如果删除将连同子节点一起删掉。\n\n请确认！";
            if (confirm(msg)==true){
                zTree.removeNode(nodes[0]);
            }
        } else {
            zTree.removeNode(nodes[0]);
        }
    }
}


// function addHoverDom(treeId, treeNode) {
//     var sObj = $("#" + treeNode.tId + "_span");
//     if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
//     var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
//         + "' title='add folder' onfocus='this.blur();'></span>";
//
//     var btn = $("#addBtn_"+treeNode.tId);
//     if(treeNode.isParent){
//         sObj.after(addStr);
//     }
//
//     if (btn) btn.bind("click", function(){
//
//
//     });
// };
function addFolderInServer(parentPath,folderName) {
    console.log("the parent path is "+parentPath+",the new folder name is "+parentPath+"/"+folderName);
    $.ajax({
        type:"GET",
        url:"createProjecfolder",
        // dataType:"json",
        //保证前台传的数组后台可以拿到
        traditional: true,
        contentType:"application/json",
        data:{foldername: parentPath+"/"+folderName},
        success:function(msg){
            // 返回值里包含successfully字样时，记录日志
            if(msg.indexOf("successfully") >= 0){
                console.log(msg);
                return true;
            }else{
                // 刷新ztree，正常创建成功不用刷新ztree
                var zTree = $.fn.zTree.getZTreeObj("treeDemo");
                zTree.reAsyncChildNodes(null, "refresh");
            }
        },
        error:function(errormsg){
            console.log(errormsg);
        }
    });
}
function beforeRemove(treeId, treeNode) {
    className = (className === "dark" ? "":"dark");
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.selectNode(treeNode);
    return confirm("删除操作将删除服务器上的文件，请格外小心，确认删除文件 -- " + treeNode.name + " 吗？");
}
function onRemove(e, treeId, treeNode) {
    //path 是自定义字段
    var nodepath=treeNode.path;
    console.log("remove the node，node path is "+nodepath);
    $.ajax({
        type:"GET",
        url:"deleteProjectfile",
        // dataType:"json",
        //保证前台传的数组后台可以拿到
        traditional: true,
        contentType:"application/json",
        data:{filename: nodepath},
        success:function(msg){
            // 返回值里包含successfully字样时，记录日志
            if(msg.indexOf("successfully") >= 0){
                console.log(msg);

            }
        },
        error:function(errormsg){
            console.log(errormsg);
        }
    });

    // 删除成功后，重新加载ztree
    initZtree();

}
function removeHoverDom(treeId, treeNode) {
    $("#addBtn_"+treeNode.tId).unbind().remove();
}

function beforeRename(treeId, treeNode, newName) {
    var oldName=treeNode.name;
    console.log("the oldname is "+treeNode.name+",the new name is "+newName);
    //文件需要验证后缀名
    if(!treeNode.isParent){
        var oldSuffix=getPostSuffix(treeNode.name);
        var newSuffix=getPostSuffix(newName);
        console.log("the old suffix is "+oldSuffix+",the new suffix is "+newSuffix);
        if(oldSuffix!=newSuffix){
            alert("文件后缀名不能改变，原后缀是"+oldSuffix+",请改回！");
            return false;
        }
    }
    //修改文件名
    var parentPath=treeNode.getParentNode().path;
    modifyFileName(parentPath+"/"+treeNode.name,parentPath+"/"+newName);

}

function modifyFileName(oldName,newName) {
    //将修改文件名的命令发送到后台服务器
    console.log("the old file name is "+oldName);
    console.log("the new file name is "+newName);

    $.ajax({
        type:"GET",
        url:"modifyProjectfileName",
        // dataType:"json",
        //保证前台传的数组后台可以拿到
        traditional: true,
        contentType:"application/json",
        data:{oldName: oldName,newName:newName},
        success:function(msg){
            // 返回值里包含successfully字样时，记录日志
            if(msg.indexOf("successfully") >= 0){
                console.log(msg);
            }else{
                // 刷新ztree，正常删除成功不用刷新ztree
                var zTree = $.fn.zTree.getZTreeObj("treeDemo");
                zTree.reAsyncChildNodes(null, "refresh");
            }
        },
        error:function(errormsg){
            console.log(errormsg);
        }
    });
}
function showRemoveBtn(treeId, treeNode) {
    return true;
}
function showRenameBtn(treeId, treeNode) {
    return true;
}

function addfile() {
    console.log("add template file")
}

function addDiyDom(treeId, treeNode) {
    var aObj = $("#" + treeNode.tId + "_a");
    //add file
    if ($("#docBtn_"+treeNode.id).length>0) return;
    var addStr = "<a id=\"modal-720051\" href=\"#modal-container-720051\" role=\"button\" class=\"btn\" data-toggle=\"modal\"><img class='addFile diyImg' src='/image/upload.png' id='docBtn_" + treeNode.tId + "' title='add file'/></a>";

    //add folder
    if ($("#folderBtn_"+treeNode.id).length>0) return;
    var folderStr = "<img class='addFile diyImg' src='/image/folder_yellow.jpg' id='folderBtn_" + treeNode.tId + "' title='add folder'/>";

    //open the file
    if ($("#openFileBtn_"+treeNode.id).length>0) return;
    var openFileStr = "<img class='diyImg' src='/image/fileopen.jpg' id='openFileBtn_" + treeNode.tId + "' title='open file'/>";
    // //modify the file
    // if ($("#renameBtn_"+treeNode.id).length>0) return;
    // var renameStr = "<img class='addFile diyImg' src='/image/modify.png' id='renameBtn_" + treeNode.tId + "' title='rename'/>";
    //
    // //delete the file
    // if ($("#deleteBtn_"+treeNode.id).length>0) return;
    // var deleteStr = "<img class='addFile diyImg' src='/image/delete.png' id='deleteBtn_" + treeNode.tId + "' title='delete'/>";
    // aObj.after(deleteStr);
    // aObj.after(renameStr);
    if(treeNode.isParent){
        aObj.after(addStr);
        aObj.after(folderStr);
    }else {
        aObj.after(openFileStr);
    }


    //绑定响应函数
    var docbtn = $("#docBtn_"+treeNode.tId);
    if (docbtn) docbtn.bind("click",function(){
        creatFile(treeId, treeNode);
    });

    var folderbtn = $("#folderBtn_"+treeNode.tId);
    if (folderbtn) folderbtn.bind("click", function(){
        creatFolder(treeId, treeNode);
    });

    var openFilebtn = $("#openFileBtn_"+treeNode.tId);
    if (openFilebtn) openFilebtn.bind("click", function(){
        //只有文件才显示打开图标，所以这里treeNode.path 实际获取的是文件的名称
        openFile(treeNode.path);
    });
}

function creatFolder(treeId, treeNode) {
    var numLength = 10;
    console.log("folderbtn clicked")
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    if(treeNode.isParent){
        console.log("add folder");
        var rannum=RndNum(numLength);
        addFolderInServer(treeNode.path,"folder"+rannum);
        // 更新ztree
        zTree.addNodes(treeNode, {id:rannum, pId:treeNode.id, isParent:true,name:"folder" +rannum});
        return false;
    }
    else{
        console.log("this is not folder,can not create the file here");
        alert("this is not folder,can not create the file here");
        return false;
    }
}

function creatFile(treeId, treeNode){
    console.log("docBtn click");
    console.log("the path name is "+treeNode.path);
    uploadPath = treeNode.path;
    console.log("the path name is "+uploadPath);
}

function openFile(filename) {
    var encodeURL=encodeURI("/openFile?filename="+filename);
    console.log("the encoded URL is "+encodeURL);
    window.location.href = encodeURL;

}
function initZtree() {
    console.log("the workProjectName is "+sessionStorage.workProjectName);
    var projectName=$("#projectName").text();
    if (""==projectName) {
        projectName=sessionStorage.workProjectName;
    }

    $.ajax({
        type: "GET",
        url: "getProjectFileTreeJSON",
        dataType: "json",
        //保证前台传的数组后台可以拿到
        traditional: true,
        contentType: "application/json",
        data: {projectname:projectName},
        // dataType: "json",
        success: function (retdata) {
            $.fn.zTree.init($("#treeDemo"), setting, retdata);
            zTree = $.fn.zTree.getZTreeObj("treeDemo");
            rMenu = $("#rMenu");
            console.log("get the project file tree successfully! [" + retdata + "]");

        },
        error: function (errdata) {
            alert("get the project file tree failed!");
            console.log("create projecttype failed!" + errdata);
        }
    });
}
var zTree, rMenu;
$(document).ready(function(){
    console.log("now,we get the tree，the project name is"+$("#projectName").text());

    initZtree();


    $("#uploadFileToServer").click(function () {
        console.log("uploadFileToServer is clicked");
        var fileObj = $("#uploadFileSelecter")[0].files[0]; // js 获取文件对象
        if (typeof (fileObj) == "undefined" || fileObj.size <= 0) {
            alert("请选择文件");
            return;
        }
        var formFile = new FormData();
        formFile.append("savePath", uploadPath);
        formFile.append("uploadfile", fileObj); //加入文件对象

        // ajax上传文件
        $.ajax({
            url: "/uploadFile",
            data: formFile,
            type: "POST",
            // dataType: "json",
            cache: false,//上传文件无需缓存
            processData: false,//用于对data参数进行序列化处理 这里必须false
            contentType: false, //必须
            success: function (successData) {
                console.log(successData);
                // 返回值里包含successfully字样时，记录日志
                if (successData.indexOf("successfully") >= 0) {
                    alert("upload the file successfully!");
                    // 刷新ztree，正常删除成功不用刷新ztree
                    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
                    zTree.reAsyncChildNodes(null, "refresh");
                }
                else{
                    alert("upload the file failed!");
                }
            }
        })
    });
});