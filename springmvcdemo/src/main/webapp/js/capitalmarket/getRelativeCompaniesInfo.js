﻿//企查查上获取的数据转换成Html
var resultHtml;
//所有
var investCompanies=new Array();

$(document).ready(function() {
    // 输入域验证
    inputDomainvalidator();

    //选择客户后，根据客户名称查询数据库中关联公司的名称，以供选择
    getRelativeCompaniesByCustomName();

    //点击全选按钮，根据全选按钮状态，决定是否全选关联公司
    selectAllCompaniesOrNot();

    //向企查查查询公司信息
    queryCompaniesInfoFromQCC();
});

function inputDomainvalidator() {

}

function selectAllCompaniesOrNot() {
    //点击全选按钮，根据全选按钮状态，决定是否全选关联公司
    $("#selectAll").click(function () {
        var status=$("#selectAll").is(':checked');
        var allCheckBox=$("input:checkbox[name='selectedCompanies']");
        if(status){
            //Action for checked
            console.log("select all lawyers");
            allCheckBox.each(function() { // 遍历name=selectedCompanies的多选框
                $(this).click();  // 每一个被选中项的值
            });
        } else {
            //Action for not checked
            console.log("deselect all lawyers");

            allCheckBox.each(function() { // 遍历name=selectedCompanies的多选框
                $(this).click();  // 每一个被选中项的值
            });
        }
    });
}
function getRelativeCompaniesByCustomName() {
    $("#customName").change(function () {
        var selectedCustomName=$("#customName").val();
        console.log("selected new custom name is "+selectedCustomName)
        // 不包含某个字符时，返回-1
        if (-1==selectedCustomName.indexOf("请选择一位客户")){
            console.log("now,use the ajaxSubmit to get the relative companies!");

            // 利用ajax动态获取关联公司
            $.ajax({
                type: "get",
                url: "getRelativeCompaniesByCustomName",
                data: {
                    customName: selectedCustomName
                    },
                dataType: "text",
                success: function (data) {
                    // var companyNum = data[0].companyNum;
                    // console.log("getRelativeCompaniesByCustomName successfully! the company number is [" + companyNum + "]");
                    $.each(JSON.parse(data), function(idx, obj) {
                        console.log(obj.companyName);
                        var html = '<div class="checkbox checkbox-success checkbox-inline">'+
                            '<input type="checkbox" class="styled" value="'+obj.companyName+'" name="selectedCompanies"/>'+
                            '<label>'+obj.companyName+'</label>'+
                            '</div>'+
                            '<br>'
                        $("#allCompanies").append(html);
                    });
                },
                error: function (errdata) {
                    alert("get Relative Companies By Custom Name failed!");
                    console.log("get Relative Companies By Custom Name"+errdata);
                }
            });
        }
    });
}

function queryCompanyInfoByCompanyName(companyName) {
    $.get("http://i.yjapi.com/ECIV4/GetDetailsByName",
        {
            key: "46e7fbcf3f7a41eba6357466493ebea3",
            searchKey: companyName
        }, function (data, status, xhr) {
            console.log(data);
            BasicInfoJsonToHtml(data);
        },
        "json");
    return false;
}

function queryCompaniesInfoFromQCC(){
    $("#queryBtn").click(function () {
        // 利用ajax向企查查动态获取关联公司的详细信息
        // $.get(URL,data,function(data,status,xhr),dataType)
        queryCompanyInfoByCompanyName("华为技术有限公司");
        searchInvestmentQCCAPI("华为技术有限公司");

        // 导出HTML
        $("#resultFromQCC").wordExport();
    });
}

function paddingStockHolder(Partners) {
//股东信息遍历
    resultHtml += '<b>股东信息:</b>' +
        '<br>';
    for (var i = 0; i < Partners.length; i++) {
        var partner = Partners[i];
        var StockType = partner.StockType;
        //自然人股东
        if (StockType.indexOf("自然人股东") != -1) {
            paddingPrivateStockHolder(partner);

        } else {
            paddingCompanyStockHolder(partner);
        }
    }
}

function paddingCompanyBasicInfo(company) {
    resultHtml = resultHtml+'<div >' +
        "公司名称：" + company.Name +
        '<br>' +
        "公司类型：" + company.EconKind +
        '<br>' +
        "注册地址：" + company.Address +
        '<br>' +
        "经营范围：" + company.Scope +
        '<br>' +
        "经营状态：" + company.Status +
        '<br>' +
        "法定代表人：" + company.OperName +
        '<br>' +
        "注册资本：" + company.RegistCapi +
        '</div>' +
        '<br>';
}

function BasicInfoJsonToHtml(data) {
    console.log(data.Result);
    var QCCResult=data.Result;
    var Partners=QCCResult.Partners;
    var companyName=QCCResult.Name;

    resultHtml='<b>公司基本信息：</b>'+
        '<br>';
    paddingCompanyBasicInfo(QCCResult);
    paddingStockHolder(Partners);


    $("#resultFromQCC").append(resultHtml);


}

function paddingPrivateStockHolder(partner) {
    resultHtml=resultHtml+'<b>股东名称</b>'+partner.StockName+
        '<br>'+
        '<b>持股比例</b>'+partner.StockPercent+
        '<br>';
}

function paddingCompanyStockHolder(partner) {
    resultHtml=resultHtml+'<b>股东名称</b>'+partner.StockName+
        '<br>'+
        '<b>持股比例</b>'+partner.StockPercent+
        '<br>';
    investCompanies.push(partner.StockName);
}

function searchInvestmentQCCAPI(companyName) {
    $.get("http://i.yjapi.com/ECIRelationV4/SearchInvestment",
        {
            key: "46e7fbcf3f7a41eba6357466493ebea3",
            searchKey: companyName
        }, function (data, status, xhr) {
            console.log(data);
            InvestmentJsonToHtml(data);
        },
        "json");
    return false;
}

function InvestmentJsonToHtml(data) {

    resultHtml='<b>投资公司信息：</b>'+
        '<br>';
    var QCCResult=data.Result;
    for (var i = 0; i < QCCResult.length; i++) {
        var investCompany = QCCResult[i];
        resultHtml=resultHtml+'<b>公司名称</b>'+investCompany.Name+
            '<br>'+
            '<b>统一社会信用代码</b>'+investCompany.CreditCode+
            '<br>'+
            '<b>注册资本</b>'+investCompany.RegistCapi+
            '<br>'+
            '<b>公司类型</b>'+investCompany.EconKind+
            '<br>'
        investCompany.push(investCompany.Name);
    }

    $("#resultFromQCC").append(resultHtml);
}

