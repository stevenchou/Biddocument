$(document).ready(function() {
    // 输入域验证
    inputDomainvalidator();
    
    // 提交输入信息到后台
    submitInput();
});

function inputDomainvalidator() {
// js验证输入
    $('#defaultForm').bootstrapValidator({
        // message: 'This value is not valid',
        // feedbackIcons: {
        //     valid: 'glyphicon glyphicon-ok',
        //     invalid: 'glyphicon glyphicon-remove',
        //     validating: 'glyphicon glyphicon-refresh'
        // },
        fields: {
            companyName: {
                /*键名username和input name值对应*/
                message: 'company name can not be empty',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'company name can not be empty'
                    }
                }
            },
            creditCode: {
                /*键名username和input name值对应*/
                message: 'credit code can not be empty',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'credit code can not be empty'
                    }
                }
            }
        }
    });
    //end bootstrapValidator
}

function submitInput() {
    $("#submitBtn").click(function () {
        $("#defaultForm").ajaxSubmit({
            forceSync: false,
            url: "personalNewRelativeCompany",
            type: "post",
            dataType: "text",
            success: function(response){
                console.log("success "+response);
                alert(response);
                $('#defaultForm')[0].reset()
            },
            error: function(response){
                console.log("error"+response);
            }

        });
        return false; // 阻止表单自动提交事件，必须返回false，否则表单会自己再做一次提交操作，并且页面跳转

    });
}