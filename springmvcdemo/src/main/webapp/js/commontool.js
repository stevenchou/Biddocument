function randomString(len) {
    len = len || 32;
    var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz';    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
    var maxPos = $chars.length;
    var pwd = '';
    for (i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}

//产生随机数函数
function RndNum(n){
    var rnd="";
    for(var i=0;i<n;i++)
        rnd+=Math.floor(Math.random()*10);
    return rnd;
}

function getPostSuffix(filename){
    var index1=filename.lastIndexOf(".");
    var index2=filename.length;
    var suffix=filename.substring(index1+1,index2);//后缀名
    return suffix;
}

function getProjectRelativePath(path) {
    var pos=path.indexOf("projects");
    //9是projects\的长度
    var subPath=path.substring(pos+9,path.length);
    return subPath;
}