﻿$(document).ready(function () {
    $('#table').bootstrapTable({
        url: "/getALLReimburseData",
        method: 'get',
        cache: false,
        // height: 680,
        striped: true,
        pagination: true,
        pageSize: 5,
        pageNumber: 1,
        pageList: [2, 5, 10, 20], sidePagination: 'client',
        showColumns: true,
        showRefresh: false,
        showExport: true,
        exportTypes: ['csv', 'txt', 'xml'],
        search: false,
        clickToSelect: true,
        columns:
            [
                // {field:"name",title:"测试姓名",align:"center",valign:"middle",sortable:"true"},
                // {field:"age",title:"年龄",align:"center",valign:"middle",sortable:"true"},
                // {field:"sex",title:"性别",align:"center",valign:"middle",sortable:"true"},
                {field: "reimburseID", title: "reimburseID", visible: false},
                {field: "generateDate", title: "费用产生时间", align: "center", valign: "middle", sortable: "true",
                    formatter:function(value,row,index){
                        var ret;
                        if(row.generateDate!=null){
                            var stringDate = row.generateDate;
                            ret=new Date(stringDate)
                        }
                        return ret.toISOString().substring(0,10);
                    }},
                {field: "category", title: "报销类别", align: "center", valign: "middle", sortable: "true"},
                {field: "detail", title: "报销明细", align: "center", valign: "middle", sortable: "true"},
                {field: "amount", title: "报销金额", align: "center", valign: "middle", sortable: "true"},
                {field: "caseNum", title: "案号", align: "center", valign: "middle", sortable: "true"},
                {field: "caseName", title: "案件名称", align: "center", valign: "middle", sortable: "true"},
                {field: "undertaker", title: "承担人", align: "center", valign: "middle", sortable: "true"},
                {field: "applyDate", title: "申请时间", align: "center", valign: "middle", sortable: "true",
                    formatter:function(value,row,index){
                        var ret;
                        if(row.applyDate!=null){
                            var stringDate = row.applyDate;
                            ret=new Date(stringDate)
                        }
                        return ret.toISOString().substring(0,10);
                    }
                },
                {
                    title: "操作",
                    align: 'center',
                    valign: 'middle',
                    width: 200, // 定义列的宽度，单位为像素px
                    events: operateEvents,
                    formatter: operateFormatter(),
                }

            ],

    });

    function operateFormatter(value, row, index) {
        console.log("enter the operation!");
        return [
            '<button type="button" class="RoleOfAgree btn btn-info  " >审批</button>'
        ].join('');

    }


});

window.operateEvents = {
    'click .RoleOfAgree': function (e, value, row, index) {
        var url = "/approve-" + row.reimburseID + "-reimburse";
        console.log("agree the row,id is " + row.reimburseID + ",the request url is " + url);
        //delete the lawyer by id
        $.ajax({
            type: "GET",
            url: url,
            //不需要传递参数
            success: function (data) {
                console.log("delete the reimburse,the return msg from server is " + data);
                $("#table").bootstrapTable('refresh');
            },
            error: function (errdata) {
                alert("delete lawyer failed!");
                console.log("delete lawyer failed!" + errdata);
            }
        });
    }
}



