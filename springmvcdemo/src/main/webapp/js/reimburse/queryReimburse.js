﻿
$(document).ready(function() {
    // 加载日期选择插件
    $('.form_date').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 3,
        minView: 2,
        forceParse: 0
    });


    //点击全选按钮，根据全选按钮状态，决定是否全选律师
    $("#selectAll").click(function () {
        var status=$("#selectAll").is(':checked');
        var allCheckBox=$("input:checkbox[name='selectedlawyers']");
        if(status){
            //Action for checked
            console.log("select all lawyers");
            allCheckBox.each(function() { // 遍历name=selectedlawyers的多选框
                $(this).click();  // 每一个被选中项的值

            });
        } else {
            //Action for not checked
            console.log("deselect all lawyers");

            allCheckBox.each(function() { // 遍历name=selectedlawyers的多选框
                $(this).click();  // 每一个被选中项的值
            });
        }
    });

    queryReimburse()

});

function queryReimburse() {
    // 点击查询按钮
    $("#queryReimburse").click(function () {
        //用ajax获取数据
        var lawyerName=[];
        var selectedlawyers=$("input:checkbox[name='selectedlawyers']:checked");
        selectedlawyers.each(function() { // 遍历name=selectedlawyers的多选框
            lawyerName.push($(this).val());
        });
        var ststus=$("input:checkbox[name='reimburseState']:checked");
        var reimburseState=[];
        ststus.each(function() { // 遍历name=selectedlawyers的多选框
            reimburseState.push($(this).val());
        });
        $.ajax({
            type:"get",
            url:"/queryReimburseByConstraint",
            dataType: "json",
            //保证前台传的数组后台可以拿到
            traditional: true,
            contentType:"application/json",
            data:{
                begindate:$("#begindate").val(),
                enddate:$("#enddate").val(),
                selectedlawyers:lawyerName,
                reimburseState:reimburseState
            },
            success:function (successdata) {
                console.log("query the reimburse successfully,the selected projects data is "+successdata);
                $("#queryConstraint").hide();
                // 用bootstrap table填充
                var date=new Date();
                var DateStr=date.toLocaleString();
                $('#table').bootstrapTable({
                    method: 'get',
                    toolbar: '#toolbar',                //工具按钮用哪个容器
                    cache: false,
                    height: 500,
                    striped: true,
                    pagination: true,
                    pageSize: 5,
                    pageNumber: 1,
                    pageList: [ 5, 10, 30,50,100],
                    sidePagination: 'client',
                    showColumns: true,
                    // showRefresh: true,
                    showExport: true,                     //是否显示导出
                    exportDataType: "all",              //basic', 'all', 'selected'.
                    buttonsAlign:"right",  //按钮位置
                    exportTypes:['excel'],  //导出文件类型
                    Icons:'glyphicon-export',
                    exportOptions:{
                        ignoreColumn: [10],
                        fileName: '报销表'+DateStr,  //文件名称设置
                        worksheetName: 'sheet1',  //表格工作区名称
                        tableName: '报销表'+DateStr,
                        excelstyles: ['background-color', 'color', 'font-size', 'font-weight']

                    },
                    // search: true,
                    clickToSelect: true,
                    data:successdata,
                    rowStyle:function(row,index){
                        var content=row.detail;
                        if (content.indexOf("费用小计") != -1||content.indexOf("费用总计") != -1) {
                            return {css:{"background-color":"lightblue"}}
                        }else{
                            return {css:{"background-color":""}}
                        }
                    },
                    columns:
                        [
                            {field: "reimburseID", title: "reimburseID", visible: false},
                            {field: "generateDate", title: "费用产生时间", align: "center", valign: "middle", sortable: "true",
                                formatter:function(value,row,index){
                                    var ret;
                                    if(row.generateDate!=null&&row.generateDate!=0){
                                        var stringDate = row.generateDate;
                                        ret=new Date(stringDate)
                                        var retDateStr=ret.toLocaleDateString();
                                        return retDateStr;
                                    }

                                }},
                            {field: "category", title: "报销类别", align: "center", valign: "middle", sortable: "true"},
                            {field: "detail", title: "报销明细", align: "center", valign: "middle", sortable: "true"},
                            {field: "amount", title: "报销金额", align: "center", valign: "middle", sortable: "true"},
                            {field: "caseNum", title: "案号", align: "center", valign: "middle", sortable: "true"},
                            {field: "caseName", title: "案件名称", align: "center", valign: "middle", sortable: "true"},
                            {field: "undertaker", title: "承担人", align: "center", valign: "middle", sortable: "true"},
                            {field: "applicantName", title: "申请人", align: "center", valign: "middle", sortable: "true"},
                            {field: "status", title: "状态", align: "center", valign: "middle", sortable: "true"},

                            {field: "applyDate", title: "申请时间", align: "center", valign: "middle", sortable: "true",
                                formatter:function(value,row,index){
                                    var ret;
                                    if(row.applyDate!=null&&row.generateDate!=0){
                                        var stringDate = row.applyDate;
                                        ret=new Date(stringDate)
                                        return ret.toLocaleDateString();
                                    }

                                }
                            },
                            {
                                title: "操作",
                                align: 'center',
                                valign: 'middle',
                                width: 60, // 定义列的宽度，单位为像素px
                                events: operateEvents,
                                formatter: function(value,row,index){
                                    return operateFormatter(value,row,index);
                                }

                            }

                        ],

                });//结束填充

                // $('#table').bootstrapTable('load',successdata);
            },
            error:function (errdata) {
                $("#queryConstraint").hide();
                alert("search the projects failed!");
                console.log("search the projects failed!"+errdata);
            }
        });
    });
}

function operateFormatter(value, row, index) {
    console.log("enter the operation!");
    var content=row.detail;
    if (content.indexOf("费用小计") != -1||content.indexOf("费用总计") != -1) {
        return;
    }
    else {
        return [
            '<button type="button" class="RoleOfDel btn btn-info  " >删除</button>'
        ].join('');
    }
}

window.operateEvents = {
    'click .RoleOfDel': function (e, value, row, index) {
        var url = "/delete-" + row.reimburseID + "-reimburse";
        console.log("delete the row,id is " + row.reimburseID + ",the request url is " + url);
        //delete the lawyer by id
        $.ajax({
            type: "GET",
            url: url,
            //不需要传递参数
            success: function (data) {
                console.log("delete the reimburse,the return msg from server is " + data);
                $("#table").bootstrapTable('refresh');
            },
            error: function (errdata) {
                alert("delete lawyer failed!");
                console.log("delete lawyer failed!" + errdata);
            }
        });
    }
    //,
    // 'click .RoleOfEdit': function (e, value, row, index) {
    //     console.log("edit the row,id is " + row.reimburseID);
    //     alert("功能开发中");
    // }
}