$(document).ready(function(){
    // 加载日期选择插件
    $('.form_date').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 2,
        forceParse: 0
    });
    // 完成加载日期选择插件

    var responseMSG=$("#responseMSG").val();
    if (""!=responseMSG) {
        $("#reimburseMSG").html(responseMSG);
        $("#responseMSG").val("");
    }
});

