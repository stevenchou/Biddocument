﻿var setting = {
    view: {
        addDiyDom: addDiyDom
    },
    check: {
        enable: false,
    }
};




$(document).ready(function(){
    //隐藏左侧导航，腾出给右侧空间
    $("#togglesiderbar").click();
    initZtree();

    attachSelectedFile();

    deletAttachClick();

});
function attachSelectedFile() {
    var selectedFileNames=sessionStorage.fileNames;
    sessionStorage.fileNames="";
    var filenameArray=selectedFileNames.split(',');
    var i;
    var len=filenameArray.length;
    if (len>0&&""!=selectedFileNames){
        for(i=0;i<len;i++){
            attachFile(filenameArray[i])
        }
    }
}

function initZtree() {
    console.log("the workProjectName is "+sessionStorage.workProjectName);
    var projectName=$("#projectName").text();
    if (""==projectName) {
        projectName=sessionStorage.workProjectName;
    }

    $.ajax({
        type: "GET",
        url: "getProjectFileTreeJSON",
        dataType: "json",
        //保证前台传的数组后台可以拿到
        traditional: true,
        contentType: "application/json",
        data: {projectname:projectName},
        // dataType: "json",
        success: function (retdata) {
            $.fn.zTree.init($("#treeDemo"), setting, retdata);
            zTree = $.fn.zTree.getZTreeObj("treeDemo");
            rMenu = $("#rMenu");
            console.log("get the project file tree successfully! [" + retdata + "]");

        },
        error: function (errdata) {
            alert("get the project file tree failed!");
            console.log("create projecttype failed!" + errdata);
        }
    });
}

// 添加附件图标，将文件添加到邮件附件
function addDiyDom(treeId, treeNode) {
    var aObj = $("#" + treeNode.tId + "_a");
    //点击添加到附件
    if ($("#attachBtn_"+treeNode.id).length>0) return;
    var attachStr = "<img class='diyImg' src='/image/attachment.ico' id='attachBtn_" + treeNode.tId + "' title='添加附件'/>";
    if(!treeNode.isParent){
        aObj.after(attachStr);
    }
    //绑定响应函数
    var attachBtn = $("#attachBtn_"+treeNode.tId);
    if (attachBtn) attachBtn.bind("click", function(){
        attachFile(getProjectRelativePath(treeNode.path));
        deletAttachClick();
    });
}

function attachFile(path){
    console.log("the new attach file path is "+path);
    var selectedFileNames=sessionStorage.fileNames;
    if(!isPathExist(selectedFileNames,path)){
        // 获取mailattach的html，添加内容，再填充到mailattach上去
        var attachHTML=$("#mailattach").html();
        var html='<div class="deleteAttach alert alert-success alert-dismissable ">\n' +
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>\n' +
            path+
            '</div>\n';
        attachHTML+=html;

        $("#mailattach").html(attachHTML);

        //添加到sessionStorage中
        if(""!=selectedFileNames){
            sessionStorage.fileNames+=",";
        }
        sessionStorage.fileNames+=path;
    }else {
        alert("此文件已经在附件中，请勿重复选择！");

    }
}

function isPathExist(selectedFileNames,path) {
    var fileNameArray=selectedFileNames.split(",");
    for (var i=0;i<fileNameArray.length;i++){
        if (path==fileNameArray[i]){
            return true;
        } else {
          continue;
        }
    }
    return false;
}

function deleteAttachFromSession(path) {
    var selectedPath =sessionStorage.fileNames;
    var newPath="";
    var pathArray=selectedPath.split(",")
    for (var i=0;i<pathArray.length;i++){
        if (path!=pathArray[i]) {
            newPath+=pathArray[i]+",";
        }
    }
    //删除最后一个，
    newPath=newPath.substring(0,(newPath.length-1));
    sessionStorage.fileNames=newPath;
}

function deletAttachClick() {
    $(".deleteAttach").click(function () {
        var text = $(this).text();
        var sub = text.replace('X', '');
        //去掉空格
        sub = sub.replace(/\ +/g, "");
        //去掉回车换行
        sub = sub.replace(/[\r\n]/g, "");
        console.log("the delete file path is " + sub);
        deleteAttachFromSession(sub);
    });
}

