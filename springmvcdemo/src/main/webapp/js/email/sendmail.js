﻿// 将收件人作为默认邮件地址添加元素
var activeElement=$("#receiver");
$(document).ready(function() {
    // 根据url，确定哪个子页面处于激活态
    var url = self.location.href;
    console.log("the url is "+url);

    //发件箱激活
    if(url.indexOf("emailto") != -1){
        console.log("active send mail")
        // $("#panel-sendMail").show();
        $("#href-sendMail").click();

    }

    // 获取sessionStorage中的eemail地址，并添加到收件人
    paddingEmail();
    // 点击添加抄送，显示抄送输入框，同时按钮变成取消抄送，再次点击，取消抄送
    $("#btnCopyTo").click(function () {
        $("#mailCopyTo").toggle(
            "nomal",function(){
                $("#copyToInput").val("");
                $("#btnCopyTo").html()=="添加抄送" ? $("#btnCopyTo").html("取消抄送") : $("#btnCopyTo").html("添加抄送");}
        );
    });


    $("#btnSecurityTo").click(function () {
        $("#mailSecurityTo").toggle(
            "nomal",function(){
                $("#SecurityToInput").val("");
                $("#btnSecurityTo").html()=="添加密送" ? $("#btnSecurityTo").html("取消密送") : $("#btnSecurityTo").html("添加密送");}
        );
    });

    // 点击分别发送，清除抄送和密送
    $("#btnToEveryone").click(function () {
        $("#copyToInput").val("");
        $("#btnCopyTo").html("添加抄送")
        $("#mailCopyTo").hide();

        $("#SecurityToInput").val("");
        $("#mailSecurityTo").hide();
        $("#btnSecurityTo").html("添加密送");
    });

    // 拖动到此处后处理
    $("#dragTarget").ondrop=function(){
        console.log("drag enter the target!")
    };

    activeAddressInput();
    
    //点击团队成员、常见客户时，修改收件人地址和email
    receiverAddress();

    //点击发送按钮，响应函数
    sendMailClick();
});

function paddingEmail() {
    var email=sessionStorage.email;
    var name=sessionStorage.name;
    var paddingContent=name+"<"+email+">;"
    console.log("the receiver email is "+email);
    $("#receiver").attr("value",paddingContent)
}

function receiverAddress() {
    $(".receiver").click(function () {
        console.log("click the receiver,now modify the active input address");
        var originalAddress=activeElement.val();
        console.log("the original address is "+originalAddress);

        //获取当前点击对象的名字
        // 获取email地址
        var email=$(this).attr('title');
        var name=$(this).attr('alt');

        var receiverInfo=name+"<"+email+">;"
        var pos=originalAddress.indexOf(receiverInfo);
        var newAddress;
        if(-1!=pos){
            newAddress=originalAddress.replace(receiverInfo,"");
            alert("删除邮件地址成功！")
        }else {
            newAddress=originalAddress+receiverInfo;
        }
        console.log("the new address is "+newAddress);
        // activeElement.attr("value",newAddress);
        activeElement.val(newAddress);
    });
}

function sendMailClick() {
    $("#sendMail").click(function () {
        var filepath=$(".deleteAttach").text();
        // 去除所有空格:
        filepath = filepath.replace(/\s+/g,"");
        console.log("the filepath is "+filepath);

        // var mailContent=$("#mailContent").text();
        var mailContent=$("#mailContent").html();
        console.log("the mailContent is "+mailContent);
        //ajax将数据传到后台
        $.ajax({
            type: "get",
            url: "sendMail",
            dataType: "json",
            //保证前台传的数组后台可以拿到
            traditional: true,
            contentType:"application/json",
            data: {
                receiver:$("#receiver").val(),
                filepath: filepath,
                copyToInput:$("#copyToInput").val(),
                SecurityToInput: $("#SecurityToInput").val(),
                theme:$("#theme").val(),
                mailContent:mailContent
            },
            dataType: "text",
            success: function (data) {
                console.log("the return value from server is  [" + data + "]");
                alert(data);
            },
            error: function (errdata) {
                alert("send mail failed!");
                console.log("send mail failed!"+errdata);
            }
        })

    });
}


function activeAddressInput() {
    $("#receiver").focusin(function () {
        activeElement=$("#receiver");
        console.log("the activeElement is copyToInput");
    });

    $("#copyToInput").focusin(function () {
        activeElement=$("#copyToInput");
        console.log("the activeElement is copyToInput");
    });

    $("#SecurityToInput").focusin(function () {
        activeElement=$("#SecurityToInput");
        console.log("the activeElement is SecurityToInput");
    });
}