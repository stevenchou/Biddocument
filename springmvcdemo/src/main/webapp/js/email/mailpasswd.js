$(document).ready(function() {
    $("#lawyername").change(function(){
        //要触发的事件
        console.log("the lawyername is "+$("#lawyername").val());

        //ajax向后台查询lawyer的邮箱
        $.ajax({
            type: "get",
            url: "getLawyerEmail",
            dataType: "text",
            //保证前台传的数组后台可以拿到
            traditional: true,
            contentType:"application/json",
            data: {
                lawyername:$("#lawyername").val()
            },
            // dataType: "json",
            success: function (data) {
                console.log("get lawyer email successfully! [" + data + "]");
                $("#lawyerEmail").val(data);
            },
            error: function (errdata) {
                alert("get lawyer email failed!");
                console.log("get lawyer email failed!"+errdata);
            }
        })

    });


})