﻿function inputgroupadd(obj,inputgroupname){
    html = '<div class="input-group saltIp col-md-12">'+
        '<input type="text" class="form-control  inputGrpItem" id="saltIp" name="'+inputgroupname+'" >'+
        '<span class="input-group-btn">'+
        '<button class="btn btn-info" type="button" data-toggle="tooltip" title="delete" id="delInputGrpItem"><span class="glyphicon glyphicon-minus"></span></button>'+
        '</span>'+
        '</div>'
    obj.insertAdjacentHTML('beforebegin',html)
}


$(document).ready(function(){
    // 加载日期选择插件
    $('.form_date').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 2,
        forceParse: 0
    });

    //加载文件上传组件
    $('.uploadlawyerfile').fileinput({
        // uploadUrl:'/uploadlawyerinfo',
        showCaption: true,
        showCancel:false,
        showUpload: false, //是否显示上传按钮
        showUploadedThumbs:false,
        showClose:false,
        autoReplace:true,
        maxFileCount:1,
        overwriteInitial:true,
        dropZoneEnabled: false,//是否显示拖拽区域
        showPreview : true, //是否显示预览
        language:'zh',
        // initialPreview:"<img src='/back_t.jpg' class='file-preview-image' />",
        initialPreviewCount:1,
        initialPreviewShowDelete:false
    });
    // 文件上传组建初始化完毕


    // js验证输入
    $('#defaultForm').bootstrapValidator({
        // message: 'This value is not valid',
        // feedbackIcons: {
        //     valid: 'glyphicon glyphicon-ok',
        //     invalid: 'glyphicon glyphicon-remove',
        //     validating: 'glyphicon glyphicon-refresh'
        // },
        fields: {
            lawyername: {/*键名username和input name值对应*/
                message: 'The username is not valid',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: 'lawyer name can not be empty'
                    },
                    stringLength: {/*长度提示*/
                        min: 2,
                        max: 30,
                        message: 'the lawyer name length must in 4 to 30 characters.'
                    }/*最后一个没有逗号*/
                }
            },
            birthday :{
                message: 'The birthday is not valid',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: 'birthday can not be empty'
                    }
                }
            },
            gender:{
                validators: {
                    notEmpty: {/*非空提示*/
                        message: '请选择性别！'
                    }
                }

            },
            // workiexplist:{
            //     validators: {
            //         notEmpty: {/*非空提示*/
            //             message: 'work experience can not be empty'
            //         }
            //     }
            // },
            // professionalfieldlist:{
            //     validators: {
            //         notEmpty: {/*非空提示*/
            //             message: 'professional field can not be empty'
            //         }
            //     }
            // },
            employeddate :{
                message: 'The employeddate is not valid',
                    validators: {
                    notEmpty: {/*非空提示*/
                        message: 'employed date name can not be empty'
                    }
                }
            },
            telephone: {
                message: 'The telephone is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'telephone  can not be empty'
                    },
                    stringLength: {
                        min: 11,
                        max: 11,
                        message: 'telephone number length must be 11 characters.'
                    },
                    regexp: {
                        regexp: /^1[0-9]{10}$/,
                        message: 'please input right telephone number.'
                    }
                }
            },
            email: {
                message: 'The email is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'email  can not be empty'
                    },
                    emailAddress: {
                        message: '邮箱地址格式有误'
                    }
                }
            },
            IDcardno : {
                validators : {
                    notEmpty : {
                        message : 'ID number can not be empty.'
                    },
                    regexp: {
                        regexp: /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/,
                        message: 'The ID card number format is incorrect, for 15 and 18 id card Numbers!'
                    },
                    callback: {/*自定义，可以在这里与其他输入项联动校验*/
                        message: 'The id number is invalid!',
                        callback:function(value, validator,$field){
                            //15位和18位身份证号码的正则表达式
                            var regIdCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
                            //如果通过该验证，说明身份证格式正确，但准确性还需计算
                            var idCard = value;
                            if (regIdCard.test(idCard)) {
                                if (idCard.length == 18) {
                                    var idCardWi = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); //将前17位加权因子保存在数组里
                                    var idCardY = new Array(1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2); //这是除以11后，可能产生的11位余数、验证码，也保存成数组
                                    var idCardWiSum = 0; //用来保存前17位各自乖以加权因子后的总和
                                    for (var i = 0; i < 17; i++) {
                                        idCardWiSum += idCard.substring(i, i + 1) * idCardWi[i];
                                    }
                                    var idCardMod = idCardWiSum % 11;//计算出校验码所在数组的位置
                                    var idCardLast = idCard.substring(17);//得到最后一位身份证号码
                                    //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
                                    if (idCardMod == 2) {
                                        if (idCardLast == "X" || idCardLast == "x") {
                                            return true;
                                            //alert("恭喜通过验证啦！");
                                        } else {
                                            return false;
                                            //alert("身份证号码错误！");
                                        }
                                    } else {
                                        //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
                                        if (idCardLast == idCardY[idCardMod]) {
                                            //alert("恭喜通过验证啦！");
                                            return true;
                                        } else {
                                            return false;
                                            //alert("身份证号码错误！");
                                        }
                                    }
                                }
                            } else {
                                //alert("身份证格式不正确!");
                                return false;
                            }
                        }
                    }
                }
            },
            inputIDcard:{
                message: 'The birthday is not valid',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: 'ID card can not be empty'
                    },
                    regexp : {
                        regexp : /(?:jpg|gif|png|jpeg|bmp|JPG|GIF|PNG|JPEG|BMP)$/,
                        message : '文件格式仅限常用图片格式：jpg，gif，png，jpeg，bmp，JPG，GIF，PNG，JPEG，BMP'
                    }
                }
            },
            inputpersonalimg:{
                message: 'The birthday is not valid',
                validators: {
                    notEmpty: {/*非空提示*/
                        message: 'personal image can not be empty'
                    },
                    regexp : {
                        regexp : /(?:jpg|gif|png|jpeg|bmp|JPG|GIF|PNG|JPEG|BMP)$/,
                        message : '文件格式仅限常用图片格式：jpg，gif，png，jpeg，bmp，JPG，GIF，PNG，JPEG，BMP'
                    }
                }
            }

        // end fields
        }
    });
    //结束js验证

    // input group中，点击删除按钮，删除一个input输入框
    $(document).on('click','#delInputGrpItem',function(){
        var el = this.parentNode.parentNode
        var saltIp = $(this).parent().parent().find('#saltIp').val()
        if (saltIp == ""){
            el.parentNode.removeChild(el)
            return
        }
    });
    // end function

    // 解决bootstrapValidator与datetimepicker混合使用时日期验证不刷新的问题
    $('#birthday').change(function() {
        $('#defaultForm').data('bootstrapValidator').updateStatus('birthday',
            'NOT_VALIDATED',null).validateField('birthday');
    });
    $('#employeddate').change(function() {
        $('#defaultForm').data('bootstrapValidator').updateStatus('employeddate',
            'NOT_VALIDATED',null).validateField('employeddate');
    });
    // end function

    $(document).on('click','#IDcardno',function() {
        var inputGrpItemEmpty=false;
        // noinspection JSAnnotator
        $(".inputGrpItem").each(function () {
            var name = $(this).attr("name");
            if ($(this).val() == "") {
                console.log(name  + " can not be empty");
                inputGrpItemEmpty=true;
                $(this).focus();
                alert("鼠标所在元素不得为空！");
                return false;;
            }
        });

    });
    //end function




});





