﻿$(document).ready(function(){
    // 加载日期选择插件
    $('.form_date').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView : 4,
        // minView: 'decade',
        maxViewMode: 1,
        minViewMode:1,
        minView: 2,
        forceParse: 0
    });


    //加载文件上传组件
    $('.uploadfile').fileinput({
        // uploadUrl:'/uploadlawyerinfo',
        showCaption: true,
        showCancel:false,
        showUpload: false, //是否显示上传按钮
        showUploadedThumbs:false,
        showClose:false,
        autoReplace:true,
        maxFileCount:1,
        overwriteInitial:true,
        dropZoneEnabled: false,//是否显示拖拽区域
        showPreview : true, //是否显示预览
        language:'zh',
        // initialPreview:"<img src='/back_t.jpg' class='file-preview-image' />",
        initialPreviewCount:1,
        initialPreviewShowDelete:false
    });
    // 文件上传组件加载完毕

    // js验证输入
    $('#defaultForm').bootstrapValidator({
        fields: {
            certificateID: {
                message: 'The certificate ID is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'certificate ID can not be empty'
                    },
                    numeric: {message: 'only be number'}
                }
            },
            certificatedate: {
                message: 'The certificate date is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'certificate date can not be empty'
                    }
                }
            },
            certificatecard: {
                message: 'The certificate card is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'certificate card can not be empty'
                    },
                    regexp : {
                        regexp : /(?:jpg|gif|png|jpeg|bmp|JPG|GIF|PNG|JPEG|BMP)$/,
                        message : '文件格式仅限常用图片格式：jpg，gif，png，jpeg，bmp，JPG，GIF，PNG，JPEG，BMP'
                    }
                }
            }
        }
    });
    //结束JS验证

    // 解决bootstrapValidator与datetimepicker混合使用时日期验证不刷新的问题
    $('#certificatedate').change(function() {
        $('#defaultForm').data('bootstrapValidator').updateStatus('certificatedate',
            'NOT_VALIDATED',null).validateField('certificatedate');
    });
    // $('#lawyername').change(function() {
    //     $('#defaultForm').data('bootstrapValidator').updateStatus('lawyername',
    //         'NOT_VALIDATED',null).validateField('lawyername');
    // });
    // end function





});