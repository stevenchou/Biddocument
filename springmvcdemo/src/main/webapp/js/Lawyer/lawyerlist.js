﻿$(document).ready(function () {
    $('#table').bootstrapTable({
        url: "/getlawyerdata",
        method: 'get',
        cache: false,
        // height: 680,
        striped: true,
        pagination: true,
        pageSize: 5,
        pageNumber: 1,
        pageList: [2, 5, 10, 20], sidePagination: 'client',
        showColumns: true,
        showRefresh: false,
        showExport: true,
        exportTypes: ['csv', 'txt', 'xml'],
        search: false,
        clickToSelect: true,
        columns:
            [
                {field: "checked", checkbox: true},
                // {field:"name",title:"测试姓名",align:"center",valign:"middle",sortable:"true"},
                // {field:"age",title:"年龄",align:"center",valign:"middle",sortable:"true"},
                // {field:"sex",title:"性别",align:"center",valign:"middle",sortable:"true"},
                {field: "DB_id", title: "DB_id", visible: false},
                {field: "lawyername", title: "姓名", align: "center", valign: "middle", sortable: "true"},
                {field: "gender", title: "性别", align: "center", valign: "middle", sortable: "true"},
                {field: "birthday", title: "出生日期", align: "center", valign: "middle", sortable: "true",
                    formatter:function(value,row,index){
                        var ret;
                        if(row.birthday!=null){
                            var stringDate = row.birthday;
                            ret=new Date(stringDate)
                        }
                        return ret.toISOString().substring(0,10);
                    }
                },
                {field: "employdate", title: "从业日期", align: "center", valign: "middle", sortable: "true",
                    formatter:function(value,row,index){
                        var ret;
                        if(row.employdate!=null){
                            var stringDate = row.employdate;
                            ret=new Date(stringDate)
                        }
                        return ret.toISOString().substring(0,10);
                    }
                },
                {field: "telephone", title: "电话", align: "center", valign: "middle"},
                {field: "ID_num", title: "身份证", align: "center", valign: "middle",},
                {field: "ID_cardpath", title: "身份证路径", align: "center", valign: "middle",
                    formatter:function(value,row,index){
                        var s;
                        if(row.ID_cardpath!=null){
                            var url = row.ID_cardpath;
                            s = '<a class = "view"  href="javascript:void(0)"><img style="width:100px;"  src="'+url+'" /></a>';
                        }
                        return s;
                    }
                    },
                {field: "personalimg", title: "个人照片", align: "center", valign: "middle",
                formatter:function(value,row,index){
                    var s;
                    if(row.personalimg!=null){
                        var url = row.personalimg;
                        s = '<a class = "view"  href="javascript:void(0)"><img style="width:100px;"  src="'+url+'" /></a>';
                    }
                    return s;}
                },
                {field: "position", title: "律所职务", align: "center", valign: "middle"},
                {field: "certificatecardNo", title: "执业证号", align: "center", valign: "middle"},
                {field: "certificatecardpath", title: "执业证路径", align: "center", valign: "middle"},
                {field: "eduinfos", title: "教育经历", align: "center", valign: "middle"},
                {field: "workexperiences", title: "工作经历", align: "center", valign: "middle"},
                {field: "professionfields", title: "专业领域", align: "center", valign: "middle"},
                {
                    title: "操作",
                    align: 'center',
                    valign: 'middle',
                    width: 200, // 定义列的宽度，单位为像素px
                    events: operateEvents,
                    formatter: operateFormatter(),
                }

            ],

    });

    function operateFormatter(value, row, index) {
        console.log("enter the operation!");
        return [
            '<button type="button" class="RoleOfDel btn btn-info  " >删除</button>',
            '<button type="button" class="RoleOfEdit btn btn-info  " >编辑</button>'
        ].join('');

    }


});

window.operateEvents = {
    'click .RoleOfDel': function (e, value, row, index) {
        var url = "/delete-" + row.DB_id + "-lawyer";
        console.log("delete the row,id is " + row.DB_id + ",the request url is " + url);
        //delete the lawyer by id
        $.ajax({
            type: "GET",
            url: url,
            //不需要传递参数
            success: function (data) {
                console.log("delete the lawyer,the return msg from server is " + data);
                $("#table").bootstrapTable('refresh');
            },
            error: function (errdata) {
                alert("delete lawyer failed!");
                console.log("delete lawyer failed!" + errdata);
            }
        });
    },
    'click .RoleOfEdit': function (e, value, row, index) {
        console.log("edit the row,id is " + row.DB_id)
    }
}



