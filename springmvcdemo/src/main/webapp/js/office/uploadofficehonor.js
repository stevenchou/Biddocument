﻿$(document).ready(function(){
    // 加载日期选择插件
    $('.form_date').datetimepicker({
        format: 'yyyy',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView : "decade",
        minView: 'decade',
        maxViewMode: 1,
        minViewMode:1,
        // minView: 2,
        forceParse: 0
    });

    //加载文件上传组件
    $('.uploadfile').fileinput({
        // uploadUrl:'/uploadlawyerinfo',
        showCaption: true,
        showCancel:false,
        showUpload: false, //是否显示上传按钮
        showUploadedThumbs:false,
        showClose:false,
        autoReplace:true,
        maxFileCount:1,
        overwriteInitial:true,
        dropZoneEnabled: false,//是否显示拖拽区域
        showPreview : true, //是否显示预览
        language:'zh',
        // initialPreview:"<img src='/back_t.jpg' class='file-preview-image' />",
        initialPreviewCount:1,
        initialPreviewShowDelete:false
    });

    $('#officehonorform').bootstrapValidator({
        fields: {
            honorcontent: {
                /*键名username和input name值对应*/
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'honor content can not be empty'
                    }
                }
            },
            inputhonorfile: {
                /*键名username和input name值对应*/
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'input honor file can not be empty'
                    },
                    regexp : {
                        regexp : /(?:jpg|gif|png|jpeg|bmp|JPG|GIF|PNG|JPEG|BMP)$/,
                        message : '文件格式仅限常用图片格式：jpg，gif，png，jpeg，bmp，JPG，GIF，PNG，JPEG，BMP'
                    }

                }
            },
            honoryear: {
                /*键名username和input name值对应*/
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'honor year can not be empty'
                    }
                }
            }
        }
    });


    // 解决bootstrapValidator与datetimepicker混合使用时日期验证不刷新的问题
    $('#honoryear').change(function() {
        $('#officehonorform').data('bootstrapValidator').updateStatus('honoryear',
            'NOT_VALIDATED',null).validateField('honoryear');
    });


});