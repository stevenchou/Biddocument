$(document).ready(function(){
    //加载文件上传组件
    $('.uploadfile').fileinput({
        // uploadUrl:'/uploadlawyerinfo',
        showCaption: true,
        showCancel:false,
        showUpload: false, //是否显示上传按钮
        showUploadedThumbs:false,
        showClose:false,
        autoReplace:true,
        maxFileCount:1,
        overwriteInitial:true,
        dropZoneEnabled: false,//是否显示拖拽区域
        showPreview : true, //是否显示预览
        language:'zh',
        // initialPreview:"<img src='/back_t.jpg' class='file-preview-image' />",
        initialPreviewCount:1,
        initialPreviewShowDelete:false
    });
    // 文件上传组建初始化完毕


    // js验证输入
    $('#defaultForm').bootstrapValidator({
        // message: 'This value is not valid',
        // feedbackIcons: {
        //     valid: 'glyphicon glyphicon-ok',
        //     invalid: 'glyphicon glyphicon-remove',
        //     validating: 'glyphicon glyphicon-refresh'
        // },
        fields: {
            filetype: {
                /*键名username和input name值对应*/
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'lawyer name can not be empty'
                    }/*最后一个没有逗号*/
                }
            },
            projectname:{
                /*键名username和input name值对应*/
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'lawyer name can not be empty'
                    }/*最后一个没有逗号*/
                }
            },
            biddocument:{
                /*键名username和input name值对应*/
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'lawyer name can not be empty'
                    }/*最后一个没有逗号*/
                }
            }
        }
    });
    //end js 验证

// add a new file attribute
    $("#newfileattribute").click(function() {
        console.log("add a new file attribute,the name is " + $("#newprojecttype").val());

        // 利用ajax动态创建custom
        $.ajax({
            type: "POST",
            url: "/newfileattribute",
            data: {attributename: $("#fileattribute").val(), attributebrief: $("#fileattributebrief").val()},
            // dataType: "json",
            success: function (data) {
                $("#allattribute").append("<option>" + $("#fileattribute").val() + "</option>");
                $('#allattribute').selectpicker('val',$("#fileattribute").val());
                // 缺一不可
                $('#allattribute').selectpicker('refresh');
                $('#allattribute').selectpicker('render');

                alert("add new projecttype successfully! [" + $("#fileattribute").val() + "]");
                console.log("add new projecttype successfully! [" + data + "]");

            },
            error: function (errdata) {
                alert("create projecttype failed!");
                console.log("create projecttype failed!"+errdata);
            }
        });
    });
    // finished add new projecttype


});