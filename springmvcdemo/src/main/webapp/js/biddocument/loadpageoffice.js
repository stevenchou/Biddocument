﻿// var selectedprojects="";
function randomString(len) {
    len = len || 32;
    var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz';    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
    var maxPos = $chars.length;
    var pwd = '';
    for (i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}
function padding_selectedprojects (datalength) {
    if (selectedprojects == "") {
        alert("请选择要填充数据");
    }
    else {
        var idArray = [];
        for (var i = 0; i < datalength; i++) {
            idArray.push(selectedprojects[i].casename);
        }
        var randStr=randomString(20);
        console.log("the rand String is "+randStr);
        console.log("now,write into word using the pageoffice js API")
        var POPara="PO_"+randStr;
        document.getElementById("PageOfficeCtrl1").DataRegionList.Add(POPara,"padding "+idArray);
        console.log("write into word using the pageoffice js API successfully!")
        generateTable(datalength);
    }
}
//显示遮罩层
function showMask(){
    var heightofwindows=$(window).height();
    console.log("the height of windows is "+heightofwindows);
    $('.activiheight').css("height","100%");
    console.log("begain to scroll");
    document.body.scrollTop = document.body.scrollHeight;
    $("#shadercontent").css("top",heightofwindows);
    $("#mask").css("height",$(document).height());
    $("#mask").css("width",$(document).width());
    $("#mask").css("display","bloack");
    $("#mask").show();
    window.scrollBy(0,10);
    $("#mask").click();
    // 禁止滚动条滑动
    $(document.body).css({
        "overflow-y": "hidden"
    });
}



$(document).ready(function(){
    // 初始化时间插件
    // 加载日期选择插件
    $('.form_date').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView : 4,
        // minView: 'decade',
        maxViewMode: 1,
        minViewMode:1,
        minView: 2,
        forceParse: 0
    });

    $("#allProjectInfo").click(function(){
        //标记写入位置
        // document.getElementById("PageOfficeCtrl1").DataRegionList.Add("","案例信息如下\n ");
        showMask();
    });


    //end jump to top
    //关闭遮罩层
    $("#cloaseshader").click(function () {
        $('.activiheight').css("height","0px");
        //隐藏遮罩层
        $("#mask").hide();
        });

    // 根据用户筛选条件，获取项目信息
    $("#searchprojects").click(function () {
        console.log("根据用户筛选条件，获取项目信息");
        console.log("the selectbyyear is "+$("#selectbyyear").val());

        //用ajax获取数据
        $.ajax({
            type:"get",
            url:"/selectprojects",
            dataType: "json",
            //保证前台传的数组后台可以拿到
            traditional: true,
            contentType:"application/json",
            data:{
                projectBeginDate:$("#projectBeginDate").val(),
                projectEndDate:$("#projectEndDate").val(),
                selectbytype:$("#selectbytype").val()
            },
            success:function (successdata) {
                console.log("serch the projects successfully,the selected projects data is "+successdata);
                // 用bootstrap table填充
                $('#selectedprojects').bootstrapTable({
                    // url: "/getprojectdata",
                    method: 'get',
                    cache: false,
                    height: 500,
                    striped: true,
                    pagination: true,
                    pageSize: 5,
                    pageNumber: 1,
                    pageList: [ 5, 10, 20], sidePagination: 'client',
                    // showColumns: true,
                    // showRefresh: true,
                    // showExport: true,
                    // exportTypes: ['csv', 'txt', 'xml'],
                    search: false,
                    clickToSelect: true,
                    data:successdata,
                    columns:
                        [
                            {field: "checked", checkbox: true},
                            // {field:"name",title:"测试姓名",align:"center",valign:"middle",sortable:"true"},
                            // {field:"age",title:"年龄",align:"center",valign:"middle",sortable:"true"},
                            // {field:"sex",title:"性别",align:"center",valign:"middle",sortable:"true"},
                            {field: "caseprojectid", title: "caseprojectid", visible: false},
                            {field: "casename", title: "项目名称", align: "center", valign: "middle", sortable: "true"},
                            {field: "casebrief", title: "项目简介", align: "center", valign: "middle", sortable: "true"},
                            {field: "custom", title: "客户", align: "center", valign: "middle", sortable: "true"},
                            {field: "signaturedate", title: "签约日期", align: "center", valign: "middle", sortable: "true"},
                            {field: "terminationdate", title: "届满日期", align: "center", valign: "middle"},
                            {field: "signatureLawyer", title: "签约律师", align: "center", valign: "middle"},
                            {field: "responsibleLawyers", title: "负责律师", align: "center", valign: "middle"},
                            {field: "teamnumbers", title: "服务团队", align: "center", valign: "middle"},
                            {field: "evidencefilepath", title: "合同路径", align: "center", valign: "middle"}
                        ],

                });//结束填充
                $('#selectedprojects').bootstrapTable('load',successdata);
            },
            error:function (errdata) {
                alert("search the projects failed!");
                console.log("search the projects failed!"+errdata);
            }
        });
    });//end 根据用户筛选条件，获取项目信息


    $("#paddingprojects").click(function () {
        console.log("padding the projects into word");
        var selectedprojects = $('#selectedprojects').bootstrapTable('getSelections');
        if (selectedprojects == "") {
            alert("请选择要填充的数据");
        }
        else {
            // 选择了数据，关闭遮罩层
            $("#cloaseshader").click();
            //填充案例数据到word文件中
            var datalength=selectedprojects.length;
            console.log("the padding data length is "+datalength)
            generateTable(datalength);
            paddingprojectData(selectedprojects,datalength);
            paddingContractFile(selectedprojects,datalength);
        }

    });
    // 结束填充

    $("#pading").click(function () {
        var testdata='[{"terminationdate":1529856000000,"casebrief":"的冯绍峰","responsibleLawyers":"null西溪村\nsfg\n","evidencefilepath":"upload/project/contract/glass.jpg","teamnumbers":"null西溪村\nsfg\n","custom":"坎德拉","signaturedate":1530028800000,"signatureLawyer":"sfg","casename":"大幅度发","caseprojectid":2,"projectfields":"null基金\n基金2\n"},{"terminationdate":1530201600000,"casebrief":"需","responsibleLawyers":"nullsfg\n","evidencefilepath":"upload/project/contract/glass.jpg","teamnumbers":"nullsfg\n","custom":"对方","signaturedate":1530115200000,"signatureLawyer":"sfg","casename":"徐吃不下","caseprojectid":3,"projectfields":"null基金\n基金2\n"},{"terminationdate":1530720000000,"casebrief":"电饭锅地方","responsibleLawyers":"null西溪村\nsfg\n","evidencefilepath":"upload/project/contract/glass.jpg","teamnumbers":"null西溪村\nsfg\n","custom":"坎德拉","signaturedate":1530115200000,"signatureLawyer":"sfg","casename":"更大幅度发换个地方","caseprojectid":8,"projectfields":"null基金\n基金2\n"}]';
        //加一是因为需要生成表头
        var datalength=3+1;
        generateTableAndPaddingData(testdata,datalength);
        var obj = $.parseJSON(testdata);
        var pad = "Function padding()" + " \r\n"
            +'Dim tableNew As Table                        \r\n'
            +' Set tableNew = ActiveDocument.Tables(1)                   \r\n'
            +' tableNew.Cell(1,1).Range.InsertAfter "序号"                     \r\n'
            +' tableNew.Cell(1,2).Range.InsertAfter "项目名称"                      \r\n'
            +' tableNew.Cell(1,3).Range.InsertAfter "项目简介"                      \r\n'
            +' tableNew.Cell(1,4).Range.InsertAfter "服务客户"                      \r\n'
            +' tableNew.Cell(1,5).Range.InsertAfter "服务年份"                      \r\n'
            +' tableNew.Cell(1,6).Range.InsertAfter "负责律师"                      \r\n';
            for (var i = 0; i < datalength; i++) {
                var i2=i+2;
                var i1=i+1;
                pad +='tableNew.Cell('+i2+',1).Range.InsertAfter "'+i1+'"    \r\n';
                pad +='tableNew.Cell('+i2+',2).Range.InsertAfter "'+obj[i].casename+'"    \r\n';
                pad +='tableNew.Cell('+i2+',3).Range.InsertAfter "'+obj[i].casebrief+'"    \r\n';
                pad +='tableNew.Cell('+i2+',4).Range.InsertAfter "'+obj[i].custom+'"    \r\n';
                pad +='tableNew.Cell('+i2+',5).Range.InsertAfter "'+obj[i].signaturedate+'"    \r\n';
                pad +='tableNew.Cell('+i2+',6).Range.InsertAfter "'+obj[i].responsibleLawyers+'"    \r\n';
            }
            pad += "End Function " + " \r\n";
        document.getElementById("PageOfficeCtrl1").RunMacro("padding", pad);
    });

});


//生成表格相关的函数
function generateTable(datalength) {
    //加一是因为需要生成表头
    var rowlen = datalength+1;
    var mac = "Function myfunc()" + " \r\n"
        +"ActiveDocument.Tables.Add Range:=Selection.Range, NumRows:="+rowlen+", NumColumns:= _"+ " \r\n"
        +"6, DefaultTableBehavior:=wdWord9TableBehavior, AutoFitBehavior:= _"+ " \r\n"
        +"wdAutoFitFixed"+ " \r\n"
        +"With Selection.Tables(1)"+ " \r\n"
        +'If .Style <> "网格型" Then'+ " \r\n"
        +'.Style = "网格型"'+ " \r\n"
        +"End If"+ " \r\n"
        +".ApplyStyleHeadingRows = True"+ " \r\n"
        +".ApplyStyleLastRow = False"+ " \r\n"
        +".ApplyStyleFirstColumn = True"+ " \r\n"
        +".ApplyStyleLastColumn = False"+ " \r\n"
        +".ApplyStyleRowBands = True"+ " \r\n"
        +".ApplyStyleColumnBands = False"+ " \r\n"
        +"End With"+ " \r\n"
        + "End Function " + " \r\n";
    document.getElementById("PageOfficeCtrl1").RunMacro("myfunc", mac);


}

function paddingprojectData(selectedprojects,datalength){

    var pad = "Function padding()" + " \r\n"
        +'Dim tableNew As Table                        \r\n'
        +' Set tableNew = ActiveDocument.Tables(1)                   \r\n'
        +' tableNew.Cell(1,1).Range.InsertAfter "序号"                     \r\n'
        +' tableNew.Cell(1,2).Range.InsertAfter "项目名称"                      \r\n'
        +' tableNew.Cell(1,3).Range.InsertAfter "项目简介"                      \r\n'
        +' tableNew.Cell(1,4).Range.InsertAfter "服务客户"                      \r\n'
        +' tableNew.Cell(1,5).Range.InsertAfter "服务年份"                      \r\n'
        +' tableNew.Cell(1,6).Range.InsertAfter "负责律师"                      \r\n';
    for (var i = 0; i < datalength; i++) {
        var i2=i+2;
        var i1=i+1;
        pad +='tableNew.Cell('+i2+',1).Range.InsertAfter "'+i1+'"    \r\n';
        pad +='tableNew.Cell('+i2+',2).Range.InsertAfter "'+selectedprojects[i].casename+'"    \r\n';
        pad +='tableNew.Cell('+i2+',3).Range.InsertAfter "'+selectedprojects[i].casebrief+'"    \r\n';
        pad +='tableNew.Cell('+i2+',4).Range.InsertAfter "'+selectedprojects[i].custom+'"    \r\n';
        pad +='tableNew.Cell('+i2+',5).Range.InsertAfter "'+selectedprojects[i].signaturedate+'"    \r\n';
        pad +='tableNew.Cell('+i2+',6).Range.InsertAfter "'+selectedprojects[i].responsibleLawyers+'"    \r\n';
    }
    pad += "End Function " + " \r\n";
    document.getElementById("PageOfficeCtrl1").RunMacro("padding", pad);
}

function paddingContractFile(selectedprojects,datalength){
    console.log("padding the contract file into word")
    //移动到文档结尾
    MoveToDocEnd();
    pagebreak();
    contentAlignLeft();
    var randStr2=randomString(20);

    document.getElementById("PageOfficeCtrl1").DataRegionList.Add("PO_"+randStr2,"这是全文附件部分，测试成功\n");
    //每次写完文字，移动到文章最后
    MoveToDocEnd();

    for (var i = 0; i < datalength; i++) {
        var projectName=selectedprojects[i].casename;
        var projectContractFile="http://localhost:8088/"+selectedprojects[i].evidencefilepath;
        console.log("the contract file path is "+projectContractFile)

        console.log("now,write the contract file and its name into word")
        var randStr=randomString(20);
        console.log("the rand String is "+randStr);
        var POPara="PO_"+randStr;
        document.getElementById("PageOfficeCtrl1").DataRegionList.Add(POPara,projectName);
        //每次写完文字，移动到文章最后
        MoveToDocEnd();
        var randStr3=randomString(20);
        document.getElementById("PageOfficeCtrl1").DataRegionList.Add("PO_"+randStr3,"\n");
        //每次写完文字，移动到文章最后
        MoveToDocEnd();
        document.getElementById("PageOfficeCtrl1").InsertInlineWebImage( projectContractFile );
        //每次写完，移动到文章最后
        MoveToDocEnd();
       if(i<(datalength-1)){
           pagebreak();
       }
    }
}


function closeNavinWindows(){
    var fun = "Function closenav()" + " \r\n"
    fun += 'CommandBars("Navigation").Visible = False    \r\n';
    fun += "End Function " + " \r\n";
    document.getElementById("PageOfficeCtrl1").RunMacro("closenav", fun);

}
//移动光标至文档结尾
function MoveToDocEnd() {
    var fun = "Function movtodocend()" + " \r\n"
    fun += 'Selection.EndKey unit:=wdStory    \r\n';
    fun += "End Function " + " \r\n";
    document.getElementById("PageOfficeCtrl1").RunMacro("movtodocend", fun);
}

//移动光标至当前行尾
function MoveToCurrentLineEnd() {
    var fun = "Function movtolineend()" + " \r\n"
    fun += 'Selection.EndKey unit:=wdLine    \r\n';
    fun += "End Function " + " \r\n";
    document.getElementById("PageOfficeCtrl1").RunMacro("movtolineend", fun);
    
}

//文件居左
function contentAlignLeft() {

    var fun = "Function alignleft()" + " \r\n"
    fun += 'Selection.ParagraphFormat.Alignment = wdAlignParagraphLeft  \r\n';
    fun += "End Function " + " \r\n";
    document.getElementById("PageOfficeCtrl1").RunMacro("alignleft", fun);
}

//文件居中
function contentAlignCenter() {

    var fun = "Function aligncenter()" + " \r\n"
    fun += 'Selection.ParagraphFormat.Alignment = wdAlignParagraphCenter  \r\n';
    fun += "End Function " + " \r\n";
    document.getElementById("PageOfficeCtrl1").RunMacro("aligncenter", fun);
}

//分页
function pagebreak() {
    var fun = "Function pagebreak()" + " \r\n"
    fun += 'Selection.InsertBreak Type:=wdPageBreak  \r\n';
    fun += "End Function " + " \r\n";
    document.getElementById("PageOfficeCtrl1").RunMacro("pagebreak", fun);
}