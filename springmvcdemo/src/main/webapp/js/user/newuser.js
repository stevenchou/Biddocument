﻿$(document).ready(function(){
    $('#newuserform').bootstrapValidator({
        fields: {
            username: {
                /*键名username和input name值对应*/
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        /*非空提示*/
                        message: 'username can not be empty'
                    },
                    regexp: {
                        /* 只需加此键值对，包含正则表达式，和提示 */
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: '只能是数字和字母_.'
                    }
                },
                password: {
                    /*键名username和input name值对应*/
                    message: 'The password is not valid',
                    validators: {
                        notEmpty: {
                            /*非空提示*/
                            message: 'password can not be empty'
                        },
                        regexp: {
                            /* 只需加此键值对，包含正则表达式，和提示 */
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '只能是数字和字母_.'
                        }
                    }
                }
            }
        }
    });



});