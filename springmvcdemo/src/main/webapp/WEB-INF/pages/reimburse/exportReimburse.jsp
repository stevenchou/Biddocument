<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/20
  Time: 22:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>导出报销</title>
</head>
<body>
<%@include file="../head.jsp"%>

<div id="main-content">
    <div style="height: 80%;overflow-y: scroll">
        <table class="col-md-12 table" style="overflow-x: scroll">
            <thead>
            <tr>
                <th>费用产生时间</th>
                <th>提交报销时间</th>
                <th>
                    报销类别
                </th>
                <th>
                    报销明细
                </th>
                <th>
                    报销金额
                </th>
                <th>
                    案号
                </th>
                <th>
                    案件名称
                </th>
                <th>
                    报销人
                </th>
                <th>
                    承担人
                </th>
                <th>
                    报销状态
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${selectedReimburses}" var="reimburse">
                <tr>
                    <td>${reimburse.generateDate}</td>
                    <td>${reimburse.applyDate}</td>
                    <td>${reimburse.category}</td>
                    <td>${reimburse.detail}</td>
                    <td>${reimburse.amount}</td>
                    <td>${reimburse.caseNum}</td>
                    <td>${reimburse.caseName}</td>
                    <td>${reimburse.applicantName}</td>
                    <td>${reimburse.undertaker}</td>
                    <td>${reimburse.state}</td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </div>

    <button class="btn btn-info" value="导出">导出</button>
</div>
</body>
</html>
