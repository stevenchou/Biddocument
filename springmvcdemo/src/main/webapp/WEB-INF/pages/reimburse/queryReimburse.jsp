<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/20
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>导出报销信息</title>
    <%@include file="../include/css-comnon.jsp"%>
</head>
<body>
<%@include file="../head.jsp"%>

<div id="main-content">
    <div id="queryConstraint">
        <div class="col-md-2"></div>
        <div class="col-md-8 column" style="height: 90%;overflow: scroll">
            <%--action="/queryReimburse" method="POST"--%>
            <form:form role="form"   id="defaultForm">
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>
                        注意!
                    </h4> 您正在进行的操作将会查询报销信息并导出！
                </div>

                <div class="form-group input-group">
                    <label class="input-group-addon">选择律师</label>
                    <div class="checkbox checkbox-success checkbox-inline">
                        <input type="checkbox" class="styled" id="selectAll"/>
                        <label>全选</label>
                    </div>
                    <br>
                    <c:forEach items="${Alllawyers}" var="lawyer"  varStatus="status">
                        <div class="checkbox checkbox-success checkbox-inline">
                            <input type="checkbox" class="styled" value="${lawyer.lawyername}" name="selectedlawyers"/>
                            <label>${lawyer.lawyername}</label>
                        </div>
                        &emsp;&emsp;&emsp;&emsp;
                        <%--//输出四个元素后，换行--%>
                        <c:if test="${status.count % 4 eq 0 || status.count eq 4}">
                            <br>
                        </c:if>
                    </c:forEach>
                </div>

                <div class="form-group">
                    <div class="input-group date form_date " data-date="" data-date-format="yyyy" data-link-field="dtp_begindate" data-link-format="yyyy-mm-dd">
                        <label for="dtp_begindate" class="input-group-addon">开始时间</label>
                        <input class="form-control" size="16" type="text" value="" readonly name="begindate"
                               id="begindate">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <input type="hidden" id="dtp_begindate" value=""/><br/>
                </div>

                <div class="form-group">
                    <div class="input-group date form_date " data-date="" data-date-format="yyyy" data-link-field="dtp_enddate" data-link-format="yyyy-mm-dd">
                        <label for="dtp_enddate" class="input-group-addon">结束时间</label>
                        <input class="form-control" size="16" type="text" value="" readonly name="enddate"
                               id="enddate">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <input type="hidden" id="dtp_enddate" value=""/><br/>
                </div>

                <div class="form-group input-group">
                    <label class="input-group-addon">报销状态</label>

                    <div class="checkbox checkbox-success checkbox-inline">
                        <input type="checkbox" class="styled" value="submitted" name="reimburseState" checked="checked"/>
                        <label>submitted</label>
                    </div>
                    &emsp;&emsp;&emsp;&emsp;
                    <div class="checkbox checkbox-success checkbox-inline">
                        <input type="checkbox" class="styled" value="approved" name="reimburseState" id="reimburseState"/>
                        <label>approved</label>
                    </div>
                </div>

            </form:form>
                <button class="btn btn-info" id="queryReimburse">查询</button>
        </div>
    </div>

    <div id="queryResp" style="height:85%;overflow-y: scroll">
        <table id="table"></table>
    </div>
</div>

<%--js引用--%>
<%@include file="../include/js-common.jsp"%>
<%@include file="../include/bootstrapTable.jsp"%>
<script src="/js/reimburse/queryReimburse.js" type="text/javascript"></script>
</body>
</html>
