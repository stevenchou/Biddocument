<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/10
  Time: 23:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>申请报销</title>
    <%@include file="../include/css-comnon.jsp"%>
</head>

<body>
<%@include file="../head.jsp" %>
<div id="main-content"  style="height:85%;overflow:scroll;">
    <div class="col-md-2"></div>
    <form role="form" class="col-md-6 " action="reimburse" method="POST"
          id="defaultForm">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4 >
                注意,
            </h4>
            <div id="reimburseMSG">
                您正在进行的操作将会提交一条报销信息到数据库！
            </div>
            <input id="responseMSG" type="text" hidden="hidden" value="${msg}"/>
        </div>

        <div class="form-group">
            <div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd"
                 data-link-field="dtp_generateDate" data-link-format="yyyy-mm-dd">
                <label for="dtp_generateDate" class="input-group-addon">费用产生时间</label>
                <input class="form-control" type="text" value="" readonly name="generateDate" id="generateDate">
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <input type="hidden" id="dtp_generateDate" value=""/><br/>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">报销类别</label>
            <div class="">
                <select class="form-control" name="category">
                    <option>办公费用</option>
                    <option>交通费用</option>
                    <option>餐饮酒店</option>
                    <option>邮寄费用</option>
                    <option>其他杂费</option>
                </select>
            </div>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">报销明细</label>
            <div>
                <input type="text" class=" form-control" name="detail" id="detail"/>
            </div>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">报销金额</label>
            <div>
                <input type="text" class=" form-control" name="amount" id="amount"/>
            </div>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">案  号</label>
            <div>
                <input type="text" class=" form-control" name="caseNum" id="caseNum"/>
            </div>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">案件名称</label>
            <div>
                <input type="text" class=" form-control" name="caseName" id="caseName"/>
            </div>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">承担人</label>
            <div class="">
                <select class="form-control" name="undertaker">
                    <option>联营所</option>
                    <option>深圳创客法律中心</option>
                </select>
            </div>
            <label class="input-group-addon">新增承担人</label>
        </div>

        <input type="submit" class="btn btn-info" value="报销"/>
    </form>
</div>

<%--//js引入--%>
<%@include file="../include/js-common.jsp"%>
<script type="text/javascript" src="/js/reimburse/reimburse.js"></script>
</body>
</html>
