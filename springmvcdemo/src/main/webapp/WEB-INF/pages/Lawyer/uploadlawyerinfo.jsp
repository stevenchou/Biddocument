<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/6/18
  Time: 2:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<html>
<head>
    <title>律师信息入库</title>
  <%@include file="../include/css-comnon.jsp"%>


</head>
<body>
<%@include file="../head.jsp" %>

<div id="main-content"  style="height:90%;overflow:scroll;">
    <div class="col-md-2"></div>
    <form role="form" class="col-md-6 " action="uploadlawyerinfo" method="POST" enctype="multipart/form-data"
          id="defaultForm">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                注意!
            </h4> 您正在进行的操作将会提交律师的个人信息到数据库！
        </div>
        <div class="form-group input-group">
            <label class="input-group-addon">律师姓名</label>
            <input type="text" class=" form-control" name="lawyername" datatype="s5-16" errormsg="昵称至少5个字符,最多16个字符！"/>
        </div>
        <div class="form-group input-group">
            <label class="input-group-addon">性    别</label>
            <div><input type="radio" name="gender" id="optionsRadios1" value="男">男</div>
            <div><input type="radio" name="gender" id="optionsRadios2" value="女">女</div>
        </div>
        <div class="form-group">
            <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                 data-link-field="dtp_birthday" data-link-format="yyyy-mm-dd">
                <label for="dtp_birthday" class="input-group-addon">出生日期</label>
                <input class="form-control" type="text" value="" readonly name="birthday" id="birthday">
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <input type="hidden" id="dtp_birthday" value=""/><br/>
        </div>


        <div class="form-group">
            <div class="input-group date form_date " data-date="" data-date-format="yyyy MM dd"
                 data-link-field="dtp_employeddate" data-link-format="yyyy-mm-dd">
                <label for="dtp_employeddate" class="input-group-addon">从业日期</label>
                <input class="form-control" size="16" type="text" value="" readonly name="employeddate"
                       id="employeddate">
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <input type="hidden" id="dtp_employeddate" value=""/><br/>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">律所职务</label>
            <div class="">
                <select class="form-control" name="position">
                    <option>管理合伙人律师</option>
                    <option>高级合伙人律师</option>
                    <option>合伙人律师</option>
                    <option>专职律师</option>
                    <option>律师助理</option>
                </select>
            </div>

        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">电话号码</label>
            <div id="telephonediv">
                <input type="text" class=" form-control" name="telephone" id="telephone"/>
            </div>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">电子邮箱</label>
            <div id="emaildiv">
                <input type="text" class=" form-control" name="email" id="email"/>
            </div>
        </div>

        <div class="form-group input-group " id="salteduGroup">
            <label class="input-group-addon">教育背景</label>
            <div class="saltIp">

                <input type="text" class="form-control inputGrpItem" id="eduinfolist" name="eduinfolist">
                <span class="input-group-btn ">
                    <%--<button class="btn btn-info " type="button" data-toggle="tooltip" title="delete"><span class="glyphicon glyphicon-minus "></span></button>--%>
                </span>
            </div>
            <button class="btn btn-info inputgroupadd" type="button" data-toggle="tooltip" title="新增"
                    onclick="inputgroupadd(this,'eduinfolist')"><span class="glyphicon glyphicon-plus"></span></button>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">工作经历</label>
            <div class=" saltIp">
                <input type="text" class="form-control  inputGrpItem" name="workiexplist">
                <span class="input-group-btn">
                    <%--<button class="btn btn-info " type="button" data-toggle="tooltip" title="delete"><span class="glyphicon glyphicon-minus"></span></button>--%>
                </span>
            </div>
            <button class="btn btn-info inputgroupadd" type="button" data-toggle="tooltip" title="新增"
                    onclick="inputgroupadd(this,'workiexplist')"><span class="glyphicon glyphicon-plus"></span></button>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">专业领域</label>
            <div>
                <input type="text" class="form-control col-md-10 inputGrpItem" name="professionalfieldlist">
            </div>
            <button class="btn btn-info inputgroupadd" type="button" data-toggle="tooltip" title="新增"
                    onclick="inputgroupadd(this,'professionalfieldlist')"><span class="glyphicon glyphicon-plus"></span>
            </button>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">身份证号</label>
            <input type="text" class=" form-control" name="IDcardno" id="IDcardno"/>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">身份证件</label>
            <input id="inputIDcard" name="inputIDcard" type="file"
                   class="file-loading uploadlawyerfile form-control"
                   accept=".jpg,.gif,.png,.jpeg,.bmp,.JPG,.GIF,.PNG,.JPEG,.BMP">
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">个人照片</label>
            <input id="inputpersonalimg" name="inputpersonalimg" type="file"
                   class="file-loading uploadlawyerfile form-control"
                   accept=".jpg,.gif,.png,.jpeg,.bmp,.JPG,.GIF,.PNG,.JPEG,.BMP">
        </div>
        <input type="submit" class="btn btn-info" value="提交上传"/>
    </form>
    <br>
    <br>
    <br>
</div>


<%--js引用--%>
<%@include file="../include/js-common.jsp"%>

<script type="text/javascript" charset="utf-8" src="/js/Lawyer/uploadlawyerinfo.js"></script>
<script type="text/javascript" charset="utf-8" src="/js/Lawyer/inputGroup.js"></script>
</body>
</html>
