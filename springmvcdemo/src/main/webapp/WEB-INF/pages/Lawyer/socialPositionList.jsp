<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/9
  Time: 22:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>社会职务列表</title>
</head>
<body>
    <%@include file="../head.jsp"%>

    <div id="main-content">
        <div>
            <table class="col-md-12 table">
                <thead>
                <tr>
                    <th>
                        律师姓名
                    </th>
                    <th>
                        社会职务
                    </th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${lawyerListHasSocialPosition}" var="lawyer">
                    <tr>
                        <td>${lawyer.lawyername}</td>
                        <%--<td>${lawyer.socialpositionlist}</td>--%>
                    <c:forEach items="${lawyer.socialpositionlist}" var="socialPosition">
                        <td>${socialPosition}</td>
                    </c:forEach>
                    </tr>
                </c:forEach>
                </tbody>

            </table>
        </div>
    </div>

</body>
</html>
