<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/6/23
  Time: 15:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>律师执业证管理</title>


    <%--引入日期时间选择插件--%>
    <link href="${pageContext.request.contextPath}/css/lib/datetimepicker/bootstrap-datetimepicker.min.css"
          rel="stylesheet" media="screen">
    <script type="text/javascript" src="/js/lib/datetimepicker/bootstrap-datetimepicker.js" charset="GB2312"></script>

    <%--引入上传文件插件--%>
    <link href="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/css/fileinput.css" media="all"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/explorer-fa/theme.css" media="all"
          rel="stylesheet" type="text/css"/>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/purify.js"
            type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/piexif.js"
            type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/sortable.js"
            type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/fileinput.js"
            type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/locales/fr.js"
            type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/locales/es.js"
            type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/explorer-fa/theme.js"
            type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/fa/theme.js"
            type="text/javascript"></script>

    <%--引入界面输入框验证插件--%>
    <link rel="stylesheet" href="/weblib/bootstrapValidator/dist/css/bootstrapValidator.min.css"/>
    <script type="text/javascript" src="/weblib/bootstrapValidator/dist/js/bootstrapValidator.js"></script>
    <script src="${pageContext.request.contextPath}/js/Lawyer/uploadcertificate.js" charset="GB2312"></script>
</head>
<body>

<%@include file="../head.jsp" %>
<div id="main-content">
    <div class="col-md-2"></div>
    <div class="col-md-6 column" style="height: 90%;overflow: scroll">
        <form:form role="form" action="/uploadcertificate" method="POST" enctype="multipart/form-data" id="defaultForm">
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>
                    注意!
                </h4> 您正在进行的操作将会提交律师的执业证到数据库！
            </div>
            <div class="form-group input-group">
                <label class="input-group-addon">选择律师</label>
                <div class="">
                    <select class="form-control" name="lawyername">
                        <c:forEach items="${lawyernames}" var="item">
                            <option>${item.lawyername}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <div class="input-group">
                    <label class="input-group-addon">执业证号</label>
                    <input type="text" class=" form-control" name="certificateID" id="certificateID"/>
                </div>
            </div>

            <div class="form-group">
                <div class="input-group date form_date " data-date="" data-date-format="yyyy" data-link-field="dtp_officehonnor" data-link-format="yyyy-mm-dd">
                    <label for="dtp_officehonnor" class="input-group-addon">执业时间</label>
                    <input class="form-control" size="16" type="text" value="" readonly name="certificatedate"
                           id="certificatedate">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <input type="hidden" id="dtp_officehonnor" value=""/><br/>
            </div>

            <div class="form-group ">
                <div class="input-group">
                    <label class="input-group-addon">执业证</label>
                    <input id="certificatecard" name="certificatecard" type="file"
                           class="file-loading uploadfile form-control"
                           accept=".jpg,.gif,.png,.jpeg,.bmp,.JPG,.GIF,.PNG,.JPEG,.BMP">
                </div>
            </div>

            <input type="submit" class="btn btn-info" value="提交上传"/>
        </form:form>
    </div>
</div>




<%--引入日期时间选择插件--%>
<script type="text/javascript" src="/js/lib/datetimepicker/bootstrap-datetimepicker.js" charset="GB2312"></script>

<%--引入上传文件插件--%>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/purify.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/piexif.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/sortable.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/fileinput.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/locales/fr.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/locales/es.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/explorer-fa/theme.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/fa/theme.js"
        type="text/javascript"></script>

<%--引入界面输入框验证插件--%>
<script type="text/javascript" src="/weblib/bootstrapValidator/dist/js/bootstrapValidator.js"></script>
<script src="${pageContext.request.contextPath}/js/Lawyer/uploadcertificate.js" charset="UTF-8"></script>

</body>
</html>
