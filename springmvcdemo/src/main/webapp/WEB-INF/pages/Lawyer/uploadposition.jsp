<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/6/23
  Time: 15:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>律师社会职务管理</title>
    <%--引入界面输入框验证插件--%>
    <link rel="stylesheet" href="/weblib/bootstrapValidator/dist/css/bootstrapValidator.min.css"/>

</head>
<body>
<%@include file="../head.jsp"%>
<div class="container">
    <div class="row clearfix">
        <div id="main-content">
            <div class="col-md-2"></div>
            <div class="col-md-8 column">
                <form:form role="form" action="/uploadposition" method="POST"  enctype="multipart/form-data" id="defaultForm">
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>
                        注意!
                        </h4> 您正在进行的操作将会提交律师的社会职务到数据库！
                    </div>

                    <div class="form-group input-group">
                        <label class="input-group-addon">选择律师</label>
                        <div class="">
                            <select class="form-control" id="lawyername" name="lawyername">
                                <c:forEach  items="${lawyerlist}" var="lawyer"  >
                                    <option >${lawyer.lawyername}</option>
                                </c:forEach>
                            </select>
                        </div>

                    </div>

                    <div class="form-group input-group " id="salteduGroup">
                        <label class="input-group-addon">社会职务</label>
                        <div class="saltIp">
                            <input type="text" class="form-control inputGrpItem" id="eduinfolist" name="socialPosition">
                            <span class="input-group-btn ">
                                    <%--<button class="btn btn-info " type="button" data-toggle="tooltip" title="delete"><span class="glyphicon glyphicon-minus "></span></button>--%>
                            </span>
                        </div>
                        <button class="btn btn-info inputgroupadd" type="button" data-toggle="tooltip" title="新增" id="addInput"
                                onclick="inputgroupadd(this,'socialPosition')"><span class="glyphicon glyphicon-plus"></span></button>
                    </div>

                    <input type="submit" class="btn btn-info" id="addSocialPosition" value="增加社会职务"/>
                </form:form>
            </div>
        </div>
    </div>
</div>
<%@include file="../include/js-common.jsp"%>
<script src="/js/Lawyer/uploadposition.js"></script>
</body>
</html>
