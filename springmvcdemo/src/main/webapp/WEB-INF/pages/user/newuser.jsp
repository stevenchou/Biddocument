<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/7/22
  Time: 17:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>新建用户</title>
    <%--引入界面输入框验证插件--%>
    <link rel="stylesheet" href="/weblib/bootstrapValidator/dist/css/bootstrapValidator.min.css"/>
</head>
<body>
<%@include file="../head.jsp"%>


<div id="main-content">
    <div class="col-md-2"></div>
    <form role="form" class="col-md-6" id="newuserform" action="/newuser" method="post"
          enctype="multipart/form-data">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                注意!
            </h4> 您正在进行的操作将会创建一个新用户,此用户会与一位律师唯一地关联！
        </div>

        <div class="form-group input-group">
        <label class="input-group-addon">用户名称</label>
        <input class="form-control" type="text" name="username"></input>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">用户密码</label>
            <input class="form-control" type="text" name="password"></input>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">选择律师</label>
            <div class="">
                <select class="form-control" name="lawyers">
                    <c:forEach items="${lawyers}" var="item">
                        <option>${item.lawyername}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <input type="submit" class="btn btn-info" value="创建用户"/>
    </form>
</div>
<%--引入界面输入框验证插件--%>
<script type="text/javascript" src="/weblib/bootstrapValidator/dist/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="/js/user/newuser.js"></script>
</body>
</html>
