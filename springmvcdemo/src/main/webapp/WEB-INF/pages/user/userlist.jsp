<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/7/22
  Time: 22:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户清单</title>
</head>
<body>
<%@include file="../head.jsp"%>

<div id="main-content">
    <div>
        <table class="col-md-12 table table-hover table-striped">
            <thead>
            <tr>
                <th>
                    编号(非执业证号)
                </th>
                <th>
                    姓名
                </th>
                <th>
                    律师姓名
                </th>
                <th>
                    角色
                </th>

            </tr>
            </thead>
            <tbody>
            <c:forEach items="${userList}" var="user">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.username}</td>
                    <td>${user.lawyer.lawyername}</td>
                    <td>${user.userRoles[0].role.name}</td>
                </tr>
            </c:forEach>
            </tbody>

        </table>
    </div>

</div>
</body>
</html>
