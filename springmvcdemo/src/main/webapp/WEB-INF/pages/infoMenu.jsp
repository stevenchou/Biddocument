<%@ page contentType="text/html;charset=UTF-8" language="java"  import="com.zhuozhengsoft.pageoffice.*"  %>
<%@ taglib uri="http://java.pageoffice.cn" prefix="po" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->


    <%--<script src="/js/jquery-1.9.1.min.js" type="text/javascript" charset="utf-8"></script>--%>
    <script src="/js/infoMenu.index.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <![endif]-->
    <script type="text/javascript">
        function Close() {
            window.external.close();
        }
        function increaseCount(value) {
            var sResult = window.external.CallParentFunc("updateCount("+value+");");

        }
        function increaseCountAndClose(value) {
            var sResult = window.external.CallParentFunc("updateCount("+value+");");
            window.external.close();
        }
    </script>
</head>
<body>
<h1>请选择要插入的信息</h1>

    <input type="button" value="设置父窗口Count的值加1" onclick="increaseCount(1);" />
    <input type="button" value="设置父窗口Count的值加5，并关闭窗口" onclick="increaseCountAndClose(5);" /></br>
    <div id="huaweiMenu">插入个人信息</div>
    <c:forEach items="${infoType}" var="type" varStatus="i">
        ${type}
    </c:forEach>


</body>
</html>