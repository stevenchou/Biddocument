<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/6/30
  Time: 14:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>新建项目</title>
    <%@include file="../include/css-comnon.jsp" %>
    <%@include file="../include/js-common.jsp" %>
</head>
<body>
<%@include file="../head.jsp" %>
<div id="main-content" style="height:90%;overflow:scroll;">
    <div class="col-md-2"></div>
    <form role="form" class="col-md-6" id="newprojectform" action="/newproject" method="post"
          enctype="multipart/form-data">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                注意!
            </h4> 您正在进行的操作将会提交项目信息到数据库！
        </div>

        <div class=" input-group form-group">
                        <span class="input-group-addon">
                            <label class="control-label">项目名称</label>
                        </span>
            <input type="text" class="form-control" name="projectname"></input>
        </div>


        <%--        <div class="form-group">--%>
        <%--            <div class="input-group ">--%>
        <%--                <select class="selectpicker form-control show-tick" multiple  data-live-search="true"--%>
        <%--                        id="allprojecttype" name="allprojecttype">--%>
        <%--                    <c:forEach items="${projecttypelist}" var="projecttype" varStatus="status">--%>
        <%--                        <option>${projecttype}</option>--%>
        <%--                    </c:forEach>--%>
        <%--                </select>--%>


        <%--                <span class="input-group-addon btn">--%>
        <%--                            <a href="#modal-container-newprojecttype" data-toggle="modal">新增项目类型</a>--%>
        <%--                        </span>--%>
        <%--                <div class="modal fade" id="modal-container-newprojecttype" role="dialog" aria-labelledby="myModalLabel"--%>
        <%--                     aria-hidden="true">--%>
        <%--                    <div class="modal-dialog">--%>
        <%--                        <div class="modal-content">--%>
        <%--                            <div class="modal-header">--%>
        <%--                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--%>
        <%--                                <h4 class="modal-title">--%>
        <%--                                    新增项目类型--%>
        <%--                                </h4>--%>
        <%--                            </div>--%>
        <%--                            <div class="modal-body">--%>
        <%--                                <label class="col-md-3 control-label">项目类型</label>--%>
        <%--                                <div class="input-group col-md-9">--%>
        <%--                                    <input type="text" class=" form-control" name="projettypename" id="projettypename"/>--%>
        <%--                                </div>--%>
        <%--                                <br/>--%>
        <%--                                <label class="col-md-3 control-label">类型说明</label>--%>
        <%--                                <div class="input-group col-md-9">--%>
        <%--                                <textarea role="3" class=" form-control " name="projecttypebrief"--%>
        <%--                                          id="projecttypebrief"></textarea>--%>
        <%--                                </div>--%>

        <%--                            </div>--%>
        <%--                            <div class="modal-footer">--%>
        <%--                                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>--%>
        <%--                                <button type="button" id="newprojecttype" class="btn btn-primary" data-dismiss="modal">--%>
        <%--                                    新增项目类型--%>
        <%--                                </button>--%>
        <%--                            </div>--%>
        <%--                        </div>--%>

        <%--                    </div>--%>

        <%--                </div>--%>
        <%--            </div>--%>
        <%--        </div>--%>

        <%--项目类型--%>
        <div class="form-group">
            <label class="col-md-12">项目类型(支持多选)</label>

            <c:forEach items="${projecttypelist}" var="projecttype">
                <div class="checkbox checkbox-success checkbox-inline">
                    <input type="checkbox" class="styled" value="${projecttype}" name="projecttype">
                    <label>${projecttype}</label>
                </div>
            </c:forEach>
        </div>
        <div class="input-group form-group">
                        <span class="input-group-addon">
                            <label class="control-label">项目简介</label>
                        </span>

            <textarea class="form-control" rows="3" name="projectbrief"></textarea>
        </div>

        <div class="input-group form-group">
                        <span class="input-group-addon">
                             <label class="control-label">客户姓名</label>
                        </span>
            <select class="form-control" id="allcustom" name="allcustom">
                <c:forEach items="${customslist}" var="item">
                    <option>${item.customname}</option>
                </c:forEach>
            </select>
            <div class="input-group-addon btn">
                <a href="/newcustom">新增客户</a>
            </div>
        </div>

        <%--合同签字律师--%>
        <div class="form-group">
            <label class="col-md-12">合同签字律师(代表人或委托人)</label>

            <c:forEach items="${signaturelawyers}" var="lawyer">
                <div class="checkbox checkbox-success checkbox-inline">
                    <input type="checkbox" class="styled" value="${lawyer.lawyername}" name="signaturelawyer">
                    <label>${lawyer.lawyername}</label>
                </div>
            </c:forEach>
        </div>

        <%--负责律师--%>
        <div class="form-group">
            <label class="col-md-12">项目负责律师（主办律师）</label>
            <c:forEach items="${responsiblelawyers}" var="lawyer">
                <div class="checkbox checkbox-success checkbox-inline">
                    <input type="checkbox" class="styled" value="${lawyer.lawyername}" name="responsiblelawyers"/>
                    <label>${lawyer.lawyername}</label>
                </div>
            </c:forEach>
        </div>

        <div class="form-group">
            <label class="col-md-12">团队成员(包含签字律师、负责律师)</label>
            <c:forEach items="${teamAlllawyers}" var="lawyer">
                <div class="checkbox checkbox-success checkbox-inline">
                    <input type="checkbox" class="styled" value="${lawyer.lawyername}" name="teamAlllawyers"/>
                    <label>${lawyer.lawyername}</label>
                </div>
            </c:forEach>
        </div>
        <div class="form-group ">
            <div class="input-group date form_date " data-date="" data-date-format="yyyy"
                 data-link-field="dtp_officehonnor" data-link-format="yyyy-mm-dd">
                            <span class="input-group-addon">
                            <label class="control-label">签约时间</label>
                            </span>
                <input class="form-control" size="16" type="text" value="" readonly name="signaturedate"
                       id="signaturedate">
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <input type="hidden" value=""/><br/>
        </div>

        <div class="form-group">
            <%--项目结束时间可以为空，为空就是至今--%>
            <div class="input-group date form_date " data-date="" data-date-format="yyyy"
                 data-link-field="dtp_officehonnor" data-link-format="yyyy-mm-dd">
                            <span class="input-group-addon">
                            <label class="control-label">届满时间</label>
                            </span>
                <input class="form-control" size="16" type="text" value="" readonly name="terminationdate"
                       id="terminationdate">
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <input type="hidden" value=""/><br/>
        </div>

        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                注意!
            </h4> 请上传完整合同，仅限以下后缀文件：.doc，.docx，.pdf！
        </div>
        <div class="form-group input-group">
                        <span class="input-group-addon">
                            <label class="control-label">完整合同</label>
                        </span>
            <input id="contractfile" name="contractfile" type="file" class="file-loading form-control uploadfile"
                   accept=".doc,.docx,.pdf"/>
        </div>


        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                注意!
            </h4> 请截取合同首页、服务团队页、签字页（如有报价，请将报价涂抹掉），仅限以下后缀文件：.jpg，.JPEG，.png，.PNG，.bmp，.BMP，.gif，.GIF！
        </div>
        <div class="form-group input-group">
            <label class="input-group-addon">合同截取</label>
            <div>
                <input type="file" class="file-loading uploadfile form-control" name="partcontract"
                       accept=".jpg,.gif,.png,.jpeg,.bmp,.JPG,.GIF,.PNG,.JPEG,.BMP">
            </div>
            <button class="btn btn-info inputgroupadd" type="button" data-toggle="tooltip" title="新增"
                    onclick="inputgroupadd(this,'partcontract ')"><span class="glyphicon glyphicon-plus"></span>
            </button>
        </div>

        <input type="submit" class="btn btn-primary" value="新建项目"/>
    </form>
</div>
<br>
<br>
<br>
<%--js引用--%>
<script src="/js/caseproject/newproject.js"></script>
</body>
</html>
