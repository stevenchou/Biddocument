<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/7/23
  Time: 22:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>项目详情</title>
</head>
<body>
<%@include file="../head.jsp" %>

<div id="main-content" style="height:100%;overflow: scroll">
    <div class="col-md-4 column" style="overflow-x:scroll;">
        <%@include file="filesystem.jsp"%>
    </div>
    <div class="col-md-8 column">
        <div id="projectName" >${projectdetail.casename}</div>
        <div >
            <h1>${projectdetail.casename}</h1>
        </div>

        <p>
            ${projectdetail.casebrief}
        </p>
        <p>
            <a class="btn btn-primary btn-large" href="#">Learn more</a>
        </p>

        <%--展示团队成员的头像，拖动文件直接到邮箱--%>
        <p2 class="text-center">
            团队成员
        </p2>
        <div class="row">
            <c:forEach items="${projectdetail.teamnumbers}" var="lawyer">
                <div class="col-md-2">
                    <a href="/emailto-${projectdetail.casename}" class="thumbnail getemail" alt="${lawyer.lawyername}"  title="${lawyer.email}">
                            <%--<img src="${myProject.evidencefilepath}">--%>
                        <img src="${lawyer.personalimgpath}" alt="${lawyer.lawyername}" >
                    </a>
                    <span>
                <a href="/emailto-${projectdetail.casename}" class="getemail"  alt="${lawyer.lawyername}" title="${lawyer.email}">
                        ${lawyer.lawyername}
                </a>
            </span>
                </div>
            </c:forEach>
        </div>
        <span></span>
        <%--展示客户的头像，拖动文件直接到邮箱--%>
        <p2 class="text-center">
            常联客户
        </p2>
            <div class="row">
                <c:forEach items="${customList}" var="custom">
                    <div class="col-md-2">
                        <a href="/emailto-${projectdetail.casename}" class="thumbnail getemail" alt="${custom.customname}" title="${custom.email}">
                                <%--<img src="${myProject.evidencefilepath}">--%>
                            <img src="${custom.imgpath}">
                        </a>
                        <span>
                    <a href="/emailto-${projectdetail.casename}" class="getemail" alt="${custom.customname}" title="${custom.email}">
                            ${custom.customname}
                    </a>
                </span>
                    </div>
                </c:forEach>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/caseproject/projectdetail.js"></script>
</body>
</html>
