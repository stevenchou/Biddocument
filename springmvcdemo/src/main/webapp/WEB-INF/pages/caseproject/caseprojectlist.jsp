<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/7/2
  Time: 2:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project List</title>
</head>
<body>
<%@include file="../include/css-comnon.jsp"%>
<%@include file="../include/js-common.jsp"%>
<%@include file="../head.jsp" %>
<div id="main-content">
    <table  id="projectlist"></table>
</div>


<script type="text/javascript" charset="UTF-8" src="/js/caseproject/caseprojectlist.js"></script>
</body>
</html>
