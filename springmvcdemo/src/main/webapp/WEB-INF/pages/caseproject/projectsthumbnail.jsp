<%@ page import="com.weijun.entity.CaseProject" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/7/22
  Time: 14:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="/css/projectsthumbnail.css" type="text/css" rel="stylesheet">
</head>
<body>
<%@include file="../include/js-common.jsp"%>
<%@include file="../head.jsp" %>

<div id="main-content">
    <h3 class="text-center text-success">
        我参与的项目
    </h3>
    <div class="row">
        <c:forEach items="${myProjectList}" var="myProject">
            <div class="col-md-3">
                <a href="/myprojects-${myProject.caseprojectid}-detail" class="thumbnail">
                    <%--<img src="${myProject.evidencefilepath}">--%>
                    <img src="${myProject.partcontractList[0]}">
                </a>
                <span>
                    <a href="/myprojects-${myProject.caseprojectid}-detail">
                            ${myProject.casename}
                    </a>
                </span>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>
