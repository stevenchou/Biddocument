<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/9
  Time: 21:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="/weblib/bootstrap-ztree/css/bootstrapStyle/bootstrapStyle.css" rel="stylesheet">
    <style type="text/css">
        .diyImg{
            height: 15px;
            width: 15px;
            margin-left: 3px;
            margin-right: 3px;
        }
        img:hover{
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="page-header">
        <h1>
            项目文件系统
        </h1>
    </div>
    <div class="zTreeDemoBackground left">
        <ul id="treeDemo" class="ztree"></ul>
    </div>
    <%--点击文件图标，弹出遮罩层，选择文件上传--%>
    <div class="modal fade" id="modal-container-720051" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title" id="myModalLabel">
                        上传文件到服务器
                    </h2>
                </div>
                <div class="modal-body">
                    <input type="file" id="uploadFileSelecter">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button> <button id="uploadFileToServer" type="button" class="btn btn-primary" data-dismiss="modal">上传</button>
                </div>
            </div>

        </div>

    </div>

    <script type="text/javascript" src="/weblib/bootstrap-ztree/js/jquery.ztree.core.js"></script>
    <script type="text/javascript" src="/weblib/bootstrap-ztree/js/jquery.ztree.excheck.js"></script>
    <script type="text/javascript" src="/weblib/bootstrap-ztree/js/jquery.ztree.exedit.js"></script>
    <script src="/js/commontool.js"></script>
    <script type="text/javascript" src="/js/caseproject/filesystem.js"></script>
</body>
</html>
