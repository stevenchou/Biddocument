<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <title>Welcome</title>
    <link rel="stylesheet" href="/weblib/tagbox/css/jquery.tagbox.css" />
    <script type="text/javascript" src="/weblib/tagbox/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/weblib/tagbox/js/jquery.tagbox.js"></script>
    <script type="text/javascript">
        jQuery(function() {
            jQuery("#jquery-tagbox-text").tagBox();
        });
    </script>
    <style type="text/css">
        body {
            font-size: 14px;
            font-family: Arial, Helvetica, sans-serif;
            margin: 20px;
        }
        div.row {
            padding: 10px;
        }

        div.row label {
            font-weight: bold;
            display: block;
            padding: 0px 0px 10px;
        }
    </style>
</head>
<body>
<form action="../jquery-tagbox/.">
    <div class="row">
        <label for="jquery-tagbox-text">文本方式</label>
        <input type="text" id="jquery-tagbox-text" />
    </div><!--div.row-->
</form>
</body>
</html>
