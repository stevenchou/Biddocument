<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/7/7
  Time: 10:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/weblib/bootstrap/bootstrap-3.3.7-dist/css/bootstrap.css"
    <%--Bootstrap单选按钮和复选框美化特效--%>
    <link rel="stylesheet" type="text/css" href="http://www.jq22.com/jquery/font-awesome.4.6.0.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/lib/Checkboxes/css/build.css">
    <%--引入日期时间选择插件--%>
    <link href="${pageContext.request.contextPath}/css/lib/datetimepicker/bootstrap-datetimepicker.min.css"
          rel="stylesheet" media="screen">

    <%--引入上传文件插件--%>
    <link href="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/css/fileinput.css" media="all"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/explorer-fa/theme.css" media="all"
          rel="stylesheet" type="text/css"/>
    <%--引入界面输入框验证插件--%>
    <link rel="stylesheet" href="/weblib/bootstrapValidator/dist/css/bootstrapValidator.min.css"/>
    <%--下拉框优化--%>
        <link rel="stylesheet" href="/weblib/bootstrap/select/bootstrap-select.min.css"/>
<%--    bootstrap-table--%>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.css">

</head>
<body>

</body>
</html>
