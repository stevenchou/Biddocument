<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/7/7
  Time: 10:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="/weblib/flatlab/js/jquery.js"></script>
    <%--The jQuery Form Plugin allows you to easily and unobtrusively upgrade HTML forms to use AJAX.--%>
    <script src="/weblib/jquery/jquery.form.js" ></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="/weblib/flatlab/js/bootstrap.js"></script>
    <%--引入日期时间选择插件--%>
    <script type="text/javascript" src="/js/lib/datetimepicker/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <%--引入上传文件插件--%>
    <script src="/weblib/bootstrap-fileinput/js/plugins/purify.js"
            type="text/javascript"></script>
    <script src="/weblib/bootstrap-fileinput/js/plugins/piexif.js"
            type="text/javascript"></script>
    <script src="/weblib/bootstrap-fileinput/js/plugins/sortable.js"
            type="text/javascript"></script>
    <script src="/weblib/bootstrap-fileinput/js/fileinput.js"
            type="text/javascript"></script>
    <script src="/weblib/bootstrap-fileinput/js/locales/fr.js"
            type="text/javascript"></script>
    <script src="/weblib/bootstrap-fileinput/js/locales/es.js"
            type="text/javascript"></script>
    <script src="/weblib/bootstrap-fileinput/themes/explorer-fa/theme.js"
            type="text/javascript"></script>
    <script src="/weblib/bootstrap-fileinput/themes/fa/theme.js"
            type="text/javascript"></script>
    <%--引入界面输入框验证插件--%>
    <script type="text/javascript" src="/weblib/bootstrapValidator/dist/js/bootstrapValidator.js"></script>

    <%--下拉框优化--%>
        <script type="text/javascript" src="/weblib/bootstrap/select/bootstrap-select.min.js"></script>

<%--    flatlab--%>
    <script src="/weblib/flatlab/js/jquery.dcjqaccordion.2.7.js" class="include" type="text/javascript"></script>
    <script src="/weblib/flatlab/js/jquery.scrollTo.min.js"></script>
    <script src="/weblib/flatlab/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="/weblib/flatlab/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="/weblib/flatlab/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="/weblib/flatlab/js/owl.carousel.js" ></script>
    <script src="/weblib/flatlab/js/jquery.customSelect.min.js" ></script>
    <script src="/weblib/flatlab/js/respond.min.js" ></script>
    <script src="/weblib/flatlab/js/sparkline-chart.js"></script>
    <script src="/weblib/flatlab/js/easy-pie-chart.js"></script>
    <script src="/weblib/flatlab/js/count.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
    <!-- Latest compiled and minified Locales -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
</head>
<body>

</body>
</html>
