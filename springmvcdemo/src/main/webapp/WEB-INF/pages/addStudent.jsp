<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Spring MVC表单之-输入框处理</title>
</head>
<body>

<h2>学生信息</h2>
<form:form method="POST" action="addPerson">
    <table>
        <tr>
            <td><form:label path="username">username：</form:label></td>
            <td><form:input path="username" /></td>
        </tr>
        <tr>
            <td><form:label path="address">address：</form:label></td>
            <td><form:input path="address" /></td>
        </tr>
        <tr>
            <td><form:label path="phone">phone：</form:label></td>
            <td><form:input path="phone" /></td>
        </tr>
        <tr>
            <td><form:label path="remark">remark：</form:label></td>
            <td><form:input path="remark" /></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="提交学生信息" /></td>
        </tr>
    </table>
</form:form>
</body>
</html>