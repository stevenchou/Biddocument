<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/23
  Time: 0:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>获取关联公司详细信息</title>
    <%@include file="../include/css-comnon.jsp"%>
</head>
<body>
<%@include file="../head.jsp" %>
<div id="main-content"  style="height:85%;overflow:scroll;">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <form role="form"  id="defaultForm" >
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4 >
                    注意,
                </h4>
                <div>
                    您正在进行的操作将会获取客户关联公司的详细信息！
                </div>
            </div>

            <div class="form-group input-group">
                <label class="input-group-addon">选择客户</label>
                <div class="">
                    <select class="form-control"  name="customName" id="customName">
                        <option>请选择一位客户</option>
                        <c:forEach  items="${personalCustomList}" var="personalCustom"  >
                            <option >${personalCustom.customname}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="form-group input-group" id="allCompanies">
                <label class="input-group-addon">公司名称</label>
                <div class="checkbox checkbox-success checkbox-inline">
                    <input type="checkbox" class="styled" id="selectAll"/>
                    <label>全选</label>
                </div>
                <br>
            </div>
            <input type="submit" class="btn btn-info" value="查询" id="queryBtn">
        </form>
        <div id="resultFromQCC">

        </div>
    </div>

</div>


<%--//js引入--%>
<%@include file="../include/js-common.jsp"%>
<script src="https://cdn.bootcss.com/jquery-json/2.6.0/jquery.json.min.js"></script>
<script type="text/javascript" src="/js/lib/jquery.wordexport/FileSaver.js"></script>
<script type="text/javascript" src="/js/lib/jquery.wordexport/jquery.wordexport.js"></script>
<script type="text/javascript" src="/js/capitalmarket/getRelativeCompaniesInfo.js"></script>
</body>
</html>
