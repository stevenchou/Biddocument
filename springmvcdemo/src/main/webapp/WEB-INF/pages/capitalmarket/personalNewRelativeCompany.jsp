<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/23
  Time: 0:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人用户新增关联公司</title>
    <%@include file="../include/css-comnon.jsp"%>
</head>
<body>
<%@include file="../head.jsp" %>
<div id="main-content"  style="height:85%;overflow:scroll;">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <form role="form"  id="defaultForm" >
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4 >
                    注意,
                </h4>
                <div id="reimburseMSG">
                    您正在进行的操作将会提交个人客户的关联公司信息到数据库！
                </div>
                <input id="responseMSG" type="text" hidden="hidden" value="${msg}"/>
            </div>

            <div class="form-group input-group">
                <label class="input-group-addon">选择客户</label>
                <div class="">
                    <select class="form-control"  name="customName">
                        <c:forEach  items="${personalCustomList}" var="personalCustom"  >
                            <option >${personalCustom.customname}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <div class="input-group">
                    <label class="input-group-addon">关联公司名称</label>
                    <div >
                        <input type="text" class=" form-control" name="relativeCompanyName" id="relativeCompanyName"/>
                    </div>
                </div>
            </div>


            <div class="form-group ">
                <div class="input-group">
                    <label class="input-group-addon">公司信用代码</label>
                    <div >
                        <input type="text" class=" form-control" name="relativeCompanyCreditCode" id="relativeCompanyCreditCode"/>
                    </div>
                </div>
            </div>


            <div class="form-group input-group">
                <label class="input-group-addon">担任职务</label>
                <div class="">
                    <select class="form-control" id="positionInRelativeCompany" name="positionInRelativeCompany">
                        <option >法人</option>
                        <option >股东</option>
                        <option >董事</option>
                        <option >监事</option>
                        <option >高管</option>
                    </select>
                </div>
            </div>
            <input type="submit" class="btn btn-info" value="提交" id="submitBtn">
        </form>
    </div>

</div>


<%--//js引入--%>
<%@include file="../include/js-common.jsp"%>
<script type="text/javascript" src="/js/capitalmarket/personalNewRelativeCompany.js"></script>
</body>
</html>
