<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/6/30
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- 可选的Bootstrap主题文件（一般不使用） -->
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"></script>
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script type="text/javascript" src="/weblib/jquery/jquery-3.4.1.js"></script>
    <script type="text/javascript" src="/weblib/bootstrap/bootstrap-3.3.7-dist/js/bootstrap.js"></script>

    <%--引入日期时间选择插件--%>
    <link href="${pageContext.request.contextPath}/css/lib/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <script type="text/javascript" src="/js/lib/datetimepicker/bootstrap-datetimepicker.js" charset="UTF-8"></script>

    <%--引入上传文件插件--%>
    <link href="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/purify.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/piexif.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/locales/fr.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/locales/es.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/explorer-fa/theme.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/fa/theme.js" type="text/javascript"></script>

    <%--引入界面输入框验证插件--%>
    <link rel="stylesheet" href="/weblib/bootstrapValidator/dist/css/bootstrapValidator.min.css"/>
    <script type="text/javascript" src="/weblib/bootstrapValidator/dist/js/bootstrapValidator.js"></script>

</head>
</html>
