<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/9
  Time: 10:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>新增客户</title>
</head>
<body>
    <%@include file="../head.jsp"%>

    <div id="main-content" style="height:90%;overflow:scroll;">
        <div class="col-md-12 column">
            <div class="tabbable" id="tabs-249479">
                <ul class="nav nav-tabs">
                    <li>
                        <a href="#panel-personalCustom" data-toggle="tab">新建个人客户</a>
                    </li>
                    <li class="active">
                        <a href="#panel-companyCustom" data-toggle="tab">新建公司客户</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="panel-personalCustom">
                        <p>
                            <%@include file="newPersonalCustom.jsp"%>
                        </p>
                    </div>
                    <div class="tab-pane active" id="panel-companyCustom">
                        <p>
                            <%@include file="newCompanyCustom.jsp"%>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
