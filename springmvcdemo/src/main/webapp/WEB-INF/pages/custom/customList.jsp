<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/4
  Time: 16:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.weijun.entity.HSCustom" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>客户列表</title>
    <%@include file="../include/css-comnon.jsp"%>
</head>
<body>
<%@include file="../head.jsp" %>

<%--获取客户图片，个人客户取头像，公司客户取公司Logo--%>

<div id="main-content">
    <h3 class="text-center text-success">
        客户列表
    </h3>
    <div class="row">
        <c:forEach items="${customList}" var="custom">
            <div class="col-md-3">
                <a href="/custom-${custom.id}-detail" class="thumbnail">
                    <img src="${custom.imgpath}">
                        <%--<img src="${myProject.partcontractList[0]}">--%>
                </a>
                <span class="text-center">
                    <a href="/custom-${custom.id}-detail">
                            ${custom.customname}
                    </a>
                </span>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>
