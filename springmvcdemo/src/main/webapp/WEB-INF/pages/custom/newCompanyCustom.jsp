<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/4
  Time: 0:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>公司客户信息入库</title>
    <%@include file="../include/css-comnon.jsp"%>
</head>
<body>

<div>
    <div class="col-md-2" ></div>
    <form role="form" class="col-md-6 " action="/newcustom" method="POST" enctype="multipart/form-data"
          id="newCustomForm">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                注意!
            </h4> 您正在进行的操作将会创建公司客户！
        </div>
        <div class="form-group input-group">
            <label class="input-group-addon">客户姓名</label>
            <input type="text" class=" form-control" name="customName" datatype="s5-16"/>
        </div>

        <div class="form-group">
            <div class="input-group">
                <label class="input-group-addon">客户简介</label>
                <textarea role="3" class=" form-control" name="customBrief" datatype="s5-16"></textarea>
            </div>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">电子邮箱</label>
            <input type="text" class=" form-control" name="customEmail" datatype="s5-16"/>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">联系地址</label>
            <input type="text" class=" form-control" name="customAddress" datatype="s5-16"/>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">联系人</label>
            <input type="text" class=" form-control" name="contactname" datatype="s5-16"/>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">联系人电话</label>
            <input type="text" class=" form-control" name="contactphone" datatype="s5-16"/>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">联系人性别</label>
            <div><input type="radio" name="contactGender" id="genderMale" value="男">男</div>
            <div><input type="radio" name="contactGender" id="genderFemale" value="女">女</div>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">公司Logo</label>
            <input id="customImg" name="companyLogo" type="file"
                   class="file-loading uploadfile form-control">
        </div>
        <input type="submit" class="btn btn-info" value="新建客户"/>
    </form>
</div>

<%--js在页面及css加载完成后加载--%>
<%--js引用--%>
<%@include file="../include/js-common.jsp"%>
<script type="text/javascript" src="/js/custom/newCompanyCustom.js"></script>
</body>
</html>
