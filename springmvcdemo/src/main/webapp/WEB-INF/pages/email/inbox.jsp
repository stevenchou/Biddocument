<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/5
  Time: 12:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<div class="inbox-head">
    <h3>收件箱</h3>
    <form class="pull-right position" action="#">
        <div class="input-append">
            <input type="text"  placeholder="Search Mail" class="sr-input">
            <button type="button" class="btn sr-btn"><i class="icon-search"></i></button>
        </div>
    </form>
</div>
<div class="inbox-body">
    <div class="mail-option">
        <div class="chk-all">
            <input type="checkbox" class="mail-checkbox mail-group-checkbox">
            <div class="btn-group" >
                <a class="btn mini all" href="#" data-toggle="dropdown">
                    All
                    <i class="icon-angle-down "></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#"> None</a></li>
                    <li><a href="#"> Read</a></li>
                    <li><a href="#"> Unread</a></li>
                </ul>
            </div>
        </div>

        <div class="btn-group">
            <a class="btn mini tooltips" href="#" data-toggle="dropdown" data-placement="top" data-original-title="Refresh">
                <i class=" icon-refresh"></i>
            </a>
        </div>
        <div class="btn-group hidden-phone">
            <a class="btn mini blue" href="#" data-toggle="dropdown">
                More
                <i class="icon-angle-down "></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#"><i class="icon-pencil"></i> Mark as Read</a></li>
                <li><a href="#"><i class="icon-ban-circle"></i> Spam</a></li>
                <li class="divider"></li>
                <li><a href="#"><i class="icon-trash"></i> Delete</a></li>
            </ul>
        </div>
        <div class="btn-group">
            <a class="btn mini blue" href="#" data-toggle="dropdown">
                Move to
                <i class="icon-angle-down "></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#"><i class="icon-pencil"></i> Mark as Read</a></li>
                <li><a href="#"><i class="icon-ban-circle"></i> Spam</a></li>
                <li class="divider"></li>
                <li><a href="#"><i class="icon-trash"></i> Delete</a></li>
            </ul>
        </div>

        <ul class="unstyled inbox-pagination">
            <li><span>1-50 of 234</span></li>
            <li>
                <a href="#" class="np-btn"><i class="icon-angle-left  pagination-left"></i></a>
            </li>
            <li>
                <a href="#" class="np-btn"><i class="icon-angle-right pagination-right"></i></a>
            </li>
        </ul>
    </div>
    <table class="table table-inbox table-hover">
        <tbody>
        <tr class="unread">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message  dont-show">Vector Lab</td>
            <td class="view-message ">Lorem ipsum dolor imit set.</td>
            <td class="view-message  inbox-small-cells"><i class="icon-paper-clip"></i></td>
            <td class="view-message  text-right">9:27 AM</td>
        </tr>
        <tr class="unread">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Mosaddek Hossain</td>
            <td class="view-message">Hi Bro, How are you?</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">March 15</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Dulal khan</td>
            <td class="view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">June 15</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Facebook</td>
            <td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">April 01</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star inbox-started"></i></td>
            <td class="view-message dont-show">Mosaddek <span class="label label-danger pull-right">urgent</span></td>
            <td class="view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">May 23</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star inbox-started"></i></td>
            <td class="view-message dont-show">Facebook</td>
            <td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
            <td class="view-message inbox-small-cells"><i class="icon-paper-clip"></i></td>
            <td class="view-message text-right">March 14</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star inbox-started"></i></td>
            <td class="view-message dont-show">Rafiq</td>
            <td class="view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"><i class="icon-paper-clip"></i></td>
            <td class="view-message text-right">January 19</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Facebook <span class="label label-success pull-right">megazine</span></td>
            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">March 04</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Mosaddek</td>
            <td class="view-message view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">June 13</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Facebook <span class="label label-info pull-right">family</span></td>
            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">March 24</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star inbox-started"></i></td>
            <td class="view-message dont-show">Mosaddek</td>
            <td class="view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">March 09</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star inbox-started"></i></td>
            <td class="dont-show">Facebook</td>
            <td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
            <td class="view-message inbox-small-cells"><i class="icon-paper-clip"></i></td>
            <td class="view-message text-right">May 14</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Sumon</td>
            <td class="view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"><i class="icon-paper-clip"></i></td>
            <td class="view-message text-right">February 25</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="dont-show">Facebook</td>
            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">March 14</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Dulal</td>
            <td class="view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">April 07</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Twitter</td>
            <td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">July 14</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star inbox-started"></i></td>
            <td class="view-message dont-show">Sumon</td>
            <td class="view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">August 10</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Facebook</td>
            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
            <td class="view-message inbox-small-cells"><i class="icon-paper-clip"></i></td>
            <td class="view-message text-right">April 14</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Mosaddek</td>
            <td class="view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"><i class="icon-paper-clip"></i></td>
            <td class="view-message text-right">June 16</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star inbox-started"></i></td>
            <td class="view-message dont-show">Sumon</td>
            <td class="view-message">Lorem ipsum dolor sit amet</td>
            <td class="view-message inbox-small-cells"></td>
            <td class="view-message text-right">August 10</td>
        </tr>
        <tr class="">
            <td class="inbox-small-cells">
                <input type="checkbox" class="mail-checkbox">
            </td>
            <td class="inbox-small-cells"><i class="icon-star"></i></td>
            <td class="view-message dont-show">Facebook</td>
            <td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
            <td class="view-message inbox-small-cells"><i class="icon-paper-clip"></i></td>
            <td class="view-message text-right">April 14</td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
