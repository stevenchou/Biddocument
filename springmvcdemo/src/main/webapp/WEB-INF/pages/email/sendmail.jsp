<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java"
    import="com.weijun.entity.Lawyer"
%>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/5
  Time: 13:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

</head>
<body>
<section class="panel">
    <div class="inbox-head">
        <h3>发件箱</h3>
    </div>

    <%--//发件箱中间正文部分--%>
    <div class="col-md-10 inbox-body" id="dragTarget">
        <form class="form-horizontal tasi-form" >
            <div class="form-group">
                <label class="col-md-2 control-label">收件人</label>
                <div class="col-md-10 easyui-panel" >
                    <input id="receiver" type="text" class="form-control" name="receiver">
                </div>
            </div>
            <div class="form-group" id="mailCopyTo">
                <label class="col-md-2  control-label">抄送</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="copyToInput" name="copyToInput">
                </div>
            </div>

            <div class="form-group" id="mailSecurityTo">
                <label class="col-md-2 control-label">密送</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" id="SecurityToInput" name="SecurityToInput" >
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">主题</label>
                <div class="col-md-10">
                    <input type="text" id="theme" class="form-control" name="theme">
                </div>
            </div>

            <div class="form-group">
                <%--<span class="col-sm-1"></span>--%>
                <button type="button" class="btn btn-primary col-md-2" id="btnCopyTo">添加抄送</button>
                <button type="button" class="btn btn-primary col-md-2" id="btnSecurityTo">添加密送</button>
                <button type="button" class="btn btn-primary col-md-2" id="btnToEveryone">分别发送</button>
            </div>

            <%--附件添加在这里--%>
            <div id="mailattach"></div>

            <div class="form-group" >
                <label class="col-md-1 control-label">正文</label>
                <div contenteditable="true" id="mailContent" class="col-md-10 sendMailContent" style="margin-left: 5%">
                    <br>
                    <br>
                    <br>
                    <br>
                    ${greetings}
                </div>
            </div>

            <div class="form-group">
                <span class="col-md-1"></span>
                <%--<input type="submit" value="发送" class="btn btn-primary col-md-2">--%>
                <button type="button" id="sendMail" class="btn btn-primary col-md-2">发送</button>
                <button type="button" id="saveDraft" class="btn btn-primary col-md-2">存草稿</button>
                <button type="button" id="cloasesendMail" class="btn btn-primary col-md-2">关闭</button>
            </div>
        </form>
    </div>


    <%--右侧联系人--%>

    <div class="col-md-2">
        <div id="teamNumbers">
            <p2>团队成员</p2>
                <div class="row">
                    <c:forEach items="${teamNumbers}" var="lawyer">
                        <div class="col-md-6 thumbnail">
                            <img class="receiver" src="${lawyer.IDcardpath}" alt="${lawyer.lawyername}" title="${lawyer.email}">
                            <span>
                                    ${lawyer.lawyername}
                            </span>
                        </div>
                    </c:forEach>
                    <div class="col-md-6 thumbnail">
                        <img src="/image/more.png" alt="律师列表" title="律师列表">
                        <span>
                            律师列表
                        </span>
                    </div>
            </div>
        </div>
        <div id="frequentCustoms">
            <p2>常联客户</p2>
                <div class="row">
                    <c:forEach items="${customList}" var="custom">
                        <div class="col-md-6 thumbnail">
                            <img class="receiver" src="${custom.imgpath}" alt="${custom.customname}" title="${custom.email}">
                            <span>
                                ${custom.customname}
                            </span>
                        </div>
                    </c:forEach>
                    <div class="col-md-6 thumbnail" style="horiz-align: left">
                        <img src="/image/more.png" alt="客户列表" title="客户列表">
                        <span>
                            客户列表
                        </span>
                    </div>
                </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="/weblib/flatlab/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/js/email/sendmail.js"></script>
</body>
</html>
