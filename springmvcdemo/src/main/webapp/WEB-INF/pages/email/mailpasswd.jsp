<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/9/6
  Time: 20:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>配置邮箱密码</title>
</head>
<body>
<%@include file="../head.jsp"%>

<div id="main-content">
    <div class="col-md-2"></div>
    <form role="form" class="col-md-6" id="mailpass" action="/mailpasswd" method="post">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                注意!
            </h4> 您正在进行的操作将会配置律师邮箱的账号！
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">选择律师</label>
            <div class="">
                <select class="form-control" name="lawyername" id="lawyername">
                    <c:forEach items="${lawyerlist}" var="item">
                        <option>${item.lawyername}</option>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">邮箱</label>
            <input class="form-control" type="text" id="lawyerEmail" name="lawyerEmail" readonly="readonly">
        </div>

        <div class="form-group input-group">
            <label class="input-group-addon">密码</label>
            <input class="form-control" type="text" value="" id="emailpasswd" name="emailpasswd">
        </div>
        <input type="submit" class="btn btn-info" value="配置邮箱密码"/>
    </form>
</div>

<script src="/js/email/mailpasswd.js"></script>
</body>
</html>
