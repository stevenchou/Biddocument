<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/9
  Time: 21:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="/weblib/bootstrap-ztree/css/bootstrapStyle/bootstrapStyle.css" rel="stylesheet">
    <style type="text/css">
        .diyImg{
            height: 15px;
            width: 15px;
            margin-left: 3px;
            margin-right: 3px;
        }
        img:hover{
            cursor: pointer;
        }
    </style>
</head>
<body>
<div class="page-header">
    <h1>
        项目文件系统
    </h1>
</div>
<div class="zTreeDemoBackground left">
    <ul id="treeDemo" class="ztree"></ul>
</div>
<script type="text/javascript" src="/weblib/bootstrap-ztree/js/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/weblib/bootstrap-ztree/js/jquery.ztree.excheck.js"></script>
<script type="text/javascript" src="/weblib/bootstrap-ztree/js/jquery.ztree.exedit.js"></script>
<script src="/js/commontool.js"></script>
<script type="text/javascript" src="/js/email/emailfilesystem.js"></script>
</body>
</html>
