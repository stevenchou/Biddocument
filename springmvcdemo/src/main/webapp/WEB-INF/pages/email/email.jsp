<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>${currentUser.lawyer.lawyername}的邮箱</title>

    <link href="/weblib/flatlab/assets/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css" >
    <link href="css/email.css" rel="stylesheet">
</head>

<body >
<section id="container" class="" style="height: 100%;min-height: 100%;_height: 100%;">
    <!--header start-->
    <%@include file="../head.jsp"%>

    <!--main content start-->
    <section id="main-content" >
            <!--mail inbox start-->
        <%--style="margin-top: -10px"用来解决mail-box与头部之间空白问题--%>
            <div class="mail-box" style="margin-top: -10px">
                <aside class="col-md-2">
                    <div class="user-head">
                        <a href="javascript:;" class="inbox-avatar" >
                            <img style="height: 60px;width: 60px" src="${currentUser.lawyer.personalimgpath}" alt="">
                        </a>
                        <div>
                            <h5>${currentUser.lawyer.lawyername}</h5>
                            <h5>${currentUser.lawyer.email}</h5>
                        </div>
                    </div>
                    <ul class="inbox-nav inbox-divider tabbable">
                        <li >
                            <a href="#panel-inbox" id="href-inbox" data-toggle="tab"><i class="icon-inbox"></i> 收件箱 <span class="label label-danger pull-right">2</span></a>

                        </li>
                        <li id="li-sendMail">
                            <a href="#panel-sendMail" id="href-sendMail" data-toggle="tab"><i class="icon-envelope-alt"></i> 发件箱</a>
                        </li>
                        <li>
                            <a href="#"><i class="icon-bookmark-empty"></i> Important</a>
                        </li>
                        <li>
                            <a href="#"><i class=" icon-external-link"></i> Drafts <span class="label label-info pull-right">30</span></a>
                        </li>
                        <li>
                            <a href="#"><i class=" icon-trash"></i> Trash</a>
                        </li>
                    </ul>

                    <div id="email_filesystem" style="overflow-x:scroll;">
                        <%@include file="../email/emailfilesystem.jsp"%>
                    </div>

                </aside>
                <aside class="col-md-8">
                    <div class="tab-content">
                        <div class="tab-pane active" id="panel-inbox">
                            <%@include file="inbox.jsp"%>
                        </div>
                        <div class="tab-pane " id="panel-sendMail">
                            <%@include file="sendmail.jsp"%>
                        </div>
                    </div>
                </aside>
            </div>
            <!--mail inbox end-->
        

        </section>

    <%--footer--%>
    <br><br>
    <%@include file="../include/footer-common.jsp"%>
    <!--main content end-->
</section>


<!-- js placed at the end of the document so the pages load faster -->
<script src="/weblib/flatlab/js/respond.min.js" ></script>

<!-- BEGIN:File Upload Plugin JS files-->
<script src="/weblib/flatlab/assets/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="/weblib/flatlab/assets/jquery-file-upload/js/vendor/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="/weblib/flatlab/assets/jquery-file-upload/js/vendor/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="/weblib/flatlab/assets/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/weblib/flatlab/assets/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/weblib/flatlab/assets/jquery-file-upload/js/jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="/weblib/flatlab/assets/jquery-file-upload/js/jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/weblib/flatlab/assets/jquery-file-upload/js/jquery.fileupload-ui.js"></script>

</body>
</html>
