<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/6/19
  Time: 23:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Upload the office honor to server</title>
    <%--引入日期时间选择插件--%>
    <link href="${pageContext.request.contextPath}/css/lib/datetimepicker/bootstrap-datetimepicker.min.css"
          rel="stylesheet" media="screen">
    <%--引入上传文件插件--%>
    <link href="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/css/fileinput.css" media="all"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/explorer-fa/theme.css" media="all"
          rel="stylesheet" type="text/css"/>
    <%--引入界面输入框验证插件--%>
    <link rel="stylesheet" href="/weblib/bootstrapValidator/dist/css/bootstrapValidator.min.css"/>


</head>
<body>
<%@include file="../head.jsp" %>

<div id="main-content" style="height: 90%;overflow-y: scroll">
    <div class="col-md-2"></div>
    <form role="form" class="col-md-6" id="officehonorform" action="/uploadofficehonor" method="post"
          enctype="multipart/form-data" >
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                注意!
            </h4> 您正在进行的操作将会提交律所每年度获取的荣誉到数据库！
        </div>
        <div class="form-group" >
            <div class="input-group date form_date " data-date="" data-date-format="yyyy"
                 data-link-field="dtp_officehonnor" data-link-format="yyyy-mm-dd">
                <label for="dtp_officehonnor" class="input-group-addon">荣誉年份</label>
                <input class="form-control" size="16" type="text" value="" readonly name="honoryear" id="honoryear">
                <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
            <input type="hidden" id="dtp_officehonnor" value=""/><br/>
        </div>
        <div class="form-group input-group">
            <label class="input-group-addon">荣誉内容</label>
            <textarea class="form-control" rows="3" name="honorcontent"></textarea>
        </div>
        <div class="form-group input-group">
            <label class="input-group-addon">证明文件</label>
            <input id="inputpersonalimg" name="inputhonorfile" type="file"
                   class="file-loading uploadfile form-control"
                   accept=".jpg,.gif,.png,.jpeg,.bmp,.JPG,.GIF,.PNG,.JPEG,.BMP">
        </div>
        <input type="submit" class="btn btn-info" value="上传荣誉"/>
    </form>
</div>

</div>

<%--js引用--%>
<%--引入日期时间选择插件--%>
<script type="text/javascript" src="/js/lib/datetimepicker/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<%--引入上传文件插件--%>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/purify.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/piexif.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/plugins/sortable.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/fileinput.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/locales/fr.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/js/locales/es.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/explorer-fa/theme.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/weblib/bootstrap-fileinput/themes/fa/theme.js"
        type="text/javascript"></script>
<%--引入界面输入框验证插件--%>
<script type="text/javascript" src="/weblib/bootstrapValidator/dist/js/bootstrapValidator.js"></script>

<script src="/js/office/uploadofficehonor.js"></script>
</body>
</html>
