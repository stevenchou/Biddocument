<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/6/19
  Time: 0:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Show all the lawyer</title>
</head>
<body>
<%@include file="../head.jsp"%>
<div id="main-content">
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>
                    编号
                </th>
                <th>
                    荣誉获取年度
                </th>
                <th>
                    荣誉内容
                </th>
                <th>
                    更细
                </th>
                <th>
                    删除
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${officeHonors}" var="officeHonor">
                <tr>
                    <td>${officeHonor.id}</td>
                    <td>${officeHonor.honoryear}</td>
                    <td>${officeHonor.honorcontent}</td>
                    <td><a href="<c:url value='/edit-${officeHonor.id}-honor' />">update</a></td>
                    <td><a href="<c:url value='/delete-${officeHonor.id}-honor' />">delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        </div>
</body>
</html>
