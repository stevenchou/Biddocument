<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>打开标书文件</title>

</head>
<body>

<%@include file="../head.jsp" %>
<%@include file="../include/css-comnon.jsp"%>
<div id="main-content">
    <div class="col-md-2 column"></div>

    <div class="col-md-6 column">
        <form:form role="form" action="uploadBidDocu" method="POST" enctype="multipart/form-data" id="defaultForm">
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                注意!
            </h4> 您正在进行的操作将会打开一个本地word文件，并将文件上传到服务器！
        </div>
        <div class="input-group form-group">
                            <span class="input-group-addon">
                                <label>项目名称</label>
                            </span>
            <select class="selectpicker form-control show-tick" data-live-search="true" id="projectnames"
                    name="projectnames">
                <c:forEach items="${projects}" var="project" varStatus="status">
                    <option>${project.casename}</option>
                </c:forEach>
            </select>
            <span class="input-group-addon">
                <label id="jump_newproject"><a href="/newproject">新增项目</a></label>
            </span>
        </div>

            <%--文件属性--%>
        <div class="input-group form-group">
                    <span class="input-group-addon">
                         <label class="control-label">文件属性</label>
                    </span>
            <select class="form-control" id="allattribute" name="allattribute">
                <c:forEach items="${fileAttributeTypes}" var="item">
                    <option>${item.attributename}</option>
                </c:forEach>
            </select>
            <span class="input-group-addon btn">
                        <a href="#modal-container-newfileattribute" data-toggle="modal">新增属性</a>
                    </span>
            <div class="modal fade" id="modal-container-newfileattribute" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">
                                添加文件属性
                            </h4>
                        </div>
                        <div class="modal-body">
                            <label class="col-md-3 control-label">文件属性</label>
                            <div class="input-group col-md-9">
                                <input type="text" class=" form-control" name="fileattribute" id="fileattribute"/>
                            </div>
                            <br/>
                            <label class="col-md-3 control-label">属性简介</label>
                            <div class="input-group col-md-9">
                            <textarea role="3" class=" form-control " name="fileattributebrief"
                                      id="fileattributebrief"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                            <button type="button" id="newfileattribute" class="btn btn-primary" data-dismiss="modal">新建文件属性
                            </button>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
                <label class="input-group-addon control-label">要打开的标书文件</label>
                <input id="biddocument" name="biddocument" type="file" class="file-loading uploadfile form-control">
            </div>

        </div>

        <input type="submit" class="btn btn-default" value="提交上传"/>
    </div>
    </form:form>
</div>


<%@include file="../include/js-common.jsp"%>
<script src="/js/biddocument/uploadbiddocu.js"></script>
</body>
</html>