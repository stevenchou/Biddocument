<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java"
         import="com.zhuozhengsoft.pageoffice.PageOfficeCtrl,com.zhuozhengsoft.pageoffice.wordwriter.WordDocument"
         pageEncoding="UTF-8" %>

<%--<%--%>
<%--    PageOfficeCtrl poCtrl = (PageOfficeCtrl) request.getAttribute("poCtrl");--%>
<%--    poCtrl.setServerPage(request.getContextPath() + "/poserver.zz"); //此行必须--%>
<%--    WordDocument doc = new WordDocument();--%>
<%--    poCtrl.setWriter(doc);--%>
<%--    poCtrl.setTitlebar(false); //隐藏标题栏--%>
<%--    poCtrl.setMenubar(false); //隐藏菜单栏--%>
<%--    poCtrl.setOfficeToolbars(false);//隐藏Office工具条--%>
<%--    poCtrl.setCustomToolbar(false);//隐藏自定义工具栏--%>

<%--%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="this is my page">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <%--<script src="/js/jquery-1.9.1.js" type="text/javascript" charset="utf-8"></script>--%>
    <link href="/css/loadpageoffice.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="pageoffice.js" id="po_js_main"></script>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>打开标书文件</title>
    <%@include file="../include/css-comnon.jsp"%>

</head>
<body menubarbar=no,toolbar=no>
<%--//顶部--%>
<a id="pagetop" href="#top"></a>
<div class="container" is="container">
    <div class="row clearfix">
        <div class="col-md-2">
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Word工具，选择数据库中信息，添加到文章中
            </div>
            <div class="panel-group" id="panel-bidDocu">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-bidDocu"
                           href="#panel-element-lawyer">律师信息</a>
                    </div>
                    <div id="panel-element-lawyer" class="panel-collapse collapse">
                        <div class="panel-body cursorlike">
                            个人信息
                        </div>
                        <div class="panel-body cursorlike">
                            团队信息
                        </div>
                        <%--<div class="panel-body">--%>
                        <%--Anim pariatur cliche...--%>
                        <%--</div>--%>
                        <div>
                            <a href="javascript:POBrowser.openWindowModeless('POBrowser.jsp','width=1200px;height=800px;');">open the file</a>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-bidDocu"
                           href="#panel-element-project">案例信息</a>
                    </div>
                    <div id="panel-element-project" class="panel-collapse collapse">
                        <div class="panel-body cursorlike" id="allProjectInfo">
                            案例信息
                        </div>
                        <%--<div class="panel-body">--%>
                        <%--Anim pariatur cliche...--%>
                        <%--</div>--%>

                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="panel-title collapsed" data-toggle="collapse" data-parent="#panel-bidDocu"
                           href="#panel-element-office">律所信息</a>
                    </div>
                    <div id="panel-element-office" class="panel-collapse collapse">
                        <div class="panel-body cursorlike">
                            Anim pariatur cliche...
                        </div>
                        <div class="panel-body cursorlike">
                            Anim pariatur cliche...
                        </div>
                        <div class="panel-body">
                            Anim pariatur cliche...
                        </div>

                    </div>
                </div>
            </div>
            <%--<button type="button" id="pading">填充</button>--%>
        </div>

<%--        <div class="col-md-10 ">--%>
<%--            <div id="PO_Content" style="width:100%; height:96%;position:relative;z-index:-1;">--%>
<%--                <%=poCtrl.getHtmlCode("PageOfficeCtrl1")%>--%>
<%--            </div>--%>
<%--            <div class="activiheight"></div>--%>
<%--        </div>--%>

        <section>
            <div id="mask" class="mask">
                <div id="shadercontent" class="col-md-12 pos" >
                    <div class="col-md-2"></div>
                    <div class="col-md-8" >
                        <div class="panel panel-default">
                            <div class="panel-body" style="height:500px;overflow: scroll">
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="false"></button>
                                    案例信息筛选，请按照下列条件筛选。
                                </div>
                                <%--根据年份选择项目--%>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group date form_date" data-date="" data-date-format="dd MM yyyy"
                                             data-link-field="dtp_projectBeginDate" data-link-format="yyyy-mm-dd">
                                            <label for="dtp_projectBeginDate" class="input-group-addon">开始日期</label>
                                            <input class="form-control" type="text" value="" readonly name="projectBeginDate" id="projectBeginDate">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        <input type="hidden" id="dtp_projectBeginDate" value=""/><br/>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group date form_date " data-date=""  data-link-field="dtp_projectEndDate" data-link-format="yyyy-mm-dd">
                                            <label for="dtp_projectEndDate" class="input-group-addon">结束时间</label>
                                            <input class="form-control" size="16" type="text" value="" readonly name="projectEndDate"
                                                   id="projectEndDate">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        <input type="hidden" id="dtp_projectEndDate" value=""/><br/>
                                    </div>
                                </div>
                                <%--根据类型选择项目--%>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="input-group ">
                                            <span class="input-group-addon">
                                                <label class="control-label">请选择类型</label>
                                            </span>
                                            <select class="selectpicker form-control show-tick col-md-12" multiple  data-live-search="true"
                                                    id="selectbytype" name="selectbytype">
                                                <c:forEach items="${projecttypelist}" var="projecttype" varStatus="status">
                                                    <option>${projecttype}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button id="searchprojects" class="btn btn-info col-md-12">搜索项目</button>
                                </div>
                                <div class="col-md-12">
                                <table  id="selectedprojects"></table>
                                </div>
                                <div class="col-md-12">
                                    <button id="cloaseshader" class="btn">close</button>
                                    <button id="paddingprojects" class="btn">填充数据到文档</button>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>

<%@include file="../include/js-common.jsp"%>
<%@include file="../include/bootstrapTable.jsp"%>
<script type="text/javascript" src="/js/biddocument/loadpageoffice.js"></script>
</body>
</html>
