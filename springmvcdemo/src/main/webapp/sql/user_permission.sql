/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.20 : Database - hslll
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE `hslll`;



DROP TABLE IF EXISTS `t_permission`;

CREATE TABLE `t_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2vqgixlxtue6gkueht27bofnq` (`roleId`),
  CONSTRAINT `FK_2vqgixlxtue6gkueht27bofnq` FOREIGN KEY (`roleId`) REFERENCES `t_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `t_permission` */

insert  into `t_permission`(`id`,`description`,`token`,`url`,`roleId`) values (1,'login',NULL,'/login',2),(2,'home',NULL,'/home',1),(3,'login',NULL,'/login',1),(4,'home',NULL,'/home',2),(5,'newcustom',NULL,'/newcustom',1),(6,'newprojecttype',NULL,'/newprojecttype',2);



/*Table structure for table `t_role` */

DROP TABLE IF EXISTS `t_role`;

CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `t_role` */

insert  into `t_role`(`id`,`description`,`name`) values (1,'administrator','admin'),(2,'lawyer','lawyer');



/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdate` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `lawyer` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7m5txn63md2keuyg3328c21fe` (`lawyer`),
  CONSTRAINT `FK_7m5txn63md2keuyg3328c21fe` FOREIGN KEY (`lawyer`) REFERENCES `t_lawyer` (`lawyerid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`createdate`,`password`,`username`,`lawyer`) values (1,'2018-07-22 16:25:36','admin','admin',NULL);

/*Table structure for table `t_user_role` */

DROP TABLE IF EXISTS `t_user_role`;

CREATE TABLE `t_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_kjp9c6hki8a1p70x44bwqex2v` (`roleId`),
  UNIQUE KEY `UK_akj61lp0wul5h73yq0xrq89cq` (`userId`),
  CONSTRAINT `FK_akj61lp0wul5h73yq0xrq89cq` FOREIGN KEY (`userId`) REFERENCES `t_user` (`id`),
  CONSTRAINT `FK_kjp9c6hki8a1p70x44bwqex2v` FOREIGN KEY (`roleId`) REFERENCES `t_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `t_user_role` */

insert  into `t_user_role`(`id`,`roleId`,`userId`) values (1,1,1);


